CREATE TABLE /*_*/wiki_edit_log (
  `lid` int(12) NOT NULL,
  `user_id` int(12) NOT NULL DEFAULT '0',
  `log_type` tinyint(1) NOT NULL DEFAULT '0',
  `log_key` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `class_method` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `commit_message` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `serialized_data` mediumtext COLLATE utf8_unicode_ci,
  `timestamp` int(14) NOT NULL DEFAULT '0',
  `cache_timestamp` int(14) NOT NULL DEFAULT '0',
  `ip_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `legacy` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0'
) /*$wgDBTableOptions*/;

ALTER TABLE /*_*/wiki_edit_log
  ADD PRIMARY KEY (`lid`),
  ADD KEY `user_id` (`user_id`,`timestamp`),
  ADD KEY `cache_timestamp` (`cache_timestamp`);

ALTER TABLE /*_*/wiki_edit_log
  MODIFY `lid` int(12) NOT NULL AUTO_INCREMENT;