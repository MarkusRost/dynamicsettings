CREATE TABLE /*_*/wiki_extensions (
  `seid` int(10) NOT NULL,
  `allowed_extension_md5_key` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `site_key` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL
) /*$wgDBTableOptions*/;

ALTER TABLE /*_*/wiki_extensions
  ADD PRIMARY KEY (`seid`),
  ADD KEY `allowed_extension_md5_key` (`allowed_extension_md5_key`,`site_key`);

ALTER TABLE /*_*/wiki_extensions
  MODIFY `seid` int(10) NOT NULL AUTO_INCREMENT;