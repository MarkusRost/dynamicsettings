CREATE TABLE /*_*/wiki_sites (
  `wid` int(12) NOT NULL,
  `wiki_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `wiki_meta_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `wiki_category` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `wiki_tags` text COLLATE utf8_unicode_ci,
  `wiki_language` varchar(10) COLLATE utf8_unicode_ci DEFAULT 'en',
  `wiki_managers` text COLLATE utf8_unicode_ci,
  `wiki_portal` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '-1',
  `wiki_group` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '-1',
  `wiki_notes` text COLLATE utf8_unicode_ci,
  `db_type` enum('mysql','mysqli','postgres','sqlite') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'mysql',
  `db_server` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `db_server_replica` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `db_port` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `db_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `db_user` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `db_password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `search_type` enum('elastic') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'elastic',
  `search_server` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `search_port` int(5) NOT NULL DEFAULT '9312',
  `created` int(14) NOT NULL DEFAULT '0',
  `edited` int(14) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `group_file_repo` tinyint(1) NOT NULL DEFAULT '0',
  `use_s3` tinyint(1) NOT NULL DEFAULT '1',
  `aws_region` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `s3_bucket` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cloudfront_id` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cloudfront_domain` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  `md5_key` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_recache` int(14) NOT NULL DEFAULT '0',
  `installed` tinyint(1) NOT NULL DEFAULT '0'
) /*$wgDBTableOptions*/;

ALTER TABLE /*_*/wiki_sites
  ADD PRIMARY KEY (`wid`),
  ADD UNIQUE KEY `md5_key` (`md5_key`),
  ADD UNIQUE KEY `db_name-unique` (`db_name`),
  ADD KEY `wiki_name` (`wiki_name`),
  ADD KEY `wiki_meta_name` (`wiki_meta_name`),
  ADD KEY `wiki_category` (`wiki_category`),
  ADD KEY `wiki_portal` (`wiki_portal`),
  ADD KEY `wiki_group` (`wiki_group`);

ALTER TABLE /*_*/wiki_sites
  MODIFY `wid` int(12) NOT NULL AUTO_INCREMENT;