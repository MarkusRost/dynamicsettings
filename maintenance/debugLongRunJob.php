<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * DebugLongRunJob
 *
 * @author    Cameron Chunn
 * @copyright (c) 2018 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
**/

require_once dirname(__DIR__, 3) . "/maintenance/Maintenance.php";

class DebugLongRunJob extends Maintenance {
	/**
	 * Main Constructor
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
		$this->addDescription("A job designed to run a short, determinable time and make output as it goes to test live output.");
	}

	/**
	 * Run a long running job
	 *
	 * @return void
	 */
	public function execute() {
		$this->output("Starting the script.\n\nSleeping for 30 seconds.\n");
		sleep(30);
		$this->output("Sleeping for 30 more seconds.\n");
		sleep(30);
		$this->output("Now sleeping for 120 seconds.\n");
		sleep(120);
		$this->output("Done.");
		return 0;
	}
}

$maintClass = "DebugLongRunJob";
require_once RUN_MAINTENANCE_IF_MAIN;
