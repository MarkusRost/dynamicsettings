<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Auto Patrol All
 *
 * @author    Cameron Chunn
 * @copyright (c) 2018 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
**/

require_once dirname(__DIR__, 3) . "/maintenance/Maintenance.php";

class AuthPatrolAll extends Maintenance {
	/**
	 * Main Constructor
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
		$this->addDescription("Automatically Flag every article as patrolled");
		$this->addOption('confirm', 'Pass this to confirm you want to run this action.', false, false);
	}

	/**
	 * Send a test email.
	 *
	 * @return void
	 */
	public function execute() {
		$confirm = $this->getOption('confirm');
		if ($confirm) {
			$db = wfGetDB(DB_MASTER);
			$db->update(
				"recentchanges",
				['rc_patrolled' => 1],
				[],
				__METHOD__
			);
			$updatedRows = $db->affectedRows();
			$this->output("\n\nA total of {$updatedRows} previously unpatrolled articles were marked as patrolled.\n\n");
		} else {
			$this->output("\n\nPlease rerun this script with '--confirm' to set all articles as patrolled.\n\n");
		}
	}
}

$maintClass = "AuthPatrolAll";
require_once RUN_MAINTENANCE_IF_MAIN;
