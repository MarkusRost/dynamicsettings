<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Node Pinger
 *
 * @author    Alexia E. Smith
 * @copyright (c) 2016 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
**/

require_once dirname(__DIR__, 3) . '/maintenance/Maintenance.php';

namespace DynamicSettings;

class NodePing extends \Maintenance {
	/**
	 * Main Constructor
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
		$this->mDescription = "NodePing";

		$this->addOption('purge', 'Purge any missing or non-responsive nodes.');
	}

	/**
	 * Test the job runners and queue.
	 *
	 * @return void
	 */
	public function execute() {
		$config = \ConfigFactory::getDefaultInstance()->makeConfig('main');

		if ($this->hasOption('purge')) {
			$nodes = \DynamicSettings\Node::getAllNodesOfType(\DynamicSettings\Node::NODE_WEB);

			foreach ($nodes as $node) {
				$ping = new \JJG\Ping($node->getHostName());
				if ($ping->ping() === false) {
					$node->purge();
				}
			}
		} else {
			$regexes = $config->get('DSNodeRegex');

			$hostname = gethostname();

			$nodeType = \DynamicSettings\Node::NODE_WEB;
			if ($hostname !== false) {
				foreach ($regexes as $type => $regex) {
					if (preg_match($regex, $hostname)) {
						$nodeType = $type;
					}
				}

				$node = \DynamicSettings\Node::newFromType($nodeType);
				$node->setHostName($hostname);
				$node->ping();
			}
		}
	}
}

$maintClass = "\DynamicSettings\NodePing";
require_once RUN_MAINTENANCE_IF_MAIN;
