<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Dynamic Settings Multiple Sitemap Generator
 *
 * @author    Alex Smith
 * @copyright (c) 2014 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
**/

$host = $argv[1];

$found = false;
if (file_exists(dirname(__DIR__, 3) . '/sites/' . $host . '/LocalSettings.php')) {
	$found = true;
}

if (!$found) {
	echo "That host was not found or is not cached out to disk!\n";
	exit;
}

$_SERVER['HTTP_HOST'] = $host;
require_once dirname(__DIR__, 3) . '/maintenance/generateSitemap.php';
