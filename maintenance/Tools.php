<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Tools Maintenance Class
 *
 * @author    Alex Smith
 * @copyright (c) 2014 Curse Inc.
 * @license   GPL-2.0-or-later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
**/

namespace DynamicSettings;

use DatabaseUpdater;
use DynamicSettings\Extensions\Extension;
use DynamicSettings\Wiki\Installer\FarmInstaller;
use Exception;
use ExtensionRegistry;
use Installer;
use LoggedUpdateMaintenance;
use MWException;
use Wikimedia\Rdbms\DBConnectionError;
use Wikimedia\Rdbms\DBReplicationWaitError;

require_once dirname(__DIR__, 3) . '/maintenance/Maintenance.php';

class Tools extends \Maintenance {
	/**
	 * Available tasks/methods in this cron class.
	 *
	 * @var array
	 */
	public static $tasks = [
		'recache',
		'recacheAds',
		'recachePromotions',
		'rebuildLanguage',
		'update',
		'switchCheckout',
		'hostHelper',
		'adjustCacheFiles',
		'install',
		'scrape',
		'searchReindex'
	];

	/**
	 * Environment mapping table
	 *
	 * @var array
	 */
	private static $environmentMap = [];

	/**
	 * Wiki objects storage.
	 *
	 * @var array
	 */
	private $wikis = [];

	/**
	 * Redis Instance
	 *
	 * @var object
	 */
	private $redis = false;

	/**
	 * Reddis logging key to push output into
	 *
	 * @var string
	 */
	private $logKey = "";

	/**
	 * Environment key holder
	 *
	 * @var integer
	 */
	private $environment;

	/**
	 * Switch checkout.
	 *
	 * @var string
	 */
	private $switchCheckout = false;

	/**
	 * Hydra checkouts.
	 *
	 * @var array
	 */
	private $hydraCheckouts = [];

	/**
	 * JSON output.
	 *
	 * @var array
	 */
	private $jsonOutput = [
		'raw'	=> '',
		'tasks'	=> []
	];

	/**
	 * Constructor
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();

		$this->mDescription = "Site tools to recache information out to disk.";

		// Options for main actions.
		$this->addOption('task', "Select the task to perform, specify multiple tasks as a comma separated list: [" . implode(', ', self::$tasks) . "]  Multiple tasks will run in the order specified.", true, true);

		// Options for site selection.
		$this->addOption('all', 'Select all sites.');
		$this->addOption('sitekey', 'Select specific site by its site key.  Comma separated keys are accepted', false, true);
		$this->addOption('domain', 'Select specific site based on its domain name for the current environment.', false, true);
		$this->addOption('portal', 'Select sites based on the domain name of the portal master.', false, true);
		$this->addOption('group', 'Select sites based on the domain name of the group master.', false, true);
		$this->addOption('search', 'Select sites based on a search term.', false, true);
		$this->addOption('dbserver', 'Select sites based on a database server.', false, true);
		$this->addOption('dbname', 'Select sites based on a database name.', false, true);
		$this->addOption('dbport', 'Select sites based on a database port.', false, true);
		$this->addOption('stale', 'Select sites based on staleness.');
		$this->addOption('dist', 'Use distributed processing of tasks.');
		$this->addOption('exact', 'When search this option specifies an exact match was given.  This will bypass the confirmation prompt if only one wiki returns from the search.');
		$this->addOption('script', 'Script to run for the --hostHelper option.');
		$this->addOption('skipDeleted', 'Skip deleted wikis.');
		$this->addOption('no-rebuild-i18n', 'Disable rebuilding of i18n cache after writing config files.');
		$this->addOption('force', 'Force task to run if previously stopped by a condition specific to each task.');
		$this->addOption('loadout', 'For installs, use this domain loadouts.', false, true);
		$this->addOption('interwiki', 'For installs, use this domain for interwiki.', false, true);
		$this->addOption('scraperArgs', 'Arguments to pass to the scraper when running the "scrape" task.', false, true);
		$this->addOption('json', 'Give task status output in JSON.');
		$this->addOption('jsonIncludeRaw', 'Include raw text output in JSON.');
	}

	/**
	 * Main Executor
	 *
	 * @return boolean Found a valid task to complete.
	 */
	public function execute() {
		if ($this->initializeEnv() === false) {
			return false;
		}
		$tasks = explode(',', $this->getOption('task'));
		if ($tasks === false) {
			$tasks = [];
		}

		foreach ($tasks as $index => $task) {
			$tasks[$index] = $task = trim($task);
			foreach (self::$tasks as $casedTask) {
				if (strtolower($task) === strtolower($casedTask)) {
					$task = $casedTask;
				}
			}
			if (!in_array($task, self::$tasks) || !method_exists($this, $task)) {
				$this->output('Invalid task "' . $task . '" specified.  Valid tasks: [' . implode(', ', self::$tasks) . "]\n");
				exit;
			}
			if ($task === 'switchCheckout') {
				$config = \ConfigFactory::getDefaultInstance()->makeConfig('main');
				$this->hydraCheckouts = $config->get('DSHydraCheckouts');
				$currentCheckout = Environment::detectCheckout();
				if (!in_array($currentCheckout, $this->hydraCheckouts)) {
					$this->output("Attempting to use switchCheckout on non-configured checkout '{$currentCheckout}'.  Valid checkouts: " . implode(', ', $this->hydraCheckouts) . "\n");
					exit;
				}
				$this->switchCheckout = $currentCheckout;
			}
			$this->jsonOutput['tasks'][$task] = ['tried' => false, 'success' => false];
		}

		$skipDeleted = $this->hasOption('skipDeleted');

		if ($this->getOption('all')) {
			$this->wikis = Wiki::loadAll();

			if (empty($this->wikis)) {
				$this->output("No sites found.\n");
				return false;
			}
		}

		if (strlen($this->getOption('sitekey')) > 0) {
			if (strpos($this->getOption('sitekey'), ',') !== false) {
				$_siteKeys = explode(',', $this->getOption('sitekey'));
			} else {
				$_siteKeys[] = $this->getOption('sitekey');
			}

			foreach ($_siteKeys as $_siteKey) {
				$wiki = Wiki::loadFromHash($_siteKey);
				if ($wiki === false) {
					$this->output("Site key {$_siteKey} not found.\n");
					return false;
				}
				$this->wikis[$wiki->getSiteKey()] = $wiki;
			}
		}

		if ($this->getOption('domain')) {
			$wiki = Wiki::loadFromDomain($this->getOption('domain'));

			if ($wiki === false) {
				$this->output("Site Domain Name not found.\n");
				return false;
			}
			$this->wikis[$wiki->getSiteKey()] = $wiki;
		}

		if ($this->getOption('portal')) {
			$wikis = Wiki::loadFromSearch(0, null, $this->getOption('portal'), 'wiki_domain', 'ASC', $skipDeleted);

			foreach ($wikis as $siteKey => $wiki) {
				if ($wiki->getDomains()->getDomain() == $this->getOption('portal') && $wiki->isPortalMaster()) {
					$childWikis = $wiki->getPortalChildren();
					if ($childWikis !== false && count($childWikis)) {
						$this->wikis = $childWikis;
					}
					$this->wikis[] = $wiki;
					unset($childWikis);
					unset($wikis);
					break;
				}
			}

			if (!$this->wikis) {
				$this->output("Portal Domain Name not found.\n");
				return false;
			}
		}

		if ($this->getOption('group')) {
			$wikis = Wiki::loadFromSearch(0, null, $this->getOption('group'), 'wiki_domain', 'ASC', $skipDeleted);

			foreach ($wikis as $siteKey => $wiki) {
				if ($wiki->getDomains()->getDomain() == $this->getOption('group') && $wiki->isGroupMaster()) {
					$childWikis = $wiki->getGroupChildren();
					if ($childWikis !== false && count($childWikis)) {
						$this->wikis = $childWikis;
					}
					$this->wikis[] = $wiki;
					unset($childWikis);
					unset($wikis);
					break;
				}
			}

			if (!$this->wikis) {
				$this->output("Group Domain Name not found.\n");
				return false;
			}
		}

		if ($this->getOption('search')) {
			$this->wikis = Wiki::loadFromSearch(null, null, $this->getOption('search'), 'wiki_domain', 'ASC', $skipDeleted);

			if (!$this->wikis) {
				$this->output("Search term not found on any wikis.\n");
				return false;
			}

			$this->searchConfirmation();
		}

		if ($this->getOption('dbserver')) {
			$this->wikis = Wiki::loadFromSearch(null, null, $this->getOption('dbserver'), 'db_server', 'ASC', $skipDeleted, ['db_server']);

			if (!$this->wikis) {
				$this->output("Database server not found on any wikis.\n");
				return false;
			}

			$this->searchConfirmation();
		}

		if ($this->getOption('dbname')) {
			global $wgMasterDatabaseName;

			if ($this->hasOption('exact')) {
				$_dbname = $this->getOption('dbname');
				if ($this->getOption('dbname') == $wgMasterDatabaseName) {
					$wiki = Wiki::getFakeMainWiki();
				} else {
					$wiki = Wiki::loadFromDatabaseName($_dbname);
				}

				if ($wiki === false) {
					$this->output("Database name {$_dbname} not found.\n");
					return false;
				}
				$this->wikis[$wiki->getSiteKey()] = $wiki;
			} else {
				if ($this->getOption('dbname') == $wgMasterDatabaseName) {
					$mainWiki = Wiki::getFakeMainWiki();
					$this->wikis[$mainWiki->getSiteKey()] = $mainWiki;
				} else {
					$this->wikis = Wiki::loadFromSearch(null, null, $this->getOption('dbname'), 'db_name', 'ASC', $skipDeleted, ['db_name']);
				}

				if (!$this->wikis) {
					$this->output("Database name not found on any wikis.\n");
					return false;
				}

				$this->searchConfirmation();
			}
		}

		if ($this->getOption('dbport')) {
			$this->wikis = Wiki::loadFromSearch(null, null, $this->getOption('dbport'), 'db_port', 'ASC', $skipDeleted, ['db_port']);

			if (!$this->wikis) {
				$this->output("Database port not found on any wikis.\n");
				return false;
			}

			$this->searchConfirmation();
		}

		if ($this->getOption('stale')) {
			$logEntries = EditLog::getStaleEntries(null, [EditLog\Entry::LOGTYPE_WIKI, EditLog\Entry::LOGTYPE_PERMISSIONS, EditLog\Entry::LOGTYPE_EXTENSIONS, EditLog\Entry::LOGTYPE_NAMESPACES, EditLog\Entry::LOGTYPE_SETTINGS, EditLog\Entry::LOGTYPE_DOMAINS]);

			foreach ($logEntries as $entry) {
				$wiki = Wiki::loadFromHash($entry->getLogKey());
				if ($wiki !== false && !array_key_exists($wiki->getSiteKey(), $this->wikis)) {
					$this->wikis[$wiki->getSiteKey()] = $wiki;
				}
			}

			if (!$this->wikis) {
				$this->output("No stale wikis found.\n");
				return false;
			}

			$this->searchConfirmation();
		}

		if (!$this->wikis) {
			$this->output("No sites were loaded to process.\n");
			return false;
		}

		if ($this->getOption('scraperArgs')) {
			$this->scraperArgs = $this->getOption('scraperArgs');
		} else {
			$this->scraperArgs = "";
		}

		if ($this->getOption('loadout')) {
			$this->loadout = $this->getOption('loadout');
		} else {
			$this->loadout = 'default';
		}

		if ($this->getOption('interwiki')) {
			$this->interwiki = $this->getOption('interwiki');
		} else {
			$this->interwiki = false;
		}
		$failed = false;
		foreach ($this->wikis as $siteKey => $wiki) {
			if (empty($wiki)) {
				$this->output("Skipping {$siteKey} due to invalid Wiki object.\n");
				continue;
			}

			$domain = $wiki->getDomains()->getDomain($this->getEnvironment());
			if (!$domain) {
				$this->output("Skipping {$siteKey} due to no domain name being set.\n");
				continue;
			}

			if ($this->hasOption('dist')) {
				$jobOptions = [
					'tasks' => $tasks,
					'site_key' => $siteKey,
					'domain' => $domain,
					'script' => $this->getOption('script'),
					'scraperArgs' => $this->scraperArgs,
					'interwiki' => $this->interwiki,
					'loadout'	=> $this->loadout
				];
				$jobKeys = Job\ToolsJob::queueJob($jobOptions);
				foreach ($tasks as $index => $task) {
					$this->output(ucfirst($task) . " task(s) queued for " . $wiki->getName() . ".  (Job Key: " . $jobKeys[$index] . ")\n\n");
				}
			} else {
				$abortIfFailed = in_array('switchCheckout', $tasks);
				foreach ($tasks as $task) {
					$success = $this->runTask($task, $wiki);
					$this->jsonOutput['tasks'][$task]['tried'] = true;
					$this->jsonOutput['tasks'][$task]['success'] = boolval($success);
					if (!$success) {
						$failed = true;
						if ($abortIfFailed) {
							$this->output("Aborting!  Task `{$task}` failed.\n");
							break;
						}
					}
				}
			}
			$wiki->releaseData(); // free up memory
		}

		$this->sendFinalOutput();

		if ($failed) {
			return false;
		}
		return true;
	}

	/**
	 * Helper function to call a "task" on this class with the correct output.
	 *
	 * @param string Task Name
	 * @param object Wiki
	 * @param array Additional arguments for the function call.
	 *
	 * @return boolean Success
	 */
	public function runTask($task, Wiki $wiki, $arguments = []) {
		if (!is_array($arguments)) {
			throw new Exception('Function arguments should be in an array.');
		}

		$json = false;
		foreach ($arguments as $argument) {
			if (strpos($argument, '--result=json') !== false) {
				$json = true;
			}
		}
		if (!$this->hasOption('exact') && !$json) {
			$message = "{$task} / {$wiki->getDomains()->getDomain()}\n";
			$header = str_pad('', strlen($message), '-') . "\n";
			$this->output("\n" . $header);
			$this->output($message);
			$this->output($header);
		}
		array_unshift($arguments, $wiki);
		return call_user_func_array([$this, $task], $arguments);
	}

	/**
	 * Handle search confirmation for wikis.
	 *
	 * @return boolean Confirmation
	 */
	private function searchConfirmation() {
		$foundWikis = '';
		foreach ($this->wikis as $siteKey => $wiki) {
			$foundWikis .= $wiki->getName() . " (" . strtoupper($wiki->getLanguage()) . ") - https://" . $wiki->getDomains()->getDomain() . "\n";
		}
		if ($this->hasOption('exact') && count($this->wikis) > 1) {
			$this->output("Option --exact was specified, but more than one wiki was returned.\n");
			return false;
		}
		if (!$this->hasOption('exact')) {
			$this->output($foundWikis . "Select the above wikis? [Y/N]\n");
			$handle = fopen('php://stdin', 'r');
			$response = fgets($handle);
			if (strtolower(trim($response)) == 'n') {
				exit;
			}
		}
		return true;
	}

	/**
	 * Rebuild language files
	 *
	 * @param object \DynamicSettings\Wiki
	 * @param string [Optional] Language to rebuild.  Will rebuild wiki's default language if omitted.
	 *
	 * @return boolean False on general errors.
	 */
	public function rebuildLanguage($wiki, $languageCode = null) {
		$domain = $wiki->getDomains()->getDomain();
		if ($languageCode === null) {
			$languageCode = $wiki->getLanguage();
		}

		$this->hostHelper($wiki, MAINTENANCE_DIR . '/rebuildLocalisationCache.php --lang=' . escapeshellarg($languageCode) . ' --force');

		return true;
	}

	/**
	 * Recaches wiki sites to their respective LocalSettings.php and Extensions.php files.
	 *
	 * @param object \DynamicSettings\Wiki
	 *
	 * @return boolean False on general errors.
	 */
	public function recache($wiki) {
		if ($this->initializeEnv() === false) {
			return false;
		}
		// Little helper to make sure the main site is always cached.
		$fakeMainWiki = Wiki::getFakeMainWiki();
		$this->saveToCache($fakeMainWiki);

		if ($wiki->isMasterWiki()) {
			// Master wikis are hard coded in ./master/LocalSettings.php.
			$this->output("Skipping recaching a master wiki.\n");
			return true;
		}

		// Set this as helper to transition from the old foreach looping code to the new recache($wiki) with a wiki object.
		$siteKey = $wiki->getSiteKey();

		// Grab allowed extension and settings.
		$allowedExtensions = AllowedExtensions::getInstance()->getAll();
		$allowedSettings = AllowedSettings::getInstance()->getAll();

		// Set up some variables to hold misc items.
		$dbUpdates = [];
		$_allowedExtensions = [];
		$_allowedSettings = [];
		$_sites = [];

		// Initialize Rename Queue
		$dr = new DomainRename($wiki);

		$dbUpdates = false;
		$logEntries = EditLog::getStaleEntries($siteKey);
		if (count($logEntries)) {
			$this->output("New changes include...\n");
		}
		foreach ($logEntries as $entry) {
			$this->output(strip_tags($entry->getKeyLink() . " - " . $entry->getTypeLink()) . "\n");
			$this->output("\t\"{$entry->getCommitMessage()}\"\n");
			if ($entry->isDatabaseUpdateRequired()) {
				$dbUpdates = true;
			}
			if ($entry->getLogType() === EditLog\Entry::LOGTYPE_DOMAINS) {
				if ($entry->getPreviousEntry() !== null && !$dr->addByLogEntry($entry)) {
					$this->output('DomainRename::addByLogEntry failed most likely due to a bad edit log entry.  ' . $entry->getSerializedData() . "\n");
					return false;
				}
			}
		}

		if ($dbUpdates) {
			$this->output("\nDatabase update is required for one or more extensions.\n");
		}

		$this->templates = new \TemplateLocalSettings();

		$this->output("\nWriting new cache files...\n");

		if ($wiki->isInstalled()) {
			// If wiki installed, attempt to execute Rename Queue.
			if (!$dr->execute()) {
				return false; // if it fails, we fail.
			}
			$this->output($dr->getOutput());
		}

		$domain	= $wiki->getDomains()->getDomain($this->getEnvironment());
		if (!$wiki->isDeleted()) {
			if (!Sites::makeSiteFolder($domain)) {
				$this->output("ERROR!  Unable to write site folder.  Exiting to prevent further errors.");
				exit;
			}

			$settings = $this->templates->localSettings($wiki, $this->getEnvironment());
			Sites::writeSiteFile($domain, 'LocalSettings.php', $settings);

			foreach ($wiki->getDomains()->getRedirects() as $redirect) {
				$this->output("Domain Redirect: " . $redirect . "\n");
				$this->cleanUp($redirect);
				Sites::makeSiteFolder($redirect);
				$settings = $this->templates->localSettingsRedirect($wiki, $this->getEnvironment());
				Sites::writeSiteFile($redirect, 'LocalSettings.php', $settings);
			}

			Sites::deleteSiteFile($domain, 'Extensions.php');

			if ($dbUpdates) {
				$this->output("Running database update...\n");
				$this->update($wiki);
			}

			$this->output("\nRecache done.\n");

			if (!$this->getOption('no-rebuild-i18n')) {
				$this->runTask('rebuildLanguage', $wiki);
				if ($wiki->getLanguage() != 'en') {
					$this->runTask('rebuildLanguage', $wiki, ['en']);
				}
			}
			$this->redis->lPush('siteRecacheWorker', $domain);
			$this->updateLastRecache($wiki);
		} else {
			$this->output("Cleaning up deleted wiki: " . $domain);
			$this->cleanUp($domain);
		}

		foreach ($logEntries as $entry) {
			$entry->setCacheTimestamp(time());
			$entry->save();
		}

		// Save to the global cache, typically Redis.(Even if deleted.)
		$this->saveToCache($wiki);

		return true;
	}

	/**
	 * Recaches advertisements to their respective site folders.
	 *
	 * @param object \DynamicSettings\Wiki
	 *
	 * @return boolean False on general errors.
	 */
	public function recacheAds($wiki) {
		$domain	= $wiki->getDomains()->getDomain($this->getEnvironment());

		$advertisements = $wiki->getAdvertisements();

		if (!$wiki->isDeleted()) {
			Sites::makeSiteFolder($domain);

			$idSlots = [];
			foreach (Wiki\Advertisements::getIdSlots() as $slot) {
				$idSlots[$slot] = $advertisements->getBySlot($slot);
			}
			$jsSlots = [];
			foreach (Wiki\Advertisements::getJsSlots() as $slot) {
				$jsSlots[$slot] = $advertisements->getBySlot($slot);
			}
			$adSlots = [];
			foreach (Wiki\Advertisements::getAdSlots() as $slot) {
				$adSlots[$slot] = $advertisements->getBySlot($slot);
			}
			$miscSlots = [];
			foreach (Wiki\Advertisements::getMiscSlots() as $slot) {
				$miscSlots[$slot] = $advertisements->getBySlot($slot);
			}
			if (count($idSlots) || count($jsSlots) || count($adSlots) || count($miscSlots)) {
				$this->output("Writing: " . $domain . "\n");
				Sites::writeSiteFile($domain, 'Advertisements.php', \TemplateSiteAds::siteAds($idSlots, $jsSlots, $adSlots, $miscSlots, $advertisements->isDisabled()));
			} else {
				$this->output("Deleting: " . $domain . "\n");
				Sites::deleteSiteFile($domain, ['Advertisements.php', 'recovery.json']);
			}
		} else {
			$this->output("Deleting: " . $domain . "\n");
			Sites::deleteSiteFile($domain, ['Advertisements.php', 'recovery.json']);
		}

		$this->output("\nRecache advertisements done.\n");

		return true;
	}

	/**
	 * Recaches wiki promos to their respective Promotions.php.
	 *
	 * @param object \DynamicSettings\Wiki
	 *
	 * @return boolean False on general errors.
	 */
	public function recachePromotions($wiki) {
		$this->templates = new \TemplatePromotions();

		$domain	= $wiki->getDomains()->getDomain($this->getEnvironment());

		if (!$wiki->isDeleted()) {
			Sites::makeSiteFolder($domain);

			$promotions = $wiki->getPromotions();
			if (is_array($promotions) && count($promotions)) {
				$this->output("Writing: " . $domain . "\n");
				$promosHTML = $this->templates->promotions($promotions);
				Sites::writeSiteFile($domain, 'Promotions.php', $promosHTML);
			} else {
				$this->output("Deleting: " . $domain . "\n");
				Sites::deleteSiteFile($domain, 'Promotions.php');
			}
		} else {
			$this->output("Deleting: " . $domain . "\n");
			Sites::deleteSiteFile($domain, 'Promotions.php');
		}

		return true;
	}

	/**
	 * Installs a wiki for the first time.
	 *
	 * @param object \DynamicSettings\Wiki
	 *
	 * @return boolean False on general errors.
	 */
	public function install($wiki) {
		global $IP;

		$siteKey = $wiki->getSiteKey();
		$domain	= $wiki->getDomains()->getDomain($this->getEnvironment());
		$wikiLanguage = $wiki->getLanguage();
		$loadout = $this->loadout;
		$interwiki = $this->interwiki;

		// Remove dashes for purposes like pt-br to ptbr (for default-loadout-ptbr.gamepedia.com)
		// MediaWiki represents them using dashes, but our default loadout domains do not have dashes.
		$wikiLanguage = str_replace("-", "", $wikiLanguage);

		// Sort options from loadout to determine loadout domain.
		if ($loadout == "none") {
			// None, we won't perform a loadout
			$loadoutDomain = false;
		} elseif ($loadout == "default") {
			// Default, we will use default-loadout-language
			$loadoutDomain = "default-loadout-{$wikiLanguage}.gamepedia.com";
			if (Wiki::loadFromDomain($loadoutDomain) == false) {
				// If no loadout for this language exists, lets use english.
				$this->output("No loadout found at {$loadoutDomain}, defaulting to english.\n");
				$loadoutDomain = "default-loadout-en.gamepedia.com";
			}
		} else {
			// Manually Specified Domain we will use for Loadout
			$loadoutDomain = $loadout;
		}

		if ($interwiki == "none") {
			// None, no interwiki action
			$interwikiDomain = false;
		} elseif ($interwiki == "default") {
			// Default, follow whatever the loadout is doing
			$interwikiDomain = $loadoutDomain;
		} else {
			// Manually defined interwiki loadout
			$interwikiDomain = $interwiki;
		}

		$this->output("\n\nStarting Install Process.\n");
		$this->output("\nLoadout: " . ($loadoutDomain ? $loadoutDomain : "none"));
		$this->output("\nInterwiki: " . ($interwikiDomain ? $interwikiDomain : "none") . "\n\n");

		define('MW_CONFIG_CALLBACK', 'FarmInstaller::overrideConfig');
		define('MEDIAWIKI_INSTALL', true);

		$config = \ConfigFactory::getDefaultInstance()->makeConfig('main');
		$installAdminAccount = $config->get('DSInstallAdminAccount');
		$installDBUser = $config->get('DSInstallDBUser');

		if ($wiki->isInstalled() && !$this->getOption('force')) {
			echo "Skipping wiki " . $wiki->getName() . " because it is already installed.  Use --force to ignore this error.\n";
			return false;
		} elseif ($this->getOption('force')) {
			echo "\n ~!~!~!~ Forcing install on Wiki already marked as installed!  ~!~!~!~ \n\n";
		}

		$options = [
			'metaNamespace'	=> $wiki->getMetaName(),
			'scriptpath'	=> $config->get('ScriptPath'),
			'pass'			=> $installAdminAccount['password'],
			'lang'			=> $wiki->getLanguage(),
			'dbtype'		=> $wiki->getDatabase()['db_type'],
			'dbserver'		=> $wiki->getDatabase()['db_server'] . ($wiki->getDatabase()['db_type'] == 'mysql' ? ':' . $wiki->getDatabase()['db_port'] : null),
			'dbport'		=> $wiki->getDatabase()['db_port'],
			'dbname'		=> $wiki->getDatabase()['db_name'],
			'dbuser'		=> $wiki->getDatabase()['db_user'],
			'dbpass'		=> $wiki->getDatabase()['db_password'],
			'installdbuser'	=> ($installDBUser !== false ? $installDBUser['user'] : $wiki->getDatabase()['db_user']),
			'installdbpass'	=> ($installDBUser !== false ? $installDBUser['password'] : $wiki->getDatabase()['db_password']),
			'confpath'		=> null // Don't care.
		];

		$installer = new FarmInstaller($wiki->getName(), $installAdminAccount['user'], $options);

		$status = $installer->doEnvironmentChecks();
		if ($status->isGood()) {
			$installer->showMessage('config-env-good');
		} else {
			$installer->showStatusMessage($status);
			return false;
		}
		if (!$this->hasOption('env-checks')) {
			$installer->execute();
		}

		$this->output("\n");

		$this->output("\n--------------INSTALL--------------\n");
		$this->output(($domain));
		$this->output("\n-----------------------------------\n");

		$exceptions = false;

		// Run a recache against the wiki.
		$this->runTask('recache', $wiki);

		// Run an update against the wiki.
		$this->runTask('update', $wiki);

		$this->createGlobalBot();

		if ($interwikiDomain) {
			$this->output("Pulling interwiki table from {$loadoutDomain} before import.\n");
			$pullInterwiki = $this->runChild('\DynamicSettings\Tools', __DIR__ . '/Tools.php');
			$pullInterwiki->mOptions = [
				'task'			=> 'hostHelper',
				'domain'		=> $wiki->getDomains()->getDomain(),
				'script'		=> $IP . '/extensions/DynamicSettings/maintenance/GetInterwikiFromLoadout.php --purgeFirst --loadout=' . escapeshellarg($interwikiDomain),
				'memory-limit'	=> 'max',
				'profiler'		=> null,
			];
			$pullInterwiki->execute();
		} else {
			$this->output("Taking no action against interwiki data.");
		}

		if ($loadoutDomain) {
			$this->output("Firing up shopvac to bring in default wiki loadout from {$loadoutDomain}\n");
			$shopVacRun = $this->runChild('\DynamicSettings\Tools', __DIR__ . '/Tools.php');
			$shopVacRun->mOptions = [
				'task'			=> 'hostHelper',
				'domain'		=> $wiki->getDomains()->getDomain(),
				'script'		=> $IP . '/extensions/DynamicSettings/maintenance/ShopVacWrapper.php --domain=' . escapeshellarg($wiki->getDomains()->getDomain()) . ' --loadout=' . escapeshellarg($loadoutDomain),
				'memory-limit'	=> 'max',
				'profiler'		=> null,
			];
			$shopVacRun->execute();

			$this->output("Default load out complete!\n\n");
		} else {
			$this->output("Not performing a default loadout import");
		}

		// Create the search index for the wiki.
		$this->output("Creating elastic search index... ");
		$elasticRebuild = $this->runChild('\DynamicSettings\Tools', __DIR__ . '/Tools.php');
		$elasticRebuild->mOptions = [
			'task'			=> 'hostHelper',
			'domain'		=> $wiki->getDomains()->getDomain(),
			'script'		=> $IP . '/extensions/CirrusSearch/maintenance/updateSearchIndexConfig.php --startOver',
			'memory-limit'	=> 'max',
			'profiler'		=> null,
		];
		$elasticRebuild->execute();
		$this->output("Search indexes created!\n\n");

		// Rebuild recent changes.
		$this->output("Running rebuild recent changes maintenance job... ");
		$populateDefault = $this->runChild('\DynamicSettings\Tools', __DIR__ . '/Tools.php');
		$populateDefault->mOptions = [
			'task'			=> 'hostHelper',
			'domain'		=> $wiki->getDomains()->getDomain(),
			'script'		=> $IP . '/maintenance/rebuildrecentchanges.php',
			'memory-limit'	=> 'max',
			'profiler'		=> null,
		];
		$populateDefault->execute();
		$this->output("Recent changes Rebuilt.\n\n");

		$autoPatrolAll = $this->runChild('\DynamicSettings\Tools', __DIR__ . '/Tools.php');
		$autoPatrolAll->mOptions = [
			'task'			=> 'hostHelper',
			'domain'		=> $wiki->getDomains()->getDomain(),
			'script'		=> $IP . '/extensions/DynamicSettings/maintenance/autoPatrolAll.php --confirm',
			'memory-limit'	=> 'max',
			'profiler'		=> null,
		];
		$autoPatrolAll->execute();

		/*if (ob_get_level() > 0) {
			$output = ob_get_contents();
			if (strpos($output, 'Unexpected Elasticsearch failure') !== false) {
				$exceptions[] = 'Elasticsearch';
			}
		}*/

		if ($exceptions === false) {
			// Update database to reflect installed state.
			$this->setWikiInstalled($wiki);
		} else {
			throw new MWException(__METHOD__ . ": The following tasks had fatal errors: " . implode(', ', $exceptions));
		}

		$this->output("The wiki should be ready to go!\n");

		return true;
	}

	/**
	 * Runs scraper.
	 *
	 * @param object \DynamicSettings\Wiki - Passing no wiki causes an update to the master wiki.
	 *
	 * @return boolean False on general errors.
	 */
	public function scrape($wiki) {
		$redisServer = $this->getRedis()->getServer();
		$rs = explode(":", $redisServer);
		$args = $this->scraperArgs . " --redisServer " . $rs[0] . " --redisPort " . $rs[1];

		$cmd = "node --expose_gc --max_old_space_size=4096 " . SITE_DIR . "/extensions/ShopVac/script/shopvac.js --aggressiveGC $args";

		exec($cmd, $output, $return_var);
		$output = implode("\n", $output);
		$this->output($output);

		return ($return_var == 0 ? true : false);
	}

	/**
	 * Run database updates against a target wiki.
	 * Falls back to running the classic update.php if legacy extensions are enabled on the wiki.
	 *
	 * @param object [Optional] \DynamicSettings\Wiki - Passing no wiki causes an update to the master wiki.
	 * @param boolean [Optional] Is this a retry of a failed update due to replication lag?
	 *
	 * @return boolean False on general errors.
	 */
	public function update($wiki = false, $lagRetry = false) {
		global $wgVersion, $wgExtensionDirectory, $wgStyleDirectory, $wgHooks;

		// Some extensions require this to be set.
		if (!defined('MW_UPDATER')) {
			define('MW_UPDATER', true);
		}

		$this->output("Hydra {$wgVersion} Updater\n\n");

		// Master wiki or the given child wiki?
		try {
			if ($wiki === false) {
				$db = wfGetDB(DB_MASTER);
			} else {
				$db = $wiki->getDatabaseLBDB(DB_MASTER);
			}
		} catch (DBConnectionError $e) {
			$this->output("DBConnectionError while attempting to get load balancer for database update: ");
			$this->output($e->getMessage() . "\n");
			$this->output($e->getTraceAsString() . "\n");
			return false; // We can return here since we have not overwritten any configurations yet.
		}

		// Check to see whether the database server meets the minimum requirements
		/**
 * @var DatabaseInstaller $dbInstallerClass
*/
		$dbInstallerClass = Installer::getDBInstallerClass($db->getType());
		$status = $dbInstallerClass::meetsMinimumRequirement($db->getServerVersion());

		$success = true;
		$registry = new ExtensionRegistry(); // Get a new copy to not pollute the static instance.
		$hasLegacyExtensions = false;
		$legacyExtensions = [];

		// Store the original LoadExtensionSchemaUpdates for safe keeping.
		$oldSchemaHooks = $wgHooks['LoadExtensionSchemaUpdates'];
		$oldHost = $_SERVER['HTTP_HOST'] ?? null;
		if ($wiki !== false) {
			$_SERVER['HTTP_HOST'] = $wiki->getDomain();
		}
		Environment::pretendIsChild(true);
		FarmInstaller::overrideConfig();
		try {
			// Get the enabled extensions on this wiki and queue then into our local instance ExtensionRegistry.
			$extensions = $wiki->getExtensions();
			foreach ($extensions as $extension) {
				switch ($extension->getLoadType()) {
					case Extension::LOAD_EXTENSION;
						$registry->queue("{$wgExtensionDirectory}/{$extension->getExtensionFolder()}/extension.json");
						break;
					case Extension::LOAD_EXTENSION_LEGACY;
						$hasLegacyExtensions = true;
						$legacyExtensions[] = $extension->getExtensionName();
						break;
					case Extension::LOAD_SKIN;
						$registry->queue("{$wgStyleDirectory}/{$extension->getExtensionFolder()}/skin.json");
						break;
				}
			}

			// Get the data from the extensions we registered.
			$data = $registry->readFromQueue($registry->getQueue());
			$registry->clearQueue();

			$hooks = [];
			if (isset($data['globals']['wgHooks']['LoadExtensionSchemaUpdates'])) {
				$hooks = $data['globals']['wgHooks']['LoadExtensionSchemaUpdates'];
			}
			// Use the read data to override hooks with only the database update hooks.
			$wgHooks['LoadExtensionSchemaUpdates'] = $hooks;

			// Standard update process as shown in maintenance/update.php.
			$updater = DatabaseUpdater::newForDB($db, false, $this);
			$updater->doUpdates(['core', 'extensions', 'stats']);

			foreach ($updater->getPostDatabaseUpdateMaintenance() as $maint) {
				$child = $this->runChild($maint);

				// LoggedUpdateMaintenance is checking the updatelog itself
				$isLoggedUpdate = $child instanceof LoggedUpdateMaintenance;

				if (!$isLoggedUpdate && $updater->updateRowExists($maint)) {
					continue;
				}

				$child->execute();
				if (!$isLoggedUpdate) {
					$updater->insertUpdateRow($maint);
				}
			}

			$updater->setFileAccess();
			if (!$this->hasOption('nopurge')) {
				$updater->purgeCache();
			}
		} catch (Exception $e) {
			if ($e instanceof DBReplicationWaitError) {
				$sleep = 5;
				if ($lagRetry) {
					$this->output("Waited {$sleep} seconds and replica(s) did not catch up.  Aborting!\n");
					$this->output($e->getMessage() . "\n");
					$this->output($e->getTraceAsString() . "\n");
					$success = false;
				} else {
					$this->output("Encountered replication lag, waiting {$sleep} seconds and retrying...\n\n");
					sleep($sleep);
					$wgHooks['LoadExtensionSchemaUpdates'] = $oldSchemaHooks;
					FarmInstaller::restoreConfig();
					Environment::pretendIsChild(false);
					$_SERVER['HTTP_HOST'] = $oldHost;
					$success = $this->update($wiki, true);
				}
			} else {
				$this->output("General exception while running update: ");
				$this->output($e->getMessage() . "\n");
				$this->output($e->getTraceAsString() . "\n");
				$success = false;
			}
		}
		// Reset LoadExtensionSchemaUpdates back to the original.
		$wgHooks['LoadExtensionSchemaUpdates'] = $oldSchemaHooks;
		FarmInstaller::restoreConfig();
		Environment::pretendIsChild(false);
		$_SERVER['HTTP_HOST'] = $oldHost;

		if ($success && $hasLegacyExtensions) {
			// If the wiki has legacy loaded extensions enabled look stupid and rerun on the command line.
			$this->output("\n\n\n\nLegacy extensions detected: " . implode(',', $legacyExtensions) . "\n");
			$script = MAINTENANCE_DIR . '/update.php --skip-external-dependencies --quick';
			$this->hostHelper($wiki, $script);
		}

		return $success;
	}

	/**
	 * Runs specified script for selected wikis.
	 *
	 * @param object \DynamicSettings\Wiki
	 * @param string [Optional] Override the script command.
	 *
	 * @return boolean False on general errors.
	 */
	public function hostHelper($wiki, $script = null) {
		$domain = $wiki->getDomains()->getDomain($this->getEnvironment());
		if (!$domain) {
			return false;
		}

		if ($script === null) {
			$script = $this->getOption('script');
		}

		if ($wiki->getSiteKey() != 'hydra-master-df63298e4fbf4f7277d') {
			$command = PHP_BINDIR . '/php ' . dirname(__DIR__, 2) . '/HydraCore/maintenance/hostHelper.php ' . escapeshellarg($domain) . ' ' . $script . ' 2>&1';
		} else {
			global $IP;
			$command = PHP_BINDIR . "/php {$IP}/" . $script . ' 2>&1';
		}
		if (strpos($script, '--result=json') === false) {
			$this->output("Running command: " . $command . "\n\n");
		}

		$descriptorspec = [
			0 => ["pipe", "r"],   // stdin is a pipe that the child will read from
			1 => ["pipe", "w"],   // stdout is a pipe that the child will write to
			2 => ["pipe", "w"]    // stderr is a pipe that the child will write to
		];

		try {
			$process = proc_open($command, $descriptorspec, $pipes);
			if (is_resource($process)) {
				while ($s = fgets($pipes[1])) {
					$this->output($s);
				}
			}
			$status = proc_close($process);
		} catch (Exception $e) {
			$status = 1;
			$this->output($e->getMessage());
		}

		if ($status > 0) {
			$this->output("\nProcess exited with error.  ({$status})\n");
			return false;
		}

		$this->output("\nProcess ended cleanly.\n");

		return true;
	}

	/**
	 * Switch A/B testing and/or checkout for a given domain if needed.
	 *
	 * @param object \DynamicSettings\Wiki
	 *
	 * @return boolean False on general errors.
	 */
	private function switchCheckout($wiki) {
		if ($this->switchCheckout === false) {
			$this->output("Checkout to switch to is invalid, aborting this task.\n");
			return false;
		}

		$_domain = $wiki->getDomains()->getDomain($this->getEnvironment());
		if (!$_domain) {
			return false;
		}
		$domains[] = $_domain;
		$domains = array_merge($domains, $wiki->getDomains()->getRedirects());

		$success = true;
		foreach ($domains as $domain) {
			try {
				if (!Environment::switchCheckout($domain, $this->switchCheckout)) {
					$success = false;
					$this->output("Failed to switch {$domain} to {$this->switchCheckout}.  (Is Redis up?)\n");
					continue;
				}
				$this->output("Switched {$domain} to {$this->switchCheckout}.\n");
			} catch (MWException $e) {
				$this->output("Exception caught when attempting to switch {$domain} to {$this->switchCheckout}: {$e->getMessage()}\n");
			}
		}

		return $success;
	}

	/**
	 * Force a reindex of search against a wiki.
	 *
	 * @param object \DynamicSettings\Wiki
	 *
	 * @return boolean False on general errors.
	 */
	private function searchReindex($wiki) {
		global $IP;

		$domain = $wiki->getDomains()->getDomain($this->getEnvironment());
		if (!$domain) {
			return false;
		}

		if ($wiki->getSiteKey() != 'hydra-master-df63298e4fbf4f7277d') {
			$command = PHP_BINDIR . '/php ' . dirname(__DIR__, 2) . '/HydraCore/maintenance/hostHelper.php ' . escapeshellcmd($domain) . " {$IP}/extensions/CirrusSearch/maintenance/forceSearchIndex.php 2>&1";
		} else {
			$command = PHP_BINDIR . "/php {$IP}/extensions/CirrusSearch/maintenance/forceSearchIndex.php 2>&1";
		}
		$this->output("Running command: " . $command . "\n");
		$output = [];
		$status = null;
		exec($command, $output, $status);
		$output = implode("\n", $output);
		$this->output($output . "\n");

		if ($status > 0) {
			return false;
		}
		return true;
	}

	/**
	 * Create the global page editor robot account.
	 *
	 * @return object Status
	 */
	protected function createGlobalBot() {
		$config = \ConfigFactory::getDefaultInstance()->makeConfig('main');
		$gpeUser = $config->get('DSGlobalPageEditorInstallAccount');

		$name = $gpeUser['user'];
		$user = \User::newFromName($name);

		if (!$user) {
			return \Status::newFatal('config-admin-error-bot-user', $name);
		}

		if ($user->idForName() == 0) {
			$userStatus = $user->addToDatabase();
			if (!$userStatus->isGood()) {
				return \Status::newFatal('config-admin-error-bot-user-add', $name);
			}

			$user->addGroup('bot');
			$user->saveSettings();

			if (class_exists('HydraAuthIdLookup')) {
				$lookup = new \HydraAuthIdLookup();
				$globalId = $lookup->centralIdFromName($user->getName());
				if ($globalId > 0) {
					\HydraAuthUser::forceUpdateGlobalLink($user->getId(), $globalId);
				}
			}

			$ssUpdate = new \SiteStatsUpdate(0, 0, 0, 0, 1);
			$ssUpdate->doUpdate();
		}
		$status = \Status::newGood();

		return $status;
	}

	/**
	 * Cleans up orphaned files and folders.
	 *
	 * @return void
	 */
	private function cleanUp($domain) {
		if (!empty($domain)) {
			Sites::deleteSiteFile(
				$domain,
				[
					'LocalSettings.php',
					'Extensions.php',
					'SemanticSettings.php',
					'SiteAds.php',
					'Advertisements.php',
					'SitePromos.php',
					'Promos.php',
					'Promotions.php',
					'Contests.php',
					'recovery.json'
				]
			);
			Sites::deleteSiteFolder($domain);
		} else {
			$this->output(__METHOD__ . ' Failed to pass a domain to clean up files.');
		}
	}

	/**
	 * Handles listening to the siteRecache Redis channel and chmod/chowns cache files.
	 *
	 * @return void
	 */
	private function adjustCacheFiles() {
		if ($this->redis !== false) {
			$config = \ConfigFactory::getDefaultInstance()->makeConfig('main');
			$permissionsMode = $config->get('DSPermissionsMode');
			$permissionsUserGroup = $config->get('DSPermissionsUserGroup');
			// Do the normal listening routine.
			$this->output(__METHOD__ . " listening to siteRecacheWorker list for recaches.\n");

			while ($message = $this->redis->brPop('siteRecacheWorker', 0)) {
				if (!empty($message[1])) {
					$folder = escapeshellcmd(Sites::getSiteCachePath($message[1]));
					$this->output("Performing CHMOD and CHOWN on " . $folder);
					shell_exec('chmod -R ' . escapeshellarg($permissionsMode) . ' ' . $folder);
					shell_exec('chown -R ' . escapeshellarg($permissionsUserGroup) . ' ' . $folder);
					$this->output("-----------------------------------\n");
				}
			}
		} else {
			$this->output(__METHOD__ . ' Failed - Redis is not initialized.', time());
		}
	}

	/**
	 * Saves basic wiki information to Redis Cache for consumption elsewhere.
	 *
	 * @param object Wiki
	 *
	 * @return boolean Success
	 */
	public function saveToCache($wiki) {
		if ($wiki->getSiteKey() === false || $this->redis === false) {
			return false;
		}

		try {
			$this->redis->sAdd('dynamicsettings:siteHashes', $wiki->getSiteKey());
			$this->redis->hSet('dynamicsettings:siteDbNames', $wiki->getDatabase()['db_name'], $wiki->getSiteKey());
			$this->redis->hSet('dynamicsettings:siteNameKeys', mb_strtolower($wiki->getNameForDisplay(), "UTF-8"), $wiki->getSiteKey()); // This gets used later for hScan.  However, hScan is case sensitive so all names are forced to lower case for case insensitive searching.

			$save = [
				'wiki_name'			=> $wiki->getName(),
				'wiki_name_display' => $wiki->getNameForDisplay(),
				'wiki_meta_name'	=> $wiki->getMetaName(),
				'wiki_domain'		=> $wiki->getDomains()->getDomain(),
				'wiki_category'		=> $wiki->getCategory(),
				'wiki_tags'			=> (count($wiki->getTags()) ? $wiki->getTags() : null),
				'wiki_language'		=> $wiki->getLanguage(),
				'wiki_managers'		=> (count($wiki->getManagers()) ? $wiki->getManagers() : null),
				'wiki_portal'		=> $wiki->getPortalKey(),
				'wiki_group'		=> $wiki->getGroupKey(),
				'wiki_notes'		=> $wiki->getNotes(),
				'edited'			=> $wiki->getEdited(),
				'created'			=> $wiki->getCreated(),
				'deleted'			=> intval($wiki->isDeleted()),
				'md5_key'			=> $wiki->getSiteKey()
			];

			if ($wiki->isGroupMaster() || $wiki->isGroupChild()) {
				$_members = \DynamicSettings\Wiki::getGroupMembersByDomain($wiki->getDomains()->getDomain(\DynamicSettings\Wiki\Domains::ENV_PRODUCTION));
				$members = [];
				foreach ($_members as $member) {
					$members[] = ['domain' => $member->getDomains()->getDomain(\DynamicSettings\Wiki\Domains::ENV_PRODUCTION), 'language' => $member->getLanguage()];
					unset($member);
				}
				$save['wiki_group_members'] = $members;
			}

			$args = [];
			foreach ($save as $key => $data) {
				$args[$key] = serialize($data);
			}

			$this->redis->hMSet('dynamicsettings:siteInfo:' . $wiki->getSiteKey(), $args);
		} catch (RedisException $e) {
			wfDebug(__METHOD__ . ": Caught RedisException - " . $e->getMessage());
			return false;
		}

		return true;
	}

	/**
	 * Setter for environment key
	 *
	 * @param $environment
	 *
	 * @return void
	 */
	private function setEnvironment($environment) {
		$this->environment = $environment;
	}

	/**
	 * Getter for environment key.
	 *
	 * @return mixed
	 */
	private function getEnvironment() {
		return $this->environment;
	}

	/**
	 * Setup mapping function for domains
	 *
	 * @return array Domain mapping
	 */
	private function setupMapping() {
		self::$environmentMap = [
			'production'	=> Wiki\Domains::ENV_PRODUCTION,
			'staging'		=> Wiki\Domains::ENV_STAGING,
			'development'	=> Wiki\Domains::ENV_DEVELOPMENT,
		];
	}

	/**
	 * Update a wiki to be marked as installed.
	 *
	 * @param object Wiki
	 *
	 * @return void
	 */
	private function setWikiInstalled($wiki) {
		$wiki->setInstalled();
		$wiki->save();
	}

	/**
	 * Update last recache time on a wiki.
	 *
	 * @param object Wiki
	 *
	 * @return void
	 */
	private function updateLastRecache($wiki) {
		// Note: If the Wiki class has unintentionally changed data on the object it will get updated in the database now.
		// I discovered this when creating the db_server_replica functionality.
		// $wiki->getDatabase() would default db_server_replica to a copy of db_server if db_server_replica was empty.
		$wiki->setLastRecache(time());
		$wiki->save();
	}

	/**
	 * Return self::$tasks.
	 *
	 * @return array Tasks
	 */
	public static function getTasks() {
		return self::$tasks;
	}

	/**
	 * Set the logKey variable
	 *
	 * @param string $key
	 *
	 * @return void
	 */
	public function setLogKey($key) {
		$this->logKey = $key;
	}

	/**
	 * Setup environment
	 *
	 * @return void
	 */
	private function initializeEnv() {
		$this->setupMapping();

		if (!$_SERVER['PHP_ENV']) {
			$this->output("No PHP environment passed by the server.\n");
			return false;
		}

		$_environment = filter_var($_SERVER['PHP_ENV'], FILTER_SANITIZE_STRING);

		if (!array_key_exists($_environment, self::$environmentMap)) {
			$this->output("PHP environment is not on the list.\n");
			$this->jsonOutput['fatal'] = 'PHP environment is not on the list.';
			$this->sendFinalOutput();
			return false;
		}

		$this->setEnvironment(self::$environmentMap[$_environment]);

		if (($pathErrors = Sites::verifyHydraPaths()) !== true) {
			$this->output("\n\nThe following paths are not writable:\n");
			foreach ($pathErrors as $path) {
				$this->output("	 - {$path}\n");
			}
			$this->output("\nThese critical directory permissions must be fixed to continue.\n\n");
			$this->jsonOutput['fatal'] = 'Sites folder not writable.';
			$this->sendFinalOutput();
			return false;
		}

		$this->DB = DSDBFactory::getMasterDB(DB_MASTER);
		$this->redis = $this->getRedis();
	}

	/**
	 * Get redis connection, and initialize if not yet initialized.
	 *
	 * @return object RedisCache
	 */
	private function getRedis() {
		if (!$this->redis) {
			$this->redis = \RedisCache::getClient('cache');
		}
		return $this->redis;
	}

	/**
	 * Handle JSON output if requested,
	 * and also logs to redis if available
	 *
	 * @param string Output
	 * @param array Data array to merge into JSON output.
	 *
	 * @return void
	 */
	protected function output($output, $json = []) {
		if ($this->getOption('json')) {
			if ($this->getOption('jsonIncludeRaw')) {
				$this->jsonOutput['raw'] .= $output;
			}
		} else {
			if (!empty($this->logKey)) {
				try {
					$this->getRedis()->append($this->logKey, (string)$output);
				} catch (Exception $e) {
					// bubble up redis errors to the output class so we can find them later.
					parent::output($e->getMessage());
				}
			}
			parent::output($output);
		}
	}

	/**
	 * Output final output such as JSON.
	 *
	 * @return void Outputs to standard output.
	 */
	private function sendFinalOutput() {
		if ($this->getOption('json')) {
			echo json_encode($this->jsonOutput);
		}
	}
}

$maintClass = '\DynamicSettings\Tools';
require_once RUN_MAINTENANCE_IF_MAIN;
