<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Edit Maintenance Class
 *
 * @author    Alexia E. Smith
 * @copyright (c) 2017 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
**/

namespace DynamicSettings;

require_once dirname(__DIR__, 3) . "/maintenance/Maintenance.php";

class Edit extends \Maintenance {
	/**
	 * Constructor
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();

		$this->mDescription = "Edit a wiki's settings.";

		// Options for site selection.
		$this->addOption('wiki', 'Select a wiki by domain or key.', true, true);
		$this->addOption('wiki_name', '', false, true);
		$this->addOption('wiki_meta_name', '', false, true);
		$this->addOption('wiki_domain', '', false, true);
		$this->addOption('wiki_domain_local', '', false, true);
		$this->addOption('wiki_domain_staging', '', false, true);
		$this->addOption('wiki_domain_redirects', 'Comma delimited list of domain redirects.', false, true);
		$this->addOption('wiki_category', '', false, true);
		$this->addOption('wiki_tags', 'Comma delimited list of tags.', false, true);
		$this->addOption('wiki_managers', '', false, true);
		$this->addOption('wiki_language', '', false, true);
		$this->addOption('wiki_notes', '', false, true);
		$this->addOption('portal', '', false, true);
		$this->addOption('group', '', false, true);
		$this->addOption('group_file_repo', '', false, true);
		$this->addOption('use_s3', '', false, true);
		$this->addOption('db_type', '', false, true);
		$this->addOption('db_server', '', false, true);
		$this->addOption('db_port', '', false, true);
		$this->addOption('db_name', '', false, true);
		$this->addOption('db_user', '', false, true);
		$this->addOption('db_password', '', false, true);
		$this->addOption('search_type', '', false, true);
		$this->addOption('search_server', '', false, true);
		$this->addOption('search_port', '', false, true);
	}

	/**
	 * Main Executor
	 *
	 * @return void
	 */
	public function execute() {
		$status = new \StatusValue;

		$wiki = Wiki::loadFromHash($this->getOption('wiki'));
		if ($wiki === false) {
			$wiki = Wiki::loadFromDomain($this->getOption('wiki'));
		}

		if ($wiki === false) {
			$this->output("Site Domain Name not found.\n");
			return false;
		}

		if ($this->hasOption('wiki_name')) {
			$status->merge($wiki->setName($this->getOption('wiki_name')));
		}
		if ($this->hasOption('wiki_meta_name')) {
			$status->merge($wiki->setMetaName($this->getOption('wiki_meta_name')));
		}

		$originalDomains = [
			\DynamicSettings\Wiki\Domains::ENV_PRODUCTION => $wiki->getDomains()->getDomain(\DynamicSettings\Wiki\Domains::ENV_PRODUCTION),
			\DynamicSettings\Wiki\Domains::ENV_DEVELOPMENT => $wiki->getDomains()->getDomain(\DynamicSettings\Wiki\Domains::ENV_DEVELOPMENT),
			\DynamicSettings\Wiki\Domains::ENV_STAGING => $wiki->getDomains()->getDomain(\DynamicSettings\Wiki\Domains::ENV_STAGING)
		];

		if ($this->hasOption('wiki_domain')) {
			$status->merge($wiki->getDomains()->setDomain($this->getOption('wiki_domain'), \DynamicSettings\Wiki\Domains::ENV_PRODUCTION));
		}
		if ($this->hasOption('wiki_domain_local')) {
			$status->merge($wiki->getDomains()->setDomain($this->getOption('wiki_domain_local'), \DynamicSettings\Wiki\Domains::ENV_DEVELOPMENT));
		}
		if ($this->hasOption('wiki_domain_staging')) {
			$status->merge($wiki->getDomains()->setDomain($this->getOption('wiki_domain_staging'), \DynamicSettings\Wiki\Domains::ENV_STAGING));
		}

		if ($this->hasOption('wiki_domain_redirects')) {
			$domainRedirects = [];
			if (strlen(mb_strtolower($this->getOption('wiki_domain_redirects'), 'UTF-8'))) {
				$_domains = explode(",", $this->getOption('wiki_domain_redirects'));
				foreach ($_domains as $index => $domain) {
					$domain = trim($domain);
					if (empty($domain)) {
						continue;
					}
					$domainRedirects[$index] = $domain;
				}
			}
			$domainRedirects = array_unique($domainRedirects);
			$status->merge($wiki->getDomains()->setRedirects($domainRedirects));
		}

		if ($this->hasOption('wiki_category')) {
			$status->merge($wiki->setCategory($this->getOption('wiki_category')));
		}

		if ($this->hasOption('wiki_tags')) {
			if (strlen(mb_strtolower($this->getOption('wiki_tags'), 'UTF-8'))) {
				$tags = explode(',', $this->getOption('wiki_tags'));
				$status->merge($wiki->setTags($tags));
			} else {
				$status->merge($wiki->setTags([]));
			}
		}

		if ($this->hasOption('wiki_managers')) {
			$rawManagers = explode(',', $this->getOption('wiki_managers'));
			$rawManagers = array_map('trim', $rawManagers);
			$wiki->setManagers($rawManagers);
		}

		if ($this->hasOption('wiki_language')) {
			$status->merge($wiki->setLanguage($this->getOption('wiki_language')));
		}
		if ($this->hasOption('wiki_notes')) {
			$status->merge($wiki->setNotes($this->getOption('wiki_notes')));
		}
		if ($this->hasOption('portal')) {
			$status->merge($wiki->setPortalKey($this->getOption('portal')));
		}
		if ($this->hasOption('group')) {
			$status->merge($wiki->setGroupKey($this->getOption('group')));
		}
		if ($this->hasOption('group_file_repo')) {
			$status->merge($wiki->setGroupFileRepo(filter_var($this->getOption('group_file_repo'), FILTER_VALIDATE_BOOLEAN)));
		}
		if ($this->hasOption('use_s3')) {
			$status->merge($wiki->setUsingS3(filter_var($this->getOption('use_s3'), FILTER_VALIDATE_BOOLEAN)));
		}

		if ($this->hasOption('db_type') || $this->hasOption('db_server') || $this->hasOption('db_port') || $this->hasOption('db_name') || $this->hasOption('db_user') || $this->hasOption('db_password')) {
			$database = $wiki->getDatabase();
			if ($this->hasOption('db_type')) {
				$database['db_type'] = $this->getOption('db_type');
			}
			if ($this->hasOption('db_server')) {
				$database['db_server'] = $this->getOption('db_server');
			}
			if ($this->hasOption('db_port')) {
				$database['db_port'] = $this->getOption('db_port');
			}
			if ($this->hasOption('db_name')) {
				$database['db_name'] = $this->getOption('db_name');
			}
			if ($this->hasOption('db_user')) {
				$database['db_user'] = $this->getOption('db_user');
			}
			if ($this->hasOption('db_password')) {
				$database['db_password'] = $this->getOption('db_password');
			}
			$status->merge($wiki->setDatabase($database));
		}

		if ($this->hasOption('search_type') || $this->hasOption('search_server') || $this->hasOption('search_port')) {
			$search = $wiki->getSearchSetup();
			if ($this->hasOption('search_type')) {
				$search['search_type'] = $this->getOption('search_type');
			}
			if ($this->hasOption('search_server')) {
				$search['search_server'] = $this->getOption('search_server');
			}
			if ($this->hasOption('search_port')) {
				$search['search_port'] = $this->getOption('search_port');
			}
			$status->merge($wiki->setSearchSetup($search));
		}

		$commitMessage = trim($this->getOption('commit_message'));
		if (!$commitMessage) {
			$status->merge(\StatusValue::newFatal('error_no_commit_message'));
		}

		if ($status->isGood()) {
			if ($wiki->save($commitMessage)) {
				if (!$wiki->getDomains()->save()) {
					throw new \MWException('There was a fatal error saving the domain data for this wiki.');
				}

				$newDomains = [
					\DynamicSettings\Wiki\Domains::ENV_PRODUCTION => $wiki->getDomains()->getDomain(\DynamicSettings\Wiki\Domains::ENV_PRODUCTION),
					\DynamicSettings\Wiki\Domains::ENV_DEVELOPMENT => $wiki->getDomains()->getDomain(\DynamicSettings\Wiki\Domains::ENV_DEVELOPMENT),
					\DynamicSettings\Wiki\Domains::ENV_STAGING => $wiki->getDomains()->getDomain(\DynamicSettings\Wiki\Domains::ENV_STAGING)
				];

				foreach ($originalDomains as $key => $value) {
					if ($newDomains[$key] !== $value) {
						$wiki->getDomains()->logDomainChange($value, $newDomains[$key], $key);
					}
				}
			} else {
				throw new \MWException('There was a fatal error saving the data for this wiki.');
			}
		} else {
			$this->output("Errors found:\n");
			foreach ($status->getErrors() as $error) {
				$this->output(wfMessage($error['message']) . "\n");
			}
		}
	}
}

$maintClass = '\DynamicSettings\Edit';
require_once RUN_MAINTENANCE_IF_MAIN;
