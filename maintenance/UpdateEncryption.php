<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * UpdateEncryption Maintenance Class
 *
 * @author    Alexia E. Smith
 * @copyright (c) 2018 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
**/

namespace DynamicSettings;

require_once dirname(__DIR__, 3) . "/maintenance/Maintenance.php";

class UpdateEncryption extends \Maintenance {
	/**
	 * AES Key
	 *
	 * @var string
	 */
	private static $aesKey = '2f961810f01d35d42259b570260742dd';

	/**
	 * AES Initialization Vector
	 * DEPRECATED - Used for the old mcrypt_decrypt.
	 *
	 * @var string
	 */
	private static $AESIV = 'e351598c8171b9dda7d44be6423c4b35';

	/**
	 * Constructor
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();

		$this->mDescription = 'Update encryption for database details by simply resaving the wikis.  Run this script if \DynamicSettings\DS::$staticIV is changed.';
	}

	/**
	 * Main Executor
	 *
	 * @return boolean Found a valid task to complete.
	 */
	public function execute() {
		exit; // Short circuit to prevent accidental running.

		$db = DSDBFactory::getMasterDB(DB_MASTER);

		$results = $db->select(
			['wiki_sites'],
			[
				'md5_key',
				'db_server',
				'db_port',
				'db_name',
				'db_user',
				'db_password'
			],
			[],
			__METHOD__
		);

		while ($row = $results->fetchRow()) {
			$md5Key = $row['md5_key'];
			unset($row['md5_key']);
			foreach ($row as $key => $value) {
				$row[$key] = $this->decryptString($value);
			}
			$success = $db->update(
				'wiki_sites',
				$row,
				['md5_key' => $md5Key],
				__METHOD__
			);
			$this->output("Updating {$md5Key}... " . var_export($success, true) . "\n");
		}
	}

	/**
	 * DEPRECATED (mycrypt)
	 * Decrypt Secret String
	 *
	 * @param string Raw encrypted string
	 *
	 * @return string Decrypted String
	 */
	private function decryptString($text) {
		$text = base64_decode($text);
		$text = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, self::$aesKey, $text, MCRYPT_MODE_CBC, self::$AESIV);
		$text = rtrim($text, "\0");
		return $text;
	}
}

$maintClass = '\DynamicSettings\UpdateEncryption';
require_once RUN_MAINTENANCE_IF_MAIN;
