<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Generic Special Page
 *
 * @author    Noah Manneschmidt
 * @copyright (c) 2015 Curse Inc.
 * @license   Proprietary
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
**/

namespace DynamicSettings;

class SpecialPage extends \HydraCore\SpecialPage {
	/**
	 * Hides special page from SpecialPages special page.
	 *
	 * @return boolean
	 */
	public function isListed() {
		return Environment::isMasterWiki() && $this->getUser()->isAllowed($this->getRestriction()) && parent::isListed();
	}

	/**
	 * Lets others determine that this special page is restricted.
	 *
	 * @return boolean
	 */
	public function isRestricted() {
		return $this->getRestriction();
	}

	/**
	 * Return the group name for this special page.
	 *
	 * @return string
	 */
	protected function getGroupName() {
		return 'dynamicsettings';
	}

	/**
	 * Indicates whether this special page may perform database writes
	 *
	 * @return boolean
	 */
	public function doesWrites() {
		return true;
	}
}
