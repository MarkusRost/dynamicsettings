<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Wiki Promotions Special Page
 *
 * @author    Tim Aldridge
 * @copyright (c) 2014 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
 **/

use DynamicSettings\Lock;
use DynamicSettings\Sites;
use \DynamicSettings\Wiki\Promotion;

class SpecialWikiPromotions extends DynamicSettings\SpecialPage {
	/**
	 * Output HTML
	 *
	 * @var string
	 */
	private $content;

	/**
	 * The Type
	 *
	 * @var string
	 */
	public $type = 'promotion';

	/**
	 * Main Constructor
	 *
	 * @return void
	 */
	public function __construct($name = null, $restriction = null) {
		parent::__construct($name !== null ? $name : 'WikiPromotions', $restriction !== null ? $restriction : 'wiki_promotions');
	}

	/**
	 * Main Executor
	 *
	 * @param string Sub page passed in the URL.
	 *
	 * @return void [Outputs to screen]
	 */
	public function execute($subpage) {
		$this->checkPermissions();

		if (Lock::isLocked() == true) {
			$this->output->showErrorPage('dynamic_settings_error', 'error_settings_locked');
			return;
		}

		$this->siteSettings = new Sites();

		$this->templates = new TemplateWikiPromotions();

		$this->output->addModuleStyles(['ext.wikiPromotions.styles']);
		$this->output->addModules(['ext.wikiPromotions.scripts']);

		$this->setHeaders();

		switch ($this->wgRequest->getVal('section')) {
			default:
			case 'list':
				$this->wikiPromotionsList();
				break;
			case 'form':
				$this->wikiPromotionsForm();
				break;
			case 'delete':
				$this->wikiPromotionsDelete();
				break;
			case 'pause':
				$this->pausePromotion();
				break;
			case 'recache':
				$this->recache();
				break;
		}

		$this->output->addHTML($this->content);
	}

	/**
	 * Promotion List
	 *
	 * @return void [Outputs to screen]
	 */
	public function wikiPromotionsList() {
		$promotions = Promotion::loadAll('weight');

		if (is_array($promotions)) {
			foreach ($promotions as $promotionId => $promotion) {
				if ($promotion->getType() != $this->type) {
					unset($promotions[$promotionId]);
				}
			}
		}

		$this->output->setPageTitle(wfMessage('wiki' . $this->type . 's'));
		$this->content = $this->templates->wikiPromotionsList($promotions, $this->type);
	}

	/**
	 * Promotion Form
	 *
	 * @return void [Outputs to screen]
	 */
	public function wikiPromotionsForm() {
		if ($this->wgRequest->getInt('promotion_id')) {
			$promotionId = $this->wgRequest->getInt('promotion_id');

			// Time to grab all the promos and wiki sites so we can use them through out all the form stuff
			$this->promotion = Promotion::loadFromId($promotionId);

			if (!$this->promotion) {
				$this->output->showErrorPage($this->type . 's_error', 'error_no_' . $this->type);
				return;
			}
		} else {
			$this->promotion = Promotion::loadFromNew();
		}

		$errors = $this->wikiPromotionsSave();

		$_wikis = $this->promotion->getWikis();
		$wikis = [];
		if (is_array($_wikis) && count($_wikis)) {
			foreach ($_wikis as $data) {
				$key = null;
				if ($data['override'] == 1) {
					$key = 'added';
				} elseif ($data['override'] == -1) {
					$key = 'removed';
				} else {
					continue;
				}

				$wikis[$key][] = $data['site_key'];
			}
		}

		if ($this->promotion->getDatabaseId()) {
			$this->output->setPageTitle(wfMessage('edit_' . $this->type) . ' - ' . $this->promotion->getName());
		} else {
			$this->output->setPageTitle(wfMessage('add_' . $this->type));
		}
		$this->content = $this->templates->wikiPromotionsForm(
			$this->promotion,
			$wikis,
			$errors,
			$this->type
		);
	}

	/**
	 * Saves submitted Promotion Forms.
	 *
	 * @return array Array containing an array of processed form information and array of corresponding errors.
	 */
	private function wikiPromotionsSave() {
		$errors = [];
		if ($this->wgRequest->wasPosted() && $this->wgRequest->getVal('do') == 'save') {
			if (!$this->promotion->setName($this->wgRequest->getText('name'))) {
				$errors['name'] = wfMessage('error-' . $this->type . '_name-invalid');
			}

			if (!$this->promotion->setDescription($this->wgRequest->getText('description')) && $this->type === 'promotion') {
				$errors['description'] = wfMessage('error-' . $this->type . '_description-invalid');
			}

			if (!$this->promotion->setImage($save['image'] = $this->wgRequest->getText('image'))) {
				$errors['image'] = wfMessage('error-' . $this->type . '_image-invalid');
			}

			if (!$this->promotion->setLink($save['link'] = $this->wgRequest->getText('link'))) {
				$errors['link'] = wfMessage('error-' . $this->type . '_link-invalid');
			}

			$this->promotion->setLanguages($this->wgRequest->getText('languages'));

			$this->promotion->setBegins($this->wgRequest->getInt('begins'));

			$this->promotion->setExpires($this->wgRequest->getInt('expires'));
			if (($this->promotion->getExpires() < time() || $this->promotion->getExpires() < $this->promotion->getBegins()) && $this->promotion->getBegins() > 0) {
				$errors['expires'] = wfMessage('error-promotion_expires-invalid');
			}

			$this->promotion->setType($this->type);
			if ($this->wgRequest->getBool('isAd', false)) {
				$this->promotion->setType('advertisement');
			}

			$this->promotion->setEnabledEverywhere($this->wgRequest->getCheck('everywhere'));

			$wikis = $this->wgRequest->getVal('wikis');
			$wikis = @json_decode($wikis, true);

			$this->promotion->clearWikis();
			if (isset($wikis['added']) && is_array($wikis['added']) && count($wikis['added'])) {
				foreach ($wikis['added'] as $siteKey) {
					$this->promotion->addWiki($siteKey, 1);
				}
			}
			if (isset($wikis['removed']) && is_array($wikis['removed']) && count($wikis['removed'])) {
				foreach ($wikis['removed'] as $siteKey) {
					$this->promotion->addWiki($siteKey, -1);
				}
			}

			if (!count($errors)) {
				if ($this->promotion->save()) {
					$this->output->redirect($this->getPageTitle()->getFullURL());
					return;
				}
			}
		}
		return $errors;
	}

	/**
	 * Delete Promotion
	 *
	 * @return void [Outputs to screen]
	 */
	public function wikiPromotionsDelete() {
		$promotion = Promotion::loadFromId($this->wgRequest->getInt('promotion_id'));

		if (!$promotion) {
			$this->output->showErrorPage($this->type . 's_error', 'error_no_' . $this->type);
			return;
		}

		if ($this->wgRequest->wasPosted() && $this->wgRequest->getVal('do') == 'confirm') {
			$this->DB->startAtomic(__METHOD__);
			$this->DB->delete(
				'wiki_promotions',
				['pid' => $promotion->getDatabaseId()],
				__METHOD__
			);
			$this->DB->delete(
				'wiki_promotions_sites',
				['promotion_id' => $promotion->getDatabaseId()],
				__METHOD__
			);
			$this->DB->endAtomic(__METHOD__);

			$this->output->redirect($this->getPageTitle()->getFullURL());
		}

		$this->output->setPageTitle(wfMessage('delete_' . $this->type) . ' - ' . $promotion->getName());
		$this->content = $this->templates->wikiPromotionsDelete($promotion, $this->type);
	}

	/**
	 * Calls into recache in the siteSettings class.
	 *
	 * @return boolean
	 */
	public function recache() {
		if (!$this->wgUser->isAllowed('wiki_promotions_recache')) {
			throw new PermissionsError('wiki_promotions_recache');
		}

		$location = stristr($_SERVER['HTTP_REFERER'], 'WikiNotices') ? 'WikiNotices' : 'WikiPromotions';

		$parameters = [
			'task' => 'recachePromotions',
			'dist' => true
		];

		$promotionId = $this->wgRequest->getInt('promotion_id');
		if ($this->wgRequest->getInt('promotion_id') > 0) {
			$promotion = Promotion::loadFromId($this->wgRequest->getInt('promotion_id'));

			if (!$promotion) {
				throw new \ErrorPageError(wfMessage($this->type . 's_error'), wfMessage('error_no_' . $this->type));
			}
			if ($promotion->isEnabledEverywhere()) {
				$parameters['all'] = true;
			} else {
				$siteKeys = [];
				foreach ($promotion->getWikis() as $wiki) {
					$siteKeys[] = $wiki['site_key'];
				}
				$parameters['sitekey'] = implode(',', $siteKeys);
			}
		} else {
			$parameters['all'] = true;
		}

		ob_start();
		$html_errors = ini_get('html_errors');
		ini_set('html_errors', '0');
		try {
			$recache = new \DynamicSettings\Tools;
			$recache->loadParamsAndArgs('Tools', $parameters);
			$success = $recache->execute();
		} catch (Exception $e) {
			$recacheOut .= $e->getMessage() . "\n";
		}

		if ($success) {
			$this->output->setPageTitle(wfMessage('tools_recache_successful')->escaped());
		} else {
			$this->output->setPageTitle(wfMessage('tools_recache_error')->escaped());
		}

		ini_set('html_errors', $html_errors);
		$recacheOut .= ob_get_contents();
		ob_end_clean();

		$this->templates = new TemplateWikiSites();
		$this->content = $this->templates->runToolOutput($recacheOut, $location, 'recachePromotions');
	}

	/**
	 * Pause/Resume Promotion
	 *
	 * @return void
	 */
	public function pausePromotion() {
		$promotionId = $this->wgRequest->getInt('promotion_id');

		$this->promotion = Promotion::loadFromId($promotionId);

		if (!$this->promotion) {
			$this->output->showErrorPage($this->type . 's_error', 'error_no_' . $this->type);
			return;
		}

		$this->promotion->setPaused();
		$this->promotion->save();

		$this->output->redirect($this->getPageTitle()->getFullURL());
	}
}
