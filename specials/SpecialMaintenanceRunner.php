<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Maint Runner Special Page
 *
 * @author    Cameron Chunn
 * @copyright (c) 2018 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
**/

use DynamicSettings\Lock;

class SpecialMaintenanceRunner extends DynamicSettings\SpecialPage {
	/**
	 * Main Constructor
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct(
			'MaintenanceRunner', // name
			'maintenancerunner', // required user right
			false // display on Special:Specialpages
		);
	}

	/**
	 * Main Executor
	 *
	 * @param string Sub page passed in the URL.
	 *
	 * @return void [Outputs to screen]
	 */
	public function execute($path) {
		$this->template = new TemplateMaintenanceRunner();
		$this->wgRequest = $this->getRequest();
		$this->wgUser    = $this->getUser();
		$this->output    = $this->getOutput();

		$this->checkPermissions();

		$wiki = \DynamicSettings\Wiki::loadFromHash($this->wgRequest->getText('siteKey', ''));

		if (!$wiki) {
			$this->output->showErrorPage('maintenance_runner_error', 'wiki_not_found');
			return;
		}

		if (Lock::isLocked($wiki->getSiteKey())) {
			throw new \ErrorPageError(wfMessage('dynamic_settings_error'), wfMessage('error_wiki_locked'));
		}

		$title = wfMessage('maintenancerunner')->escaped();
		if ($wiki) {
			$title .= " - " . $wiki->getName();
		}

		$this->output->setPageTitle($title);
		$this->content = $this->template->maintenanceForm($wiki);
		$this->output->addHTML($this->content);
	}

	/**
	 * Return the group name for this special page.
	 *
	 * @return string
	 */
	protected function getGroupName() {
		return 'other'; // Change to display in a different category on Special:SpecialPages.
	}
}
