<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * ExtractFieldDataTrait Trait
 *
 * @author    Alexia Smith
 * @copyright (c) 2018 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
 **/

namespace DynamicSettings\Traits;

trait ExtractFieldDataTrait {
	/**
	 * Get API Exposed Fields
	 *
	 * @return array API Exposed Fields
	 */
	public function getApiExposedFields() {
		return $this->apiExposed;
	}

	/**
	 * Extra data off an object and return it.
	 *
	 * @param array [Optional] Limit fields to return.
	 *
	 * @return array Field Name => Field Data
	 */
	public function extractFieldData($requestedFields = null) {
		$validFields = $this->getApiExposedFields();
		$data = [];
		foreach ($validFields as $field => $functions) {
			if (is_array($requestedFields) && !in_array($field, $requestedFields)) {
				continue;
			}
			list($getter, $setter, $type, $permission) = $functions;
			if ($permission !== null && !$this->getUser()->isAllowed($permission)) {
				continue;
			}
			$data[$field] = $this->$getter();
		}
		return $data;
	}
}
