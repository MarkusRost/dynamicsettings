<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * GenerateAllWikiSitemapsJob
 *
 * @author    Cameron Chunn
 * @copyright (c) 2017 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
 **/

namespace DynamicSettings\Job;

use DynamicSettings\DSDBFactory;

class GenerateAllWikiSitemapsJob extends \SyncService\Job {
	/**
	 * Handles invoking job
	 *
	 * @param array not used
	 *
	 * @return integer exit value for this thread
	 */
	public function execute($args = []) {
		$db = DSDBFactory::getMasterDB(DB_MASTER);

		$results = $db->select(
			[
				'wiki_sites',
				'wiki_domains'
			],
			[
				'wiki_sites.*',
				'wiki_domains.*'
			],
			[
				'wiki_sites.deleted'	=> 0,
				'wiki_domains.type'		=> 1
			],
			__METHOD__,
			null,
			[
				'wiki_domains' => [
					'LEFT JOIN', 'wiki_domains.site_key = wiki_sites.md5_key'
				]
			]
		);

		while ($row = $results->fetchRow()) {
			$this->runUpdate($row['domain']);
		}
	}

	/**
	 * Run a Wiki Update
	 *
	 * @param string Domain to update
	 *
	 * @return void [Outputs to CLI]
	 */
	function runUpdate($domain = null) {
		global $IP;
		$cmd = "php {$IP}/extensions/DynamicSettings/maintenance/generateWikiSitemap.php " . escapeshellcmd($domain)
			 . " --fspath /media/hydra-media/sitemaps/" . escapeshellcmd($domain) . "/"
			 . " --urlpath https://" . escapeshellcmd($domain) . "/sitemaps/" . escapeshellcmd($domain) . "/";

		$output = shell_exec($cmd);
		if (strlen(trim($output)) > 0) {
			$this->outputLine($output);
		}
	}

	/**
	 * Return cron schedule if applicable.
	 *
	 * @return mixed False for no schedule or an array of schedule information.
	 */
	public static function getSchedule() {
		return [
			[
				'minutes' => '0',
				'hours' => '0',
				'days' => '*',
				'months' => '*',
				'weekdays' => '*',
				'arguments' => []
			]
		];
	}
}
