<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Domain Rename
 *
 * @author    Cameron Chunn
 * @copyright (c) 2016 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
 **/

namespace DynamicSettings;

class DomainRename {
	/**
	 * Wiki having its domain renamed.
	 *
	 * @var object
	 */
	private $wiki = null;

	/**
	 * Storage for RenameQueue functions.
	 *
	 * @var object
	 */
	private $renameQueue = [];

	/**
	 * Message output from the rename operation.
	 *
	 * @var string
	 */
	private $output = '';

	/**
	 * Main Constructor
	 *
	 * @param object $wiki Wiki
	 *
	 * @return void
	 */
	public function __construct(Wiki $wiki) {
		if (empty($wiki->getSiteKey())) {
			throw new \MWException('Missing site key for domains being renamed.');
		}
		$this->wiki = $wiki;
	}

	/**
	 * Calls into add using logEntry data.
	 *
	 * @param object $logEntry
	 *
	 * @return boolean
	 */
	public function addByLogEntry($logEntry) {
		if ($logEntry->getLogKey() !== $this->wiki->getSiteKey()) {
			throw new \MWException('Attempted to rename an unrelated wiki.');
		}
		$rename = $logEntry->getData();
		return $this->add($rename['old'], $rename['new'], $rename['type'], $logEntry->getTimestamp());
	}

	/**
	 * Add a rename instruction to the queue.
	 * Renames should be added in the order of oldest to newest as that is the ordering the renamer will use to figure out the oldest and newest domains.
	 *
	 * @param string       $oldDomain Old Domain
	 * @param string       $newDomain New Domain
	 * @param integer      $type      Domain type from Wiki\Domains.
	 * @param integer|null $sorting   [Optional] Give a value to enforce sorting on the rename queue.
	 *
	 * @return boolean Success
	 */
	protected function add($oldDomain, $newDomain, $type, $sorting = null) {
		if ($oldDomain === false && !empty($newDomain)) {
			// This is a brand new wiki.	 The old domain is boolean false and the new domain is filled out.
			// The old domain of false is returned from Domains::getDomain().  So return true here and skip adding it to the queue.
			return true;
		}
		if (($oldDomain !== false && empty($oldDomain)) || empty($newDomain) || !is_numeric($type)) {
			return false;
		}

		$rename = [
			'old' => $oldDomain,
			'new' => $newDomain,
			'type' => $type
		];

		if ($sorting !== null) {
			$this->renameQueue[$rename['type']][$sorting] = $rename;
			asort($this->renameQueue[$rename['type']]);
		} else {
			$this->renameQueue[$rename['type']][] = $rename;
		}

		return true;
	}

	/**
	 * Execute the Rename Queue
	 * Currently assumed a CLI output. May need
	 * to be modified for use elsewhere later.
	 *
	 * @return boolean True on success, False on error.
	 */
	public function execute() {
		$this->output = '';

		foreach ($this->renameQueue as $type => $rename) {
			$oldDomain = reset($rename)['old'];
			$newDomain = end($rename)['new'];

			if (empty($oldDomain) || empty($newDomain)) {
				throw new \MWException('Attempted to rename a wiki to or from a blank domain name.');
			}

			$this->output .= "\n-----------------------------------\n";
			$this->output .= "Domain Name Change Detected!\n";
			$this->output .= "$oldDomain -> $newDomain";
			$this->output .= "\n-----------------------------------\n";

			$oldCache = Sites::getSiteCachePath($oldDomain);
			$oldMedia = Sites::getSiteMediaPath($oldDomain);
			$newCache = Sites::getSiteCachePath($newDomain);
			$newMedia = Sites::getSiteMediaPath($newDomain);

			$issues = [];
			$actions = [];

			$this->output .= "\nCache Directory: $oldCache\n";
			if (file_exists($oldCache)) {
				$this->output .= " -> Exists.\n";
				if (file_exists($newCache)) {
					$this->output .= " -> COLISSION\n";
					$issues[] = "COLISSION: $newCache already exists, can't move $oldCache.\n";
				} else {
					$this->output .= " -> Queuing Move\n";
					$actions[] = ["move", $oldCache, $newCache];
				}
			} else {
				$this->output .= " -> Doesn't exist.\n";
			}

			$this->output .= "\nMedia Directory: $oldMedia\n";
			if (file_exists($oldMedia)) {
				$this->output .= " -> Exists.\n";
				if (file_exists($newMedia)) {
					$this->output .= " -> COLISSION!\n";
					$issues[] = "COLISSION: $newMedia already exists, can't move $oldMedia.\n";
				} else {
					$this->output .= " -> Queuing Move\n";
					$actions[] = ["move", $oldMedia, $newMedia];
				}
			} else {
				$this->output .= " -> Doesn't exist.\n";
			}

			if (count($issues)) {
				$this->output .= "\n\nThe following issues were encountered while attempting to rename domains:\n";
				foreach ($issues as $issue) {
					$this->output .= " - $issue\n";
				}
				$this->output .= "\nPlease manually fix these issues before proceeding.\n\n";
				return false;
			}

			// handle all queued up actions.
			if (count($actions)) {
				$this->output .= "\nRunning Actions:\n";
				foreach ($actions as $action) {
					$do = array_shift($action);
					switch ($do) {
						case "move":
							$this->output .= " -> Move {$action[0]} to {$action[1]}\n";
							rename($action[0], $action[1]);
						break;
						default:
							// Maybe we need more someday.
							// Who knows.
							// I just program stuff.
							$this->output .= "\nUnknown action \"$do\".\n";
						break;
					}
				}
			}
		}

		return true;
	}

	/**
	 * Get rename output.
	 *
	 * @return string Rename Output
	 */
	public function getOutput() {
		return $this->output;
	}
}
