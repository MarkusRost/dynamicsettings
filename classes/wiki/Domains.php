<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Domains Class
 *
 * @author    Alexia E. Smith
 * @copyright (c) 2014 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
 **/

namespace DynamicSettings\Wiki;

use DynamicSettings\DSDBFactory;
use DynamicSettings\EditLog;
use DynamicSettings\Environment;
use RequestContext;

class Domains {
	/**
	 * Production Environment Domain
	 *
	 * @var integer
	 */
	const ENV_PRODUCTION = 1;

	/**
	 * Staging Environment Domain
	 *
	 * @var integer
	 */
	const ENV_STAGING = 2;

	/**
	 * Development Environment Domain
	 *
	 * @var integer
	 */
	const ENV_DEVELOPMENT = 3;

	/**
	 * Local Environment Domain
	 *
	 * @var integer
	 */
	const ENV_LOCAL = 4;

	/**
	 * Redirect Domain
	 *
	 * @var integer
	 */
	const ENV_REDIRECT = 5;

	/**
	 * Domain information.
	 *
	 * @var array
	 */
	private $domains = [];

	/**
	 * Redirect information.
	 *
	 * @var array
	 */
	private $redirects = [];

	/**
	 * Wiki Object
	 *
	 * @var object
	 */
	private $wiki;

	/**
	 * Main Constructor
	 *
	 * @param object Wiki
	 *
	 * @return void
	 */
	private function __construct(\DynamicSettings\Wiki $wiki) {
		$this->wiki = $wiki;
	}

	/**
	 * Load a new Domain object from Wiki object.
	 *
	 * @param object Wiki
	 *
	 * @return mixed Domain object or false on failure.
	 */
	public static function loadFromWiki(\DynamicSettings\Wiki $wiki) {
		$domains = new Domains($wiki);
		$db = DSDBFactory::getMasterDB(DB_MASTER);
		$results = $db->select(
			['wiki_domains'],
			['*'],
			'site_key = "' . $db->strencode($wiki->getSiteKey()) . '"',
			__METHOD__
		);

		if ($results !== false && !$domains->load($results)) {
			$domains = false;
		}

		return $domains;
	}

	/**
	 * Load data on to this object.
	 *
	 * @param mixed Fetched database results.
	 *
	 * @return boolean Success
	 */
	public function load($results) {
		while ($row = $results->fetchRow()) {
			if ($row['type'] == self::ENV_REDIRECT) {
				$this->redirects[] = $row['domain'];
				continue;
			}
			$this->domains[intval($row['type'])] = $row['domain'];
		}
		return true;
	}

	/**
	 * Save the current information to the database.
	 *
	 * @return boolean Success
	 */
	public function save() {
		$db = DSDBFactory::getMasterDB(DB_MASTER);

		$success = false;

		if ($this->wiki->isReadOnly()) {
			return false;
		}

		$db->startAtomic(__METHOD__);

		// Clear out database first of existing entries for this wiki.
		$db->delete(
			'wiki_domains',
			['site_key' => $this->wiki->getSiteKey()],
			__METHOD__
		);

		$save = [];
		// If the array keys for one $save array gets modified it has to be modified below in redirects to match otherwise the database driver will not insert the data correctly.
		foreach ($this->domains as $type => $domain) {
			$save[] = [
				'domain'	=> $domain,
				'site_key'	=> $this->wiki->getSiteKey(),
				'type'		=> $type
			];
		}
		foreach ($this->redirects as $index => $domain) {
			$save[] = [
				'domain'	=> $domain,
				'site_key'	=> $this->wiki->getSiteKey(),
				'type'		=> self::ENV_REDIRECT
			];
		}

		if (count($save)) {
			$result = $db->insert(
				'wiki_domains',
				$save,
				__METHOD__
			);

			if (!$result) {
				$db->cancelAtomic(__METHOD__);
			} else {
				$success = true;
				$db->endAtomic(__METHOD__);
			}
		}

		return $success;
	}

	/**
	 * Log a domain name change.
	 *
	 * @param string Old Domain
	 * @param string New Domain
	 * @param integer Domain Type
	 *
	 * @return void
	 */
	public function logDomainChange($old, $new, $type) {
		$wgUser = RequestContext::getMain()->getUser();

		$data = [
			'old'	=> $old,
			'new'	=> $new,
			'type'	=> $type
		];

		$miscData = [
			'display_name'			=> $this->wiki->getNameForDisplay(),
		];

		EditLog::addEntry(
			$this->wiki->getSiteKey(), // Log Key: site_key
			$data,
			__METHOD__,
			$wgUser,
			(empty($old) ? "Set domain to {$new}" : "Changed domain {$old} to {$new}"),
			$miscData
		);
	}

	/**
	 * Get a domain for the specified domain type.  If no type is specified it returns the domain for the current environment.
	 *
	 * @param string Domain Type Constant
	 *
	 * @return mixed Valid string domain, false for an invalid environment, or false for an unset domain type.
	 */
	public function getDomain($type = false) {
		// An invalid constant will pass null.
		if ($type === false) {
			$type = self::getDomainEnvironment();
		}
		if ($type === null) {
			return false;
		}

		if (array_key_exists($type, $this->domains)) {
			return $this->domains[$type];
		} else {
			return false;
		}
	}

	/**
	 * Set domain for a certain type.
	 *
	 * @param string Domain
	 * @param string [Optional] Domain Type Constant
	 * @param boolean [Optional] Force set this domain even in use.  This is only used for Wiki::getFakeMainWiki().
	 *
	 * @return object StatusValue
	 */
	public function setDomain($domain, $type = false, $force = false) {
		$domain = mb_strtolower(trim($domain), 'UTF-8');

		$postFix = '';
		switch ($type) {
			case self::ENV_DEVELOPMENT:
				$postFix = '_local';
				break;
			case self::ENV_STAGING:
				$postFix = '_staging';
				break;
		}

		// An invalid constant will pass null.
		if ($type === null || empty($domain) || !filter_var('http://' . $domain, FILTER_VALIDATE_URL)) {
			return \StatusValue::newFatal('error-wiki_domain' . $postFix . '-invalid');
		}
		if ($type === false) {
			$type = self::getDomainEnvironment();
		}

		if ((!$force && $this->isDomainInUse($domain)) || (in_array($domain, $this->domains) && $this->domains[intval($type)] != $domain)) {
			return \StatusValue::newFatal('error-wiki_domain' . $postFix . '-inuse');
		}

		$this->domains[intval($type)] = $domain;

		return \StatusValue::newGood();
	}

	/**
	 * Get all the redirect domains.
	 *
	 * @return array
	 */
	public function getRedirects() {
		return $this->redirects;
	}

	/**
	 * Set the domains to redirect to the current environment's domain.
	 *
	 * @param array List of domains to redirect.
	 *
	 * @return object StatusValue
	 */
	public function setRedirects($redirects) {
		if (!is_array($redirects)) {
			throw new Exception(__METHOD__ . ": Non-array value passed.");
		}

		foreach ($redirects as $key => $redirect) {
			$redirect = trim($redirect);

			if (empty($redirect)) {
				unset($redirects[$key]);
				continue;
			}

			if (!filter_var('http://' . $redirect, FILTER_VALIDATE_URL)) {
				return \StatusValue::newFatal('error-wiki_domain_redirects-invalid');
			}

			// Check if it is in use by another site, in use as a regular domain, or a duplicate in the redirect list.
			if ($this->isDomainInUse($redirect) || in_array($redirect, $this->domains)) {
				// Return false even if one of the domains is in use.
				return \StatusValue::newFatal('error-wiki_domain_redirects-inuse');
			}

			$redirects[$key] = $redirect;
		}

		$this->redirects = $redirects;
		$this->redirects = array_unique($this->redirects);

		return \StatusValue::newGood();
	}

	/**
	 * Look up the domain name in the database and determine if it is in use.
	 *
	 * @param string Valid domain name to look up.
	 *
	 * @return boolean In Use or Not
	 */
	private function isDomainInUse($domain) {
		$config = \ConfigFactory::getDefaultInstance()->makeConfig('main');
		$wgDSMasterDomains = $config->get('DSMasterDomains');

		$domain = mb_strtolower($domain, "UTF-8");

		$db = DSDBFactory::getMasterDB(DB_MASTER);

		$result = $db->select(
			['wiki_domains', 'wiki_sites'],
			['wiki_domains.*, wiki_sites.deleted'],
			[
				"site_key != " . $db->addQuotes($this->wiki->getSiteKey()),
				"domain" => $domain
			],
			__METHOD__,
			[
				'LIMIT' => 1
			],
			[
				'wiki_sites' => [
					'INNER JOIN', 'wiki_sites.md5_key = wiki_domains.site_key'
				]
			]
		);
		if ($db->numRows($result) > 0) {
			return true;
		}

		if (is_array($wgDSMasterDomains) && in_array($domain, $wgDSMasterDomains)) {
			return true;
		}

		return false;
	}

	/**
	 * Look for conflicts with this wiki's domains in the database.
	 * Primarily used when undeleting wikis.
	 *
	 * @return array Array of potential conflicts; empty if there are none.
	 */
	public function checkForConflicts() {
		$lookups = array_merge($this->domains, $this->redirects);

		$db = DSDBFactory::getMasterDB(DB_MASTER);

		$results = $db->select(
			['wiki_domains', 'wiki_sites'],
			['wiki_domains.*, wiki_sites.deleted'],
			[
				"site_key != " . $db->addQuotes($this->wiki->getSiteKey()),
				"domain" => $lookups
			],
			__METHOD__,
			[
				'LIMIT' => 1
			],
			[
				'wiki_sites' => [
					'INNER JOIN', 'wiki_sites.md5_key = wiki_domains.site_key'
				]
			]
		);

		$conflicts = [];
		while ($row = $results->fetchRow()) {
			$conflicts[] = [
				'domain' => $row['domain'],
				'site_key' => $row['site_key'],
				'type' => $row['type'],
				'deleted' => $row['deleted']
			];
		}
		return $conflicts;
	}

	/**
	 * Gets the current domain environment.
	 *
	 * @return integer Class Constant
	 */
	public static function getDomainEnvironment() {
		switch (Environment::detectEnvironment()) {
			default:
			case 'development':
				return self::ENV_DEVELOPMENT;
				break;
			case 'staging':
				return self::ENV_STAGING;
				break;
			case 'production':
				return self::ENV_PRODUCTION;
				break;
		}
	}
}
