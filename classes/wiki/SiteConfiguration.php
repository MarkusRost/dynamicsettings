<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * SiteConfiguration Override
 *
 * @author    Alexia E. Smith
 * @copyright (c) 2016 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
 **/

namespace \DynamicSettings\Wiki;

// Provide an override for MediaWiki's build in SiteConfiguration class.
class SiteConfiguration extends \SiteConfiguration {
	/**
	 * Retrieves an array of local databases
	 *
	 * @return array
	 */
	public function &getLocalDatabases() {
		return $this->wikis;
	}

	/**
	 * Work out the site and language name from a database name
	 *
	 * @param string $db
	 *
	 * @return array
	 */
	public function siteFromDB($db) {
		// Allow override
		$def = $this->getWikiParams($db);
		if (!is_null($def['suffix']) && !is_null($def['lang'])) {
			return [$def['suffix'], $def['lang']];
		}

		$site = null;
		$lang = null;
		foreach ($this->suffixes as $altSite => $suffix) {
			if ($suffix === '') {
				$site = '';
				$lang = $db;
				break;
			} elseif (substr($db, -strlen($suffix)) == $suffix) {
				$site = is_numeric($altSite) ? $suffix : $altSite;
				$lang = substr($db, 0, strlen($db) - strlen($suffix));
				break;
			}
		}
		$lang = str_replace('_', '-', $lang);
		return [$site, $lang];
	}

	/**
	 * Since we use Redis it is all loaded already.
	 *
	 * @return void
	 */
	public function loadFullData() {
		$this->fullLoadDone = true;
	}
}
