<?php
/**
 * DynamicSettings
 * Wiki API
 *
 * @author    Alexia E. Smith
 * @copyright (c) 2014 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   DynamicSettings
 * @link      https://gitlab.com/hydrawiki
 **/

namespace DynamicSettings\Api;

use DynamicSettings\Environment;
use DynamicSettings\Wiki;

class WikiApi extends \ApiBase {
	/**
	 * Main Executor
	 *
	 * @return void [Outputs to screen]
	 */
	public function execute() {
		if (!Environment::isMasterWiki()) {
			$this->dieWithError(['apierror-permissiondenied-generic']);
			return;
		}

		$this->params = $this->extractRequestParams();

		$lookup = \CentralIdLookup::factory();
		$globalId = $lookup->centralIdFromLocalUser($this->getUser());
		if (!$this->getUser()->isLoggedIn() || !$globalId) {
			$this->dieUsageMsg(['invaliduser', $this->params['do']]);
		}

		switch ($this->params['do']) {
			case 'getWiki':
				$response = $this->getWiki();
				break;
			default:
				$this->dieUsageMsg(['invaliddo', $this->params['do']]);
				break;
		}

		foreach ($response as $key => $value) {
			$this->getResult()->addValue(null, $key, $value);
		}
	}

	/**
	 * Requirements for API call parameters.
	 *
	 * @return array Merged array of parameter requirements.
	 */
	public function getAllowedParams() {
		return [
			'do' => [
				\ApiBase::PARAM_TYPE		=> 'string',
				\ApiBase::PARAM_REQUIRED => true
			],
			'search' => [
				\ApiBase::PARAM_TYPE		=> 'string',
				\ApiBase::PARAM_REQUIRED => false
			],
			'site_key' => [
				\ApiBase::PARAM_TYPE		=> 'string',
				\ApiBase::PARAM_REQUIRED => false
			],
			'site_keys' => [
				\ApiBase::PARAM_TYPE		=> 'string',
				\ApiBase::PARAM_REQUIRED => false
			],
			'fields' => [
				\ApiBase::PARAM_TYPE		=> 'string',
				\ApiBase::PARAM_REQUIRED => false
			],
			'domain' => [
				\ApiBase::PARAM_TYPE		=> 'string',
				\ApiBase::PARAM_REQUIRED => false
			]
		];
	}

	/**
	 * Descriptions for API call parameters.
	 *
	 * @return array Merged array of parameter descriptions.
	 */
	public function getParamDescription() {
		return [
			'do'		=> 'Action to take.',
			'search'	=> 'Search term to use for finding wikis.',
			'site_keys'	=> 'Site keys of the wikis to load.  Used for multiple wiki look up only.  Maximum 100 at a time.',
			'site_key'	=> 'Site key of the wiki to load.'
		];
	}

	/**
	 * Merge API errors into the mediawiki error array.
	 *
	 * @return array
	 */
	public function getPossibleErrors() {
		return array_merge(
			parent::getPossibleErrors(),
			[
				[
					'code' => 'NoSiteKeys',
					'info' => 'The site_key parameter was not set.'
				],
				[
					'code' => 'NoDomain',
					'info' => 'No domain was passed.'
				]
			]
		);
	}

	/**
	 * Search for possible wikis to return.
	 *
	 * @return array API result.
	 */
	public function getWiki() {
		$wikis = [];
		if (!empty($this->params['search'])) {
			$wikis = Wiki::loadFromSearch(0, 1000, $this->params['search'], 'wiki_name', 'ASC', true);
		} elseif (!empty($this->params['site_keys'])) {
			$siteKeys = explode(',', $this->params['site_keys']);
			$siteKeys = array_map('trim', $siteKeys);
			if (count($siteKeys) > 100) {
				$siteKeys = array_slice($siteKeys, 0, 100);
			}
			$wikis = Wiki::loadFromHash($siteKeys);
		}

		if (empty($wikis)) {
			return ['success' => false, 'data' => []];
		}

		foreach ($wikis as $siteKey => $wiki) {
			if ($wiki->isDeleted()) {
				unset($wikis[$siteKey]);
				continue;
			}
			$wikiData[$siteKey] = $wiki->extractFieldData();
		}

		return ['success' => true, 'data' => $wikiData];
	}

	/**
	 * Get the list of extensions for a wiki.
	 *
	 * @return array API result.
	 */
	public function getExtensions() {
		return ['success' => true, 'data' => $extensionData];
	}

	/**
	 * Extra data off an object and return it.
	 *
	 * @return void
	 */
	private function extractFieldData($object) {
		$validFields = $object->getApiExposedFields();
		$data = [];
		foreach ($validFields as $field => $functions) {
			list($getter, $setter, $type, $permission) = $functions;
			if ($permission !== null && !$this->getUser()->isAllowed($permission)) {
				continue;
			}
			$data[$field] = $object->$getter();
		}
		return $data;
	}

	/**
	 * Get version of this API Extension.
	 *
	 * @return string API Extension Version
	 */
	public function getVersion() {
		return '1.0';
	}
}
