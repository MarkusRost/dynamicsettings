<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * ArrayObject class that passes a wiki context on to any iterators or filtered versions.
 * Forms a base class for ExtensionCollection and SettingCollection
 *
 * @author    Noah Manneschmidt
 * @copyright (c) 2015 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
 **/

namespace DynamicSettings;

abstract class WikiContextArrayObject extends \ArrayObject {
	use \DynamicSettings\Traits\WikiContext;

	protected static $iteratorClass = 'ArrayIterator';

	public function debugDump() {
		var_export($this->getArrayCopy()) . "\n";
	}

	/**
	 * Override default constructor to allow passing in an unmodified database result set
	 *
	 * @param mixed   $input
	 * @param integer $flags
	 * @param string  $iterator_class
	 */
	public function __construct($input = [], $flags = 0, $iterator_class = '') {
		// allow passing a raw database return set (false|ResultWrapper)
		if ($input === false) {
			$input = [];
		} elseif (is_object($input) && get_class($input) == 'ResultWrapper') {
			$input = [];
			foreach ($results as $row) {
				$this[$row->md5_key] = (array)$row;
			}
		}
		if (empty($iterator_class)) {
			$self = get_class($this);
			$iterator_class = $self::$iteratorClass;
		}
		parent::__construct($input, $flags, $iterator_class);
	}

	/**
	 * Pass wiki context on to any generated iterators
	 *
	 * @return object Traversible
	 */
	public function getIterator() {
		$ret = parent::getIterator();
		// pass context on to iterator
		$ret->setWikiContext($this->wikiContext);
		return $ret;
	}

	/**
	 * Merge the array-like data into the current data set
	 *
	 * @param array|ArrayObject $arrayish
	 *
	 * @return void
	 * @throws \InvalidArgumentException
	 */
	public function merge($arrayish) {
		if ($arrayish instanceof \ArrayObject) {
			$arrayish = $arrayish->getArrayCopy();
		} elseif (!is_array($arrayish)) {
			throw new \InvalidArgumentException('Only arrays and ArrayObjects can be included');
		}
		$internal = $this->getArrayCopy();
		$this->exchangeArray(array_merge($internal, $arrayish));
	}

	/**
	 * Filter the contents with a given callback
	 *
	 * @return object new self
	 */
	public function filter(callable $cb) {
		$self = get_class($this);
		$ret = new $self(array_filter($this->getArrayCopy(), $cb));
		$ret->setWikiContext($this->wikiContext);
		return $ret;
	}

	/**
	 * Get a new ArrayObject with a slice of the current items
	 *
	 * @see   array_slice
	 * @param integer $offset
	 * @param integer $length
	 *
	 * @return object self
	 */
	public function slice($offset, $length = null) {
		$self = get_class($this);
		$ret = new $self(array_slice($this->getArrayCopy(), $offset, $length, true));
		$ret->setWikiContext($this->wikiContext);
		return $ret;
	}

	/**
	 * Filter the contents down to those where any of the given fields match the given search term
	 *
	 * @param array  $fields string name of fields within which to search
	 * @param string $term
	 *
	 * @return object self
	 * @throws \InvalidArgumentException
	 */
	public function search(array $fields, $term) {
		if (!is_string($term)) {
			throw new \InvalidArgumentException('Second argument $term must be a string');
		}
		$term = mb_strtolower($term, 'UTF-8');
		return $this->filter(function ($item) use ($fields, $term) {
			foreach ($fields as $field) {
				$legacyToFunction = 'get' . str_replace(' ', '', (ucwords(str_replace('_', ' ', $field))));
				if (is_array($item)) {
					$fieldVal = $item[$field];
				} else {
					if (!method_exists($item, $field)) {
						$fieldVal = $item->$legacyToFunction();
					} else {
						$fieldVal = $item->$field();
					}
				}
				if (is_array($fieldVal)) {
					$fieldVal = implode(',', $fieldVal);
				}
				$fieldVal = mb_strtolower($fieldVal, 'UTF-8');
				if (strpos($fieldVal, $term) !== false) {
					return true;
				}
			}
			return false;
		});
	}

	/**
	 * Sort the contents by the given field
	 *
	 * @param string $field      to sort by
	 * @param string $sortOption sort function to use, of 'natcasesort' (default) or 'asort'
	 *
	 * @return object self
	 */
	public function sort($field, $reverse = false, $sortOption = 'natcasesort') {
		if ($sortOption == 'asort') {
			$compFunc = 'strcasecmp';
		} else {
			$compFunc = 'strnatcasecmp';
		}
		$this->uasort(function ($a, $b) use ($compFunc, $field, $reverse) {
			$legacyToFunction = 'get' . str_replace(' ', '', (ucwords(str_replace('_', ' ', $field))));
			if (is_array($a)) {
				$aValue = $a[$field];
			} else {
				if (!method_exists($a, $field)) {
					$aValue = $a->$legacyToFunction();
				} else {
					$aValue = $a->$field();
				}
			}
			if (is_array($b)) {
				$bValue = $b[$field];
			} else {
				if (!method_exists($b, $field)) {
					$bValue = $b->$legacyToFunction();
				} else {
					$bValue = $b->$field();
				}
			}
			$cmp = $compFunc($aValue, $bValue);
			if ($reverse) {
				$cmp = -$cmp;
			}
			return $cmp;
		});
		return $this;
	}
}
