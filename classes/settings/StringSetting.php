<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Allowed Setting class for strings
 *
 * @author    Noah Manneschmidt
 * @copyright (c) 2015 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
 **/

namespace DynamicSettings\Settings;

class StringSetting extends Setting {
	/**
	 * Variables that are allowed to be interpolated in strings.
	 *
	 * @var array
	 */
	static private $allowedVarInterp = [
		'$IP',
		'$wgServerName',
		'$wgUploadPath'
	];

	/**
	 * @return string of php code for assigning/defining this setting for the current wiki context
	 */
	public function getExportedCode() {
		return $this->data['setting_key'] . ' = ' . $this->interpolateVariables(var_export($this->getValue(), true)) . ';';
	}

	/**
	 * Validates the given value against the current setting type
	 *
	 * @param string Potential value for this setting.
	 *
	 * @return boolean
	 */
	public function isValid($value) {
		if (is_numeric($value)) {
			$value = strval($value);
		}
		return is_string($value);
	}

	/**
	 * Replace variables from an allowed list inside strings.
	 *
	 * @access private
	 * @param  string $string String to replace variables inside of.
	 * @return string
	 */
	private function interpolateVariables($string) {
		foreach (self::$allowedVarInterp as $variable) {
			$search = '{' . $variable . '}';
			$replace = "' . {$variable} . '";
			if (strpos($string, $search) !== false) {
				$string = str_replace($search, $replace, $string);
			}
		}
		return $string;
	}
}
