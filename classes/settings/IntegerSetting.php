<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Allowed Setting class for integers
 *
 * @author    Noah Manneschmidt
 * @copyright (c) 2015 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
 **/

namespace DynamicSettings\Settings;

class IntegerSetting extends Setting {
	/**
	 * Validates the given value against the current setting type
	 *
	 * @param string Potential value for this setting.
	 *
	 * @return boolean
	 */
	public function isValid($value) {
		return is_int($value);
	}

	/**
	 * Force integer type on the text input.
	 *
	 * @param string Form Value
	 *
	 * @return integer
	 */
	protected function decodeFormString($value) {
		return intval($value);
	}
}
