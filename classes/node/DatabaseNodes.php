<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * DatabaseNodes Class
 *
 * @author    Cameron Chunn
 * @copyright (c) 2016 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
 **/

namespace DynamicSettings\Node;

class DatabaseNodes {
	/**
	 * Output HTML
	 *
	 * @var string
	 */
	private $config;

	/**
	 * setup initial config variables.
	 */
	public function __construct() {
		$cf = \ConfigFactory::getDefaultInstance()->makeConfig('main');

		// load configuration
		$configs = $cf->get('DSInstallDBNodeConfig');
		$useConfig = $cf->get('DSInstallDBUseNodeConfig');
		if (isset($configs[$useConfig])) {
			$this->config = $configs[$useConfig];
		} else {
			// default to first config if the use config doesn't exist.
			$this->config = $configs[0];
		}

		// Setup database node password.
		$DBUser = $cf->get('DSInstallDBUser');
		$this->config['nodeUser'] = $DBUser['user'];
		$this->config['nodePassword'] = $DBUser['password'];
	}

	/**
	 * Call the defined worker's API too run the getAvailableNodes() function on the worker.
	 *
	 * @return array Nodes
	 */
	public function getAvailableNodesFromWorker() {
		global $wgServerName, $wgDSHydraHttpAuthInfo;

		$key = DatabaseNodesApi::currentKey();

		$curl = curl_init();

		curl_setopt_array(
			$curl,
			[
				CURLOPT_RETURNTRANSFER 	=> 1,
				CURLOPT_URL				=> 'https://' . $this->getWorkerNodeAddress() . '/api.php?action=databasenodes&format=json&key=' . $key,
				CURLOPT_HTTPHEADER		=> ['Host: ' . $wgServerName],
				CURLOPT_SSL_VERIFYHOST	=> false, // @TODO: Not fine if going over the internet.
				CURLOPT_SSL_VERIFYPEER	=> false // @TODO: Not fine if going over the internet.
			]
		);

		if (!empty($wgDSHydraHttpAuthInfo)) {
			curl_setopt($curl, CURLOPT_USERPWD, $wgDSHydraHttpAuthInfo);
		}

		$result = curl_exec($curl);

		$error = ['error' => "Couldn't get node list from worker"];
		if ($result === false) {
			$result = ['error' => curl_error($curl)];
		} else {
			try {
				$result = json_decode($result, 1);
			} catch (Exception $e) {
				$result = $error;
			}
			if (!is_array($result)) {
				$result = $error;
			}
		}

		curl_close($curl);

		return $result;
	}

	/**
	 * Get available nodes
	 *
	 * @return array Nodes
	 */
	public function getAvailableNodes() {
		$nodes = [];

		for ($i = 1; $i <= $this->config['nodeCount']; $i++) {
			if (!in_array($i, $this->config['skipNodes'])) {
				$nodes[$i] = $this->getNodeCount($i);
			}
		}

		foreach ($nodes as $n => $a) {
			$nodes[$n] = [
				'node_number' => $n,
				'node' => $this->getNodeName($n),
				'count' => $a !== null ? $a : 0,
				'free' => $a !== null ? ($this->config['wikisPerNode'] - $a) : 0
			];
		}

		usort($nodes, function ($node1, $node2) {
			return $node1['free'] <= $node2['free'];
		});

		return $nodes;
	}

	/**
	 * Get number of databases currently on a node.
	 *
	 * @param integer $nodeNumber the node number
	 *
	 * @return integer|null Available nodes or null for connection error.
	 */
	public function getNodeCount($nodeNumber) {
		$db = $this->getNodeDatabase($nodeNumber);
		if ($db) {
			$dbs = $db->query('show databases');
			$count = 0;
			foreach ($dbs as $db) {
				if (!in_array($db->Database, ['mysql', 'percona', 'performance_schema', 'information_schema'])) {
					$count++;
				}
			}
			return $count;
		}
		return null;
	}

	/**
	 * Get connection to a node's database.
	 *
	 * @param integer $nodeNumber the node number
	 *
	 * @return mixed Database Connection or false on exception.
	 */
	private function getNodeDatabase($nodeNumber) {
		global $wgExternalServers;

		$node = $this->getNodeName($nodeNumber);

		$services = \MediaWiki\MediaWikiServices::getInstance();
		$config = $services->getMainConfig();

		$dbDriver = $config->get('DSDBMySQLDriver');

		$wgExternalServers[$node] = [
			[
				'host'		=> $this->getNodeHost($nodeNumber),
				'user'		=> $this->config['nodeUser'],
				'password'	=> $this->config['nodePassword'],
				'type'		=> 'mysql',
				'driver'	=> $dbDriver,
				'load'		=> 1
			]
		];

		try {
			// This code is copied from ServiceWiring get around shitty MediaWikiServices singleton issues.
			$lbConf = \MWLBFactory::applyDefaultConfig(
				$config->get('LBFactoryConf'),
				$config,
				$services->getConfiguredReadOnlyMode()
			);
			$class = \MWLBFactory::getLBFactoryClass($lbConf);

			$lb = new $class($lbConf);
			$conn = $lb->newExternalLB($node)->getConnection(DB_REPLICA, false, 'mysql');
		} catch (\DBConnectionError $e) {
			$conn = false;
		}

		return $conn;
	}

	/**
	 * Return the host + port number of a node.
	 *
	 * @param integer $this->config['templates'][$id]
	 *
	 * @return string
	 */
	private function getNodeHost($nodeNumber) {
		$address = $this->templateParse('dbAddress', $nodeNumber);
		$port = $this->templateParse('dbPort', $nodeNumber);
		return $address . ":" . $port;
	}

	/**
	 * Get the name of the node.
	 *
	 * @param [type] $nodeNumber [description]
	 *
	 * @return [type] [description]
	 */
	private function getNodeName($nodeNumber) {
		return $this->templateParse('name', $nodeNumber);
	}

	/**
	 * Get the address for connecting to the worker node.
	 *
	 * @return string Worker Node Address
	 */
	private function getWorkerNodeAddress() {
		return $this->templateParse('workerNode');
	}

	/**
	 * Get a string with the correct number padding for a provided node number.
	 *
	 * @param integer $nodeNumber a node
	 * @param integer $len        length the string needs to be
	 *
	 * @return string
	 */
	private function getNodeNumberString($nodeNumber, $len = 2) {
		return str_pad($nodeNumber, $len, '0', STR_PAD_LEFT);
	}

	/**
	 * Parse a template and fill in needed values.
	 *
	 * @param string $str                            the string we are parsing
	 * @param string $this->config['templates'][$id]
	 *
	 * @return string
	 */
	private function templateParse($id, $nodeNumber = false) {
		$str = isset($this->config['templates'][$id]) ? $this->config['templates'][$id] : "";
		if (empty($str)) {
			return "";
		}
		$nodeStr2 = $nodeNumber ? $this->getNodeNumberString($nodeNumber, 2) : "";
		$nodeStr3 = $nodeNumber ? $this->getNodeNumberString($nodeNumber, 3) : "";
		$env = (isset($_SERVER['PHP_ENV']) && $_SERVER['PHP_ENV'] == 'development') ? "dev" : "live";
		return str_replace(["{node%1}", "{node%2}", "{node%3}", "{env}"], [$nodeNumber, $nodeStr2, $nodeStr3, $env], $str);
	}

	/**
	 * Get configuration.
	 *
	 * @return array
	 */
	public function getConfig($stripPasswords = false) {
		$config = $this->config;
		if ($stripPasswords) {
			unset($config['nodePassword']);
		}
		return $config;
	}
}
