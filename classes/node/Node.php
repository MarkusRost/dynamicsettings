<?php
/**
 * Curse Inc.
 * Dynamic Settings
 *
 * @author    Alexia E. Smith
 * @copyright (c) 2016 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
 **/

namespace DynamicSettings;

class Node {
	/**
	 * Node information loaded from storage.
	 *
	 * @var array
	 */
	private $node = [];

	/**
	 * Node information loaded from storage.
	 *
	 * @var array
	 */
	private $nodeType = self::NODE_GENERIC;

	/**
	 * Generic Node
	 *
	 * @var integer
	 */
	const NODE_GENERIC = 1;

	/**
	 * Web Node
	 *
	 * @var integer
	 */
	const NODE_WEB = 2;

	/**
	 * Cache(Varnish, Squid) Node
	 *
	 * @var integer
	 */
	const NODE_CACHE = 3;

	/**
	 * Database Node
	 *
	 * @var integer
	 */
	const NODE_DATABASE = 4;

	/**
	 * Services(Node.js and more) Node
	 *
	 * @var integer
	 */
	const NODE_SERVICES = 5;

	/**
	 * Memory Store Node
	 *
	 * @var integer
	 */
	const NODE_MEMSTORE = 6;

	/**
	 * Get a new Node instance for the specified type.
	 *
	 * @return mixed Initialized object.
	 */
	public static function newFromType($type) {
		$node = new self();
		$node->nodeType = $type;

		return $node;
	}

	/**
	 * Return Node objects for all nodes of a certain type.
	 *
	 * @param integer One of the defined node type constants.
	 *
	 * @return array Array of nodes.
	 */
	public static function getAllNodesOfType($type) {
		$redis = \RedisCache::getClient('cache');

		$_type = intval($type);

		if (empty($_type) || $_type < 1) {
			throw new \MWException(__METHOD__ . ": Invalid node type of {$_type} passed.");
		}

		$rawNodes = $redis->sMembers('nodes:' . $_type);

		$nodes = [];
		foreach ($rawNodes as $node) {
			$_node = self::newFromType($_type);
			$_node->setHostName($node);
			$nodes[] = $_node;
		}

		return $nodes;
	}

	/**
	 * Return the pretty host name for the node.
	 *
	 * @return mixed String or false if not set.
	 */
	public function getHostName() {
		return (isset($this->node['host']) ? $this->node['host'] : false);
	}

	/**
	 * Set the host name for this node.
	 *
	 * @param string Host Name
	 *
	 * @return void
	 */
	public function setHostName($host) {
		$this->node['host'] = $host;
	}

	/**
	 * Return the pretty host name for the node.
	 *
	 * @return mixed String or false if not set.
	 */
	public function getIPAddress() {
		if (isset($this->node['ip']) && filter_var($this->node['ip'], FILTER_VALIDATE_IP)) {
			return $this->node['ip'];
		}
		$ip = gethostbyname($this->node['host']);
		if (filter_var($ip, FILTER_VALIDATE_IP)) {
			$this->node['ip'] = $ip;
			return $this->node['ip'];
		}
		return false;
	}

	/**
	 * Ping this node type out to be added to the list.
	 *
	 * @return void
	 */
	public function ping() {
		$redis = \RedisCache::getClient('cache');

		if (empty($this->nodeType) || $this->nodeType < 1) {
			throw new \MWException(__METHOD__ . ": Invalid node type of {$_type} configured.");
		}

		if (empty($this->node['host'])) {
			throw new \MWException(__METHOD__ . ": Invalid host name \"{$this->node['host']}\" set.");
		}

		$redis->sAdd('nodes:' . $this->nodeType, $this->node['host']);
	}

	/**
	 * Purge this host from Redis.
	 *
	 * @return void
	 */
	public function purge() {
		$redis = \RedisCache::getClient('cache');

		$redis->sRem('nodes:' . $this->nodeType, $this->node['host']);
	}
}
