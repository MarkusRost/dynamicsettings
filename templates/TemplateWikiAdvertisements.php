<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Wiki Advertisements Skin
 *
 * @author    Brent Copeland
 * @copyright (c) 2014 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
 **/

use \DynamicSettings\Wiki\Advertisements;

class TemplateWikiAdvertisements {
	/**
	 * Ad List
	 *
	 * @param array Multidimensional array of wiki information.
	 * @param string Data sorting key
	 * @param string Data sorting direction
	 * @param string Search Term
	 * @param array Pagination
	 *
	 * @return string Built HTML
	 */
	public static function wikiAdvertisements($wikis, $pagination, $sortKey, $sortDir, $searchTerm) {
		global $wgOut, $wgUser, $wgRequest;

		$wikiAdvertisementsPage	= Title::newFromText('Special:WikiAdvertisements');

		$html = "
	<div>{$pagination}</div>
	<div class='button_bar'>
		<div class='buttons_left'>
			<form method='get' action='{$wikiAdvertisementsPage->getFullUrl()}'>
				<fieldset>
					<input type='hidden' name='section' value='list' />
					<input type='hidden' name='action' value='search' />
					<input type='text' name='list_search' value='" . htmlentities($searchTerm, ENT_QUOTES) . "' class='search_field' placeholder='" . wfMessage('search')->escaped() . "'/>
					<input type='submit' value='" . wfMessage('list_search') . "' class='mw-ui-button mw-ui-progressive' />
					<a href='{$wikiAdvertisementsPage->getFullUrl(['action' => 'resetSearch'])}' class='mw-ui-button mw-ui-destructive'>" . wfMessage('list_reset') . "</a>
				</fieldset>
			</form>
		</div>
		<div class='button_break'></div>
		<div class='buttons_right'>
			<div class='ad_legend'>" . wfMessage('ad_legend')->escaped() .
			" O - " . wfMessage('slot_override')->escaped() . " | A - " . wfMessage('slot_append')->escaped() . " | X - " . wfMessage('slot_disable')->escaped()
			. "
			</div>
		</div>
		<div class='buttons_right'>
			" . ($wgUser->isAllowed('wiki_advertisements') ? "<a href='{$wikiAdvertisementsPage->getFullUrl(['section' => 'wiki', 'action' => 'edit', 'siteKey' => '-1'])}' class='mw-ui-button'>" . wfMessage('edit_default_ad_tags') . "</a>" : null) . "
			" . ($wgUser->isAllowed('wiki_recache_ads') ? "<a href='{$wikiAdvertisementsPage->getFullUrl(['section' => 'recache'])}' class='mw-ui-button'>" . wfMessage('recache_all') . "</a>" : null) . "
		</div>
	</div>
	<table id='wikilist'>
		<thead>
			<tr class='sortable' data-sort-dir='" . ($sortDir == 'desc' ? 'desc' : 'asc') . "'>
				<th class='controls'>&nbsp;</th>
				<th" . ($sortKey == 'wiki_name' || $sortKey == 'wiki_domain' ? " data-selected='true'" : '') . ">
					<span data-sort='wiki_name'" . ($sortKey == 'wiki_name' ? " data-selected='true'" : '') . ">" . wfMessage('wiki_name')->escaped() . "</span>
					<span data-sort='domain'" . ($sortKey == 'domain' ? " data-selected='true'" : '') . ">" . wfMessage('wiki_domain')->escaped() . "</span>
				</th>";
		foreach (Advertisements::$slotTypes as $type) {
			foreach (Advertisements::getSlots($type) as $slot) {
				$html .= "
					<th class='{$type}_slots'" . ($sortKey == $slot ? " data-selected='true'" : null) . "><span data-sort='{$slot}'" . ($sortKey == $slot ? " data-selected='true'" : null) . ">" . wfMessage($slot . '_title')->escaped() . "</span></th>";
			}
		}
				$html .= "
			</tr>
		</thead>
		<tbody>
		";
		if (count($wikis)) {
			foreach ($wikis as $siteKey => $wiki) {
				if ($wiki->isDeleted()) {
					continue;
				}
				$advertisements = $wiki->getAdvertisements();

				$html .= "
				<tr>
					<td class='controls'>
						<div class='controls_container'>";
				if ($wgUser->isAllowed('wiki_advertisements')) {
					$html .= "
							" . HydraCore::awesomeIcon('wrench') . "
							<span class='dropdown'>
								<a href='{$wikiAdvertisementsPage->getFullUrl(['section' => 'wiki', 'action' => 'edit', 'siteKey' => $siteKey])}' title='" . wfMessage('edit_wiki_advertisements')->escaped() . "' class='edit_ads'>" . HydraCore::awesomeIcon('pencil-alt') . wfMessage('edit')->escaped() . "</a>
								<a href='{$wikiAdvertisementsPage->getFullUrl(['section' => 'recache', 'siteKey' => $siteKey])}' title='" . wfMessage('recache_wiki_advertisements')->escaped() . "' class='recache_ads'>" . HydraCore::awesomeIcon('sync') . wfMessage('recache')->escaped() . "</a>
								" . ($advertisements->hasCustomSlots() ? "<a href='{$wikiAdvertisementsPage->getFullUrl(['section' => 'wiki', 'action' => 'revert', 'siteKey' => $siteKey])}' title='" . wfMessage('revert_wiki_advertisements_to_default')->escaped() . "' class='revert_ads'>" . HydraCore::awesomeIcon("undo") . wfMessage('revert_ads')->escaped() . "</a>" : null) . "
								" . (!$advertisements->isDisabled() ? "<a href='{$wikiAdvertisementsPage->getFullUrl(['section' => 'wiki', 'action' => 'disableAds', 'siteKey' => $siteKey])}'>" . HydraCore::awesomeIcon("ban") . wfMessage("disable_ads")->escaped() . "</a>
								<a href='{$wikiAdvertisementsPage->getFullUrl(['section' => 'wiki', 'action' => 'enableAds', 'siteKey' => $siteKey])}'>" . HydraCore::awesomeIcon("undo") . wfMessage("enable_ads")->escaped() . "</a>" : '') . "
								<a href='{$wikiAdvertisementsPage->getFullUrl(['section' => 'wiki', 'action' => 'disableSlotsToggle', 'siteKey' => $siteKey])}'>" . ($advertisements->isDisabled() ? HydraCore::awesomeIcon("undo") : HydraCore::awesomeIcon("ban")) . wfMessage($advertisements->isDisabled() ? "enable_slots" : "disable_slots")->escaped() . "</a>
							</span>";
				}
				$html .= "
						</div>
					</td>
					<td>
						<span data-sort='wiki_name'" . ($sortKey == 'wiki_name' ? " data-selected='true'" : '') . ">{$wiki->getNameForDisplay()}</span>
						<a href='https://{$wiki->getDomains()->getDomain()}/'><span data-sort='domain'" . ($sortKey == 'domain' ? " data-selected='true'" : '') . ">{$wiki->getDomains()->getDomain()}</span></a>
					</td>";
				foreach (Advertisements::$slotTypes as $type) {
					$html .= self::slotRowConfigIndicator($type, Advertisements::getSlots($type), $wiki);
				}
				$html .= "
				</tr>";
			}
		} else {
			$html .= "
			<tr>
				<td colspan='" . (count(Advertisements::getSlots()) + 3) . "'>" . wfMessage('no_wikis_found') . "</td>
			</tr>
			";
		}
		$html .= "
		</tbody>
	</table>";

		$html .= $pagination;

		return $html;
	}

	/**
	 * Render a slot state selector.
	 *
	 * @param string Slots Type (ad, js)
	 * @param array Slots
	 * @param object Wiki
	 *
	 * @return string HTML
	 */
	private static function slotRowConfigIndicator($type, $slots, $wiki) {
		$advertisements = $wiki->getAdvertisements();

		$html = '';
		foreach ($slots as $slot) {
			if (!$advertisements->isDisabled()) {
				switch ($advertisements->getConfigBySlot($slot)) {
					case Advertisements::SLOT_DISABLE:
						$icon = "X";
						break;
					case Advertisements::SLOT_APPEND:
						$icon = "A";
						break;
					case Advertisements::SLOT_OVERRIDE:
						$icon = "O";
						break;
				}
			} else {
				$icon = "X";
			}
			if (!empty($advertisements->getRawSlot($slot))) {
				$icon = '<strong class="slot_not_empty">' . $icon . '</strong>';
			}
			$html .= "
				<td class='{$type}_slots'>" . $icon . "</td>";
		}

		return $html;
	}

	/**
	 * Wiki Advertisement Form
	 *
	 * @param object Wiki
	 * @param array Key name => Error of errors
	 *
	 * @return string Built HTML
	 */
	public static function wikiAdForm($wiki, $errors) {
		$wikiAdvertisementsPage	= Title::newFromText('Special:WikiAdvertisements');

		$html = "
		<form id='wiki_settings_form' class='ad_slots' method='post' action='{$wikiAdvertisementsPage->getFullUrl(['section' => 'wiki', 'action' => 'edit', 'do' => 'save'])}'>
			<fieldset>";
		foreach (Advertisements::$slotTypes as $type) {
			$html .= self::slotsTextarea($type, Advertisements::getSlots($type), $wiki, $errors);
		}

		$html .= "
				<input id='siteKey' name='siteKey' type='hidden' value='" . ($wiki->getName() == 'Default Ad Tags' ? -1 : $wiki->getSiteKey()) . "'/>
				<button id='wiki_submit' name='wiki_submit' class='mw-ui-button mw-ui-progressive'>" . wfMessage('save_key_value')->escaped() . "</button>
			</fieldset>
		</form>
		";

		return $html;
	}

	/**
	 * Render a slot state selector.
	 *
	 * @param string Slots Type (ad, js)
	 * @param array Slots
	 * @param object Wiki
	 * @param array [Optional] User input errors.
	 *
	 * @return string HTML
	 */
	private static function slotsTextarea($type, $slots, $wiki, $errors = []) {
		$advertisements = $wiki->getAdvertisements();

		$html = wfMessage($type . 'slot_header')->parse();
		foreach ($slots as $slot) {
			$state = $advertisements->getConfigBySlot($slot);
			$html .= (isset($errors[$slot]) ? '<span class="error">' . $errors[$slot] . '</span>' : '') . "
				<label for='{$slot}' class='label_above'>" . wfMessage($slot, $wiki->getSiteKey(), (strlen($wiki->getSiteKey()) > 2 ? 2 : 1))->parse() . "</label>
				" . ($wiki->getSiteKey() !== false ? "<pre class='default_slot'>" . htmlentities($advertisements->getDefaultBySlot($slot), ENT_QUOTES) . "</pre>" : '') .
				($wiki->getSiteKey() !== false ? self::slotStateSelector($slot, $state) . "<br/>" : '') . "
				<textarea name='{$slot}' id='{$slot}' class='lined wide'" . ($state === Advertisements::SLOT_DISABLE ? " disabled='disabled'" : '') . ">" . htmlentities($advertisements->getRawSlot($slot), ENT_QUOTES) . "</textarea><br/>";
		}
		return $html;
	}

	/**
	 * Render a slot state selector.
	 *
	 * @param string Slot Name
	 * @param integer Slot State
	 *
	 * @return string HTML
	 */
	private static function slotStateSelector($slot, $state) {
		$html = "
			<select id='{$slot}_state' name='{$slot}_state' data-slot='{$slot}' class='slot_state_selector'>
				<option " . ($state === Advertisements::SLOT_DISABLE ? "selected" : '') . " value='-1'>" . wfMessage('slot_disable')->escaped() . "</option>
				<option " . ($state === Advertisements::SLOT_APPEND ? "selected" : '') . " value='0'>" . wfMessage('slot_append')->escaped() . "</option>
				<option " . ($state === Advertisements::SLOT_OVERRIDE ? "selected" : '') . " value='1'>" . wfMessage('slot_override')->escaped() . "</option>
			</select>
		";
		return $html;
	}

	/**
	 * Wiki Advertisement Deletion Form
	 *
	 * @param array Wiki information
	 *
	 * @return string Built HTML
	 */
	public static function wikiAdRevert($wiki) {
		$wikiAdvertisementsPage	= Title::newFromText('Special:WikiAdvertisements');

		$html = "
		<form method='post' action='" . $wikiAdvertisementsPage->getFullUrl(['section' => 'wiki', 'action' => 'revert']) . "'>
			" . wfMessage('revert_ads_confirm')->escaped() . "<br/>
			<input type='hidden' name='do' value='confirm'/>
			<input type='hidden' name='siteKey' value='{$wiki->getSiteKey()}'/>
			<button type='submit' class='mw-ui-button mw-ui-destructive'>" . wfMessage('revert_ads')->escaped() . "</button>
		</form>
		";

		return $html;
	}
}
