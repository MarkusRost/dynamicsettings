<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * SitePromos PHP Template
 *
 * @author    Alex Smith
 * @copyright (c) 2014 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
**/

class TemplatePromotions {
	/**
	 * Output PHP
	 *
	 * @var string
	 */
	private $PHP;

	/**
	 * Promotions
	 *
	 * @param array Promotions
	 *
	 * @return string Built PHP
	 */
	public function promotions($promotions) {
		$this->PHP = "<?php\n";
		if (is_array($promotions) && count($promotions)) {
			$this->PHP .= "\$wgDSPromotions = [\n";
			foreach ($promotions as $promotion) {
				if ($promotion->getType() != 'notice') {
					$this->PHP .= "
	[
		'pid' => '" . $promotion->getDatabaseId() . "',
		'name' => '" . str_replace("'", "\'", $promotion->getName()) . "',
		'description' => '" . str_replace("'", "\'", $promotion->getDescription()) . "',
		'image' => '" . str_replace("'", "\'", $promotion->getImage()) . "',
		'link' => '" . str_replace("'", "\'", $promotion->getLink()) . "',
		'languages' => " . var_export($promotion->getLanguages(), true) . ",
		'begins' => '" . $promotion->getBegins() . "',
		'expires' => '" . $promotion->getExpires() . "',
		'weight' => '" . $promotion->getWeight() . "',
		'type' => '" . $promotion->getType() . "',
		'paused' => '" . intval($promotion->isPaused()) . "',
	],";
				}
			}
			$this->PHP .= "
];\n";

			$this->PHP .= "\$wgDSNotices = [\n";
			foreach ($promotions as $promotion) {
				if ($promotion->getType() == 'notice') {
					$this->PHP .= "
	[
		'pid' => '" . $promotion->getDatabaseId() . "',
		'name' => '" . str_replace("'", "\'", $promotion->getName()) . "',
		'description' => '" . str_replace("'", "\'", $promotion->getDescription()) . "',
		'image' => '" . str_replace("'", "\'", $promotion->getImage()) . "',
		'link' => '" . str_replace("'", "\'", $promotion->getLink()) . "',
		'languages' => " . var_export($promotion->getLanguages(), true) . ",
		'begins' => '" . $promotion->getBegins() . "',
		'expires' => '" . $promotion->getExpires() . "',
		'weight' => '" . $promotion->getWeight() . "',
		'type' => '" . $promotion->getType() . "',
		'paused' => '" . intval($promotion->isPaused()) . "',
	],";
				}
			}
			$this->PHP .= "
];\n";
		}

		$this->PHP .= "?>\n";

		return $this->PHP;
	}
}
