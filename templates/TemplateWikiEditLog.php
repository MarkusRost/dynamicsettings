<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Wiki Edit Log Skin
 *
 * @author    Alex Smith
 * @copyright (c) 2014 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
**/

class TemplateWikiEditLog {
	/**
	 * Output HTML
	 *
	 * @var string
	 */
	private $HMTL;

	/**
	 * Wiki Edit Log
	 *
	 * @param array Array of log entries
	 * @param array Pagination
	 * @param array Calculated totals of entries.
	 * @param array Stale Wiki Information
	 *
	 * @return string Built HTML
	 */
	public function wikiEditLog($logs, $hidden, $pagination, $uncachedEntries) {
		global $wgOut, $wgUser;

		$wikiSitesPage		= Title::newFromText('Special:WikiSites');
		$wikiEditLogPage	= Title::newFromText('Special:WikiEditLog');

		$html = "
		{$pagination}
		<div class='button_bar'>";
		if (count($uncachedEntries)) {
			$html .= "
			<div class='buttons_left'>
				<ul id='stale_wikis'>
					<li>
						<span>" . wfMessage('uncached_entries') . " (" . count($uncachedEntries) . ")</span>
						<ul>
						";
			foreach ($uncachedEntries as $key => $entry) {
				$html .= "<li>{$entry}</li>";
			}
			$html .= "
						</ul>
					</li>
				</ul>
			</div>
			<div class='button_break'></div>
			<div class='buttons_right'>
				<a href='" . $wikiEditLogPage->getFullURL(['hide_legacy' => ($hidden['legacy'] ? 'false' : 'true')]) . "' class='mw-ui-button with_legend'><span class='legend_legacy'></span>" . wfMessage(($hidden['legacy'] ? 'show' : 'hide') . '_legacy_entries') . "</a>
				<a href='" . $wikiEditLogPage->getFullURL(['hide_deleted' => ($hidden['deleted'] ? 'false' : 'true')]) . "' class='mw-ui-button with_legend'><span class='legend_deleted'></span>" . wfMessage(($hidden['deleted'] ? 'show' : 'hide') . '_deleted_entries') . "</a>
			</div>";
		}
		$html .= "
		</div>
		<table id='wikilist'>
			<thead>
				<tr>
					<th class='controls unsortable'>" . wfMessage('edit_log_tools')->escaped() . "</th>
					<th class='unsortable'>&nbsp;</th>
					<th>" . wfMessage('edit_log_key')->escaped() . "</th>
					<th>" . wfMessage('edit_log_user')->escaped() . "</th>
					<th>" . wfMessage('edit_log_type')->escaped() . "</th>
					<th>" . wfMessage('edit_log_commit_message')->escaped() . "</th>
					<th>" . wfMessage('edit_log_timestamp')->escaped() . "</th>
					<th>" . wfMessage('edit_log_ip_address')->escaped() . "</th>
					<th>" . wfMessage('edit_log_id')->escaped() . "</th>
				</tr>
			</thead>
			<tbody>";
		if (count($logs)) {
			foreach ($logs as $log) {
				$rowClass = '';
				if ($log->isLegacy()) {
					$rowClass = 'legacy';
				} elseif ($log->isDeleted()) {
					$rowClass = 'deleted';
				} elseif ($log->canRecache()) {
					$rowClass = 'uncached';
				}

				$html .= "
					<tr class='editlog {$rowClass}'>
						<td class='controls'>
						" . ($wgUser->isAllowed('wiki_edit_log_form_data') ? "<div class='form_diff' data-log-id='{$log->getId()}' style='display: none;'><pre>" . htmlentities($log->getDiffAgainstPrevious(), ENT_QUOTES) . "</pre></div>" : null) . "
							<div class='controls_container'>
								" . HydraCore::awesomeIcon('wrench') . "
								<span class='dropdown'>
									<a href='" . $wikiEditLogPage->getFullURL(['section' => 'log', 'log' => $log->getId()]) . "' title='Log Detail'>" . HydraCore::awesomeIcon('info-circle') . wfMessage('log_detail')->escaped() . "</a>
									" . ($wgUser->isAllowed('wiki_edit_log_form_data') ? "<span class='show_form_diff' data-log-id='{$log->getId()}'>" . HydraCore::awesomeIcon('columns') . wfMessage('log_diff')->escaped() . "</span>" : null) . "
									" . (($wgUser->isAllowed('wiki_edit_log_soft_delete') && $log->isDeletable()) ? " <a href ='" . $wikiEditLogPage->getFullURL(['section' => 'log', 'action' => 'delete', 'log' => $log->getId()]) . "' title ='" . wfMessage('soft_delete_log_entry')->escaped() . "'>" . HydraCore::awesomeIcon('minus-circle') . wfMessage('soft_delete')->escaped() . "</a> " : null) . "
									" . (($wgUser->isAllowed('wiki_edit_log_hard_delete') && !$log->isLegacy()) ? " <a href ='" . $wikiEditLogPage->getFullURL(['section' => 'log', 'action' => 'delete', 'log' => $log->getId(), 'permanent' => '1']) . "' title ='" . wfMessage('hard_delete_log_entry')->escaped() . "'>" . HydraCore::awesomeIcon('times-circle') . wfMessage('hard_delete')->escaped() . "</a> " : null) . "
								</span>
							</div>
						</td>
						<td class='recache'>";
				if ($wgUser->isAllowed('wiki_recache') && $log->canRecache()) {
					$html .= "<form method='post' action='" . $wikiSitesPage->getFullURL(['section' => 'recache']) . "'><fieldset><input name='siteKey' type='hidden' value='{$log->getLogKey()}'/><button type='submit' title='" . wfMessage('recache_wiki')->escaped() . "'>" . HydraCore::awesomeIcon('sync') . "</button></fieldset></form>";
				} else {
					$html .= HydraCore::awesomeIcon('check-circle');
				}
				$html .= "
						</td>
						<td>{$log->getKeyLink()}</td>
						<td>" . ($log->getUser() ? "<a href='{$log->getUser()->getUserPage()->getFullUrl()}'>{$log->getUser()->getName()}</a>" : '&nbsp;') . "</td>
						<td>{$log->getTypeLink()}</td>
						<td>{$log->getCommitMessage()}</td>
						<td>{$log->getTimestamp('Y-m-d H:i e')}</td>
						<td><a href='http://www.geoiptool.com/en/?IP={$log->getIPAddress()}' target='_blank'>{$log->getIPAddress()}</a></td>
						<td><a href='" . $wikiEditLogPage->getFullURL(['section' => 'log', 'log' => $log->getId()]) . "'>{$log->getId()}</td>
					</tr>";
			}
		} else {
			$html .= "
				<tr>
					<td colspan='9'>" . wfMessage('no_logs_found') . "</td>
				</tr>
			";
		}
		$html .= "
			</tbody>
		</table>";

		$html .= $pagination;

		return $html;
	}

	/**
	 * Wiki Edit Detail Page
	 *
	 * @param object Edit Log Entry
	 *
	 * @return string
	 */
	public function wikiEditDetail($log) {
		$wgUser = RequestContext::getMain()->getUser();

		$html = '';

		if ($wgUser->isAllowed('wiki_recache') && $log->canRecache()) {
			$wikiSitesPage = Title::newFromText('Special:WikiSites');
			$html .= "
		<div class='button_bar'>
			<div class='buttons_left'>
				<form method='post' action='{$wikiSitesPage->getFullURL(['section' => 'recache'])}'><fieldset><input name='siteKey' type='hidden' value='{$log->getLogKey()}'/><button type='submit' title='" . wfMessage('wikisites-recache_wiki')->escaped() . "' class='mw-ui-button'>" . wfMessage('recache_wiki')->escaped() . "</button></fieldset></form>
			</div>
		</div>";
		}

		$html .= "
		<div id='edit_details'>
			<ul>
				<li><span>" . wfMessage('edit_log_id')->escaped() . ":</span> {$log->getId()}</li>
				<li><span>" . wfMessage('edit_log_user')->escaped() . ":</span> " . ($log->getUser() ? "<a href='{$log->getUser()->getUserPage()->getFullUrl()}'>{$log->getUser()->getName()}</a> (<a href='http://www.geoiptool.com/en/?IP={$log->getIPAddress()}' target='_blank'>{$log->getIPAddress()}</a>)" : '') . "</li>
				<li><span>" . wfMessage('edit_log_timestamp')->escaped() . ":</span> {$log->getTimestamp('Y-m-d H:i e')}</li>
				<li><span>" . wfMessage('edit_log_key')->escaped() . ":</span> {$log->getKeyLink()}</li>
				<li><span>" . wfMessage('edit_log_type')->escaped() . ":</span> {$log->getTypeLink()}</li>
				<li><span>" . wfMessage('edit_log_commit_message')->escaped() . ":</span> " . htmlentities($log->getCommitMessage(), ENT_QUOTES) . "</li>
				<li><span>Diff of the commit:</span></li>
			</ul>
			<div class='log_diff'><pre>{$log->getDiffAgainstPrevious()}</pre></div>
		</div>";
		return $html;
	}
}
