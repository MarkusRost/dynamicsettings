<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Wiki Sites Skin
 *
 * @author    Alex Smith
 * @copyright (c) 2014 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
**/

use DynamicSettings\Environment;
use DynamicSettings\Lock;
use DynamicSettings\Wiki\Domains;

class TemplateWikiSites {
	/**
	 * Wiki Sites
	 *
	 * @param array Array of Wiki Information
	 * @param array Pagination
	 * @param boolean [Optional] Hide Deleted Wikis
	 * @param string [Optional] Data sorting key
	 * @param string [Optional] Data sorting direction
	 * @param string [Optional] Search Term
	 *
	 * @return string Built HTML
	 */
	public function wikiSites($wikis, $pagination, $hideDeleted = false, $sortKey = 'wiki_name', $sortDir = 'ASC', $searchTerm = null) {
		global $wgOut, $wgUser, $wgRequest;

		$wikiSitesPage			= Title::newFromText('Special:WikiSites');
		$wikiInstallPage 		= Title::newFromText('Special:WikiInstall');
		$settingsPage			= Title::newFromText('Special:WikiSettings');
		$extensionsPage			= Title::newFromText('Special:WikiExtensions');
		$permissionsPage		= Title::newFromText('Special:WikiPermissions');
		$namespacesPage			= Title::newFromText('Special:WikiNamespaces');
		$advertisementsPage		= Title::newFromText('Special:WikiAdvertisements');
		$maintenanceRunnerPage	= Title::newFromText('Special:MaintenanceRunner');
		$editLogPage			= Title::newFromText('Special:WikiEditLog');

		$html = "
	<div>{$pagination}</div>
	<div class='button_bar'>
		<div class='buttons_left'>
			<form method='get' action='{$wikiSitesPage->getFullURL()}'>
				<fieldset>
					<input type='hidden' name='section' value='list'/>
					<input type='hidden' name='action' value='search'/>
					<input type='text' name='list_search' value='" . htmlentities($searchTerm, ENT_QUOTES) . "' class='search_field' placeholder='" . wfMessage('list_search')->escaped() . "' title='" . wfMessage('wiki_sites_search_tooltip')->escaped() . "'/>
					<input type='submit' value='" . wfMessage('list_search')->escaped() . "' class='mw-ui-button mw-ui-progressive'/>
					<a href='{$wikiSitesPage->getFullURL(['action' => 'resetSearch'])}' class='mw-ui-button mw-ui-destructive'>" . wfMessage('list_reset')->escaped() . "</a>
				</fieldset>
			</form>
		</div>
		<div class='button_break'></div>
		<div class='buttons_right'>
			<form id='mass_action_all' method='post'>
				" . ($wgUser->isAllowed('wiki_recache') ? "<button class='mw-ui-button confirmWithTitle' type='submit' formaction='{$wikiSitesPage->getFullUrl(['section' => 'recache'])}' title='" . wfMessage('wikisites-recache_wiki_all')->escaped() . "'/>" . HydraCore::awesomeIcon('sync') . "</button>" : null) . "
				" . ($wgUser->isAllowed('wiki_update') ? "<button class='mw-ui-button confirmWithTitle' type='submit' formaction='{$wikiSitesPage->getFullUrl(['section' => 'update'])}' title='" . wfMessage('wikisites-update_all')->escaped() . "'/>" . HydraCore::awesomeIcon('wrench') . "</button>" : null) . "
				" . ($wgUser->isAllowed('wiki_rebuildLanguage') ? "<button class='mw-ui-button confirmWithTitle' type='submit' formaction='{$wikiSitesPage->getFullUrl(['section' => 'rebuildLanguage'])}' title='" . wfMessage('wikisites-rebuild_language_all')->escaped() . "'/>" . HydraCore::awesomeIcon('language') . "</button>" : null) . "
			</form>
		</div>
		<div class='buttons_right'>
			<a href='" . $wikiSitesPage->getFullURL(['hideDeleted' => ($hideDeleted ? '0' : '1')]) . "' class='mw-ui-button with_legend'><span class='legend_deleted'></span>" . wfMessage(($hideDeleted ? 'show' : 'hide') . '_deleted_wikis')->escaped() . "</a>
			" . ($wgUser->isAllowed('wiki_add_edit') ? "<a href='{$wikiSitesPage->getFullURL(['section' => 'wiki', 'action' => 'add'])}' class='mw-ui-button mw-ui-constructive'>" . wfMessage('add_wiki')->escaped() . "</a>" : null) . "
		</div>
	</div>";

		$html .= "
	<table id='wikilist'>
		<thead>
			<tr class='sortable' data-sort-dir='" . ($sortDir == 'desc' ? 'desc' : 'asc') . "'>";
		if ($wgUser->isAllowed('wiki_mass_action')) {
			$html .= "
				<th class='controls unsortable'>
					<form id='mass_action' method='post'>
						<input id='action_all' name='action_all' type='checkbox'/>
						<div id='mass_button_holder'>
							" . ($wgUser->isAllowed('wiki_recache') ? "<button class='mw-ui-button' type='submit' formaction='{$wikiSitesPage->getFullUrl(['section' => 'recache', 'action' => 'massaction'])}' title='" . wfMessage('wikisites-recache_wiki_selection')->escaped() . "'/>" . HydraCore::awesomeIcon('sync') . "</button>" : null) . "
							" . ($wgUser->isAllowed('wiki_update') ? "<button class='mw-ui-button' type='submit' formaction='{$wikiSitesPage->getFullUrl(['section' => 'update', 'action' => 'massaction'])}' title='" . wfMessage('wikisites-update_selection')->escaped() . "'/>" . HydraCore::awesomeIcon('wrench') . "</button>" : null) . "
							" . ($wgUser->isAllowed('wiki_rebuildLanguage') ? "<button class='mw-ui-button' type='submit' formaction='{$wikiSitesPage->getFullUrl(['section' => 'rebuildLanguage', 'action' => 'massaction'])}' title='" . wfMessage('wikisites-rebuild_language_selection')->escaped() . "'/>" . HydraCore::awesomeIcon('language') . "</button>" : null) . "
							" . ($wgUser->isAllowed('wiki_add_edit') ? "<button class='mw-ui-button' type='submit' formaction='{$wikiSitesPage->getFullUrl(['section' => 'wiki', 'action' => 'massedit'])}' title='" . wfMessage('wikisites-edit_main_config_selection')->escaped() . "'/>" . HydraCore::awesomeIcon('pencil-alt') . "</button>" : null) . "
						</div>
					</form>
				</th>";
		}
		$html .= "
				<th class='controls unsortable'>&nbsp;</th>
				<th" . ($sortKey == 'wiki_name' || $sortKey == 'wiki_meta_name' ? " data-selected='true'" : '') . ">
					<span data-sort='wiki_name'" . ($sortKey == 'wiki_name' ? " data-selected='true'" : '') . ">" . wfMessage('wiki_name')->escaped() . "</span>
					<span data-sort='wiki_meta_name'" . ($sortKey == 'wiki_meta_name' ? " data-selected='true'" : '') . ">" . wfMessage('wiki_meta_name')->escaped() . "</span>
				</th>
				<th" . ($sortKey == 'domain' ? " data-selected='true'" : '') . ">
					<span data-sort='domain'" . ($sortKey == 'domain' ? " data-selected='true'" : '') . ">" . wfMessage((Environment::detectEnvironment() !== 'development' ? 'wiki_domain' : 'wiki_domain_staging'))->escaped() . "</span>
					<span data-sort='domain'" . ($sortKey == 'domain' ? " data-selected='true'" : '') . ">" . wfMessage((Environment::detectEnvironment() !== 'development' ? 'wiki_domain_staging' : 'wiki_domain_local'))->escaped() . "</span>
				</th>
				<th" . ($sortKey == 'wiki_category' ? " data-selected='true'" : '') . ">
					<span data-sort='wiki_category'" . ($sortKey == 'wiki_category' ? " data-selected='true'" : '') . ">" . wfMessage('wiki_category')->escaped() . "</span>
				</th>
				<th" . ($sortKey == 'wiki_language' ? " data-selected='true'" : '') . ">
					<span data-sort='wiki_language'" . ($sortKey == 'wiki_language' ? " data-selected='true'" : '') . ">" . wfMessage('wiki_language')->escaped() . "</span>
				</th>
				<th" . ($sortKey == 'wiki_portal' || $sortKey == 'wiki_group' ? " data-selected='true'" : '') . ">
					<span data-sort='wiki_portal'" . ($sortKey == 'wiki_portal' ? " data-selected='true'" : '') . ">" . wfMessage('portal_type')->escaped() . "</span>
					<span data-sort='wiki_group'" . ($sortKey == 'wiki_group' ? " data-selected='true'" : '') . ">" . wfMessage('group_type')->escaped() . "</span>
				</th>
				<th" . ($sortKey == 'wiki_managers' ? " data-selected='true'" : '') . ">
					<span data-sort='wiki_managers'" . ($sortKey == 'wiki_managers' ? " data-selected='true'" : '') . ">" . wfMessage('wiki_managers')->escaped() . "</span>
				</th>
				<th" . ($sortKey == 'created' || $sortKey == 'edited' ? " data-selected='true'" : '') . ">
					<span data-sort='created'" . ($sortKey == 'created' ? " data-selected='true'" : '') . ">" . wfMessage('wiki_created')->escaped() . "</span>
					<span data-sort='edited'" . ($sortKey == 'edited' ? " data-selected='true'" : '') . ">" . wfMessage('wiki_edited')->escaped() . "</span>
				</th>
			</tr>
		</thead>
		<tbody>
		";
		if (is_array($wikis) && count($wikis)) {
			$_wikis = [];
			foreach ($wikis as $wiki) {
				$siteKey = $wiki->getSiteKey();

				$html .= "
				<tr class='" . ($wiki->isDeleted() ? "deleted " : '') . (!$wiki->isInstalled() && !$wiki->isDeleted() ? "new_wiki " : '') . "'>";
				if ($wgUser->isAllowed('wiki_mass_action')) {
					$html .= "
					<td>" . ($wiki->isDeleted() ? "&nbsp;" : "<input name='mass_action[]' type='checkbox' value='{$siteKey}'/>") . "</td>";
				}
				$html .= "
					<td class='controls'>
						<div class='controls_container'>
";
				$extraMenuOptions = [];
				if (!$wiki->isDeleted()) {
					wfRunHooks('WikiSitesMenuOptions', [&$extraMenuOptions, $wiki]);
				}
				$dropdown = '';
				$html .= HydraCore::awesomeIcon('wrench');
				if (!$wiki->isDeleted() && !Lock::isLocked($siteKey)) {
					$dropdown .= ($wgUser->isAllowed('wiki_add_edit') ? "<a href='{$wikiSitesPage->getFullUrl(['section' => 'wiki', 'action' => 'edit', 'siteKey' => $siteKey])}' title='" . wfMessage('wikisites-edit_main_config')->escaped() . "'>" . HydraCore::awesomeIcon('database') . wfMessage('main')->escaped() . "</a>" : null) . "
					" . ($wgUser->isAllowed('wiki_settings') ? "<a href='{$settingsPage->getFullUrl(['siteKey' => $siteKey])}' title='" . wfMessage('wikisites-change_extra_settings')->escaped() . "'>" . HydraCore::awesomeIcon('cogs') . wfMessage('settings')->escaped() . "</a>" : null) . "
					" . ($wgUser->isAllowed('wiki_extensions') ? "<a href='{$extensionsPage->getFullUrl(['siteKey' => $siteKey])}' title='" . wfMessage('wikisites-enable_disable_extension')->escaped() . "'>" . HydraCore::awesomeIcon('plug') . wfMessage('extensions')->escaped() . "</a>" : null) . "
					" . ($wgUser->isAllowed('wiki_group_permissions') ? "<a href='{$permissionsPage->getFullUrl(['siteKey' => $siteKey])}' title='" . wfMessage('wikisites-edit_group_permissions')->escaped() . "'>" . HydraCore::awesomeIcon('key') . wfMessage('permissions')->escaped() . "</a>" : null) . "
					" . ($wgUser->isAllowed('wiki_namespaces') ? "<a href='{$namespacesPage->getFullUrl(['siteKey' => $siteKey])}' title='" . wfMessage('wikisites-edit_namespaces')->escaped() . "'>" . HydraCore::awesomeIcon('list') . wfMessage('namespaces')->escaped() . "</a>" : null) . "
					" . ($wgUser->isAllowed('wiki_advertisements') ? "<a href='{$advertisementsPage->getFullUrl(['section' => 'wiki', 'do' => 'edit', 'siteKey' => $siteKey])}' title='" . wfMessage('wikisites-edit_advertisements')->escaped() . "'>" . HydraCore::awesomeIcon('dollar-sign') . wfMessage('advertisements')->escaped() . "</a>" : null) . "
					" . ($wgUser->isAllowed('maintenancerunner') ? "<a href='{$maintenanceRunnerPage->getFullURL(['siteKey' => $siteKey])}' title='" . wfMessage('maintenancerunner')->escaped() . "'>" . HydraCore::awesomeIcon('terminal') . wfMessage('maintenancerunner')->escaped() . "</a>" : null) . "
					";
				}

				if (!Lock::isLocked($siteKey)) {
					$dropdown .= ($wiki->isInstalled() && !$wiki->isDeleted() && $wgUser->isAllowed('wiki_searchReindex') ? "<form method='post' action='{$wikiSitesPage->getFullURL(['section' => 'searchReindex'])}'><fieldset><input name='siteKey' type='hidden' value='{$wiki->getSiteKey()}'/><button type='submit' title='" . wfMessage('wikisites-search_reindex')->escaped() . "'>" . HydraCore::awesomeIcon('search') . wfMessage('wikisites-search_reindex')->escaped() . "</button></fieldset></form>" : null) . "
					" . ($wgUser->isAllowed('wiki_recache') ? "<form method='post' action='{$wikiSitesPage->getFullURL(['section' => 'recache'])}'><fieldset><input name='siteKey' type='hidden' value='{$wiki->getSiteKey()}'/><button type='submit' title='" . wfMessage('wikisites-recache_wiki')->escaped() . "'>" . HydraCore::awesomeIcon('sync') . wfMessage('recache_site')->escaped() . "</button></fieldset></form>" : null) . "
					" . ($wiki->isInstalled() && !$wiki->isDeleted() && $wgUser->isAllowed('wiki_update') ? "<form method='post' action='{$wikiSitesPage->getFullURL(['section' => 'update'])}'><fieldset><input name='siteKey' type='hidden' value='{$wiki->getSiteKey()}'/><button type='submit' title='" . wfMessage('wikisites-update')->escaped() . "'>" . HydraCore::awesomeIcon('wrench') . wfMessage('update')->escaped() . "</button></fieldset></form>" : null) . "
					" . ($wiki->isInstalled() && !$wiki->isDeleted() && $wgUser->isAllowed('wiki_rebuildLanguage') ? "<form method='post' action='{$wikiSitesPage->getFullURL(['section' => 'rebuildLanguage'])}'><fieldset><input name='siteKey' type='hidden' value='{$wiki->getSiteKey()}'/><button type='submit' title='" . wfMessage('wikisites-rebuild_language')->escaped() . "'>" . HydraCore::awesomeIcon('language') . wfMessage('rebuild_language')->escaped() . "</button></fieldset></form>" : null);
				}
				if (!$wiki->isInstalled() && !$wiki->isDeleted()) {
					$dropdown .= "
								<a href='{$wikiInstallPage->getFullURL(['siteKey' => $siteKey])}' title='" . wfMessage('install_wiki')->escaped() . "'>
								" . HydraCore::awesomeIcon('sync') . wfMessage('install_wiki')->escaped() . "</a>";
				}
				$dropdown .=
				($wgUser->isAllowed('wiki_edit_log') ? "<a href='{$editLogPage->getFullUrl(['siteKey' => $siteKey])}' title='" . wfMessage('wikisites-edit_log')->escaped() . "'>" . HydraCore::awesomeIcon('bars') . wfMessage('edit_log')->escaped() . "</a>" : null)
				. implode("\n", $extraMenuOptions) .
				($wgUser->isAllowed('lock_settings') ? "<form method='post' action='{$wikiSitesPage->getFullURL(['section' => 'togglelock'])}'><fieldset><input name='siteKey' type='hidden' value='{$wiki->getSiteKey()}'/><button type='submit' title='" . wfMessage('wikisites-lock_settings')->escaped() . "'>" . (Lock::isLocked($siteKey) ? HydraCore::awesomeIcon('unlock') . wfMessage('unlock_settings')->escaped() : HydraCore::awesomeIcon('lock') . wfMessage('lock_settings')->escaped()) . "</button></fieldset></form>" : null) .
				(!$wiki->isDeleted() && $wgUser->isAllowed('wiki_delete') ? "<a href='{$wikiSitesPage->getFullURL(['section' => 'wiki', 'action' => 'delete', 'siteKey' => $siteKey])}' title='" . wfMessage('wikisites-delete_wiki')->escaped() . "'>" . HydraCore::awesomeIcon('minus-circle') . wfMessage('delete')->escaped() . "</a>" : null) .
				($wiki->isDeleted() && $wgUser->isAllowed('wiki_undelete') ? "<a href='{$wikiSitesPage->getFullURL(['section' => 'wiki', 'action' => 'undelete', 'siteKey' => $siteKey])}' title='" . wfMessage('wikisites-undelete_wiki')->escaped() . "'>" . HydraCore::awesomeIcon('plus-circle') . wfMessage('wiki_undelete')->escaped() . "</a>" : null);
				if (!empty(trim($dropdown))) {
					$html .= "
							<span class='dropdown'>" . $dropdown . "</span>";
				}
				$html .= "
						</div>
					</td>
					<td>
						<span data-sort='wiki_name'" . ($sortKey == 'wiki_name' ? " data-selected='true'" : '') . ">{$wiki->getName()}</span>
						<span data-sort='wiki_meta_name'" . ($sortKey == 'wiki_meta_name' ? " data-selected='true'" : '') . ">{$wiki->getMetaName()}</span>
						<span class='subtext'>(" . wfMessage('md5')->escaped() . ": {$wiki->getSiteKey()})</span>
					</td>
					<td>
						<a href='https://{$wiki->getDomains()->getDomain((Environment::detectEnvironment() !== 'development' ? Domains::ENV_PRODUCTION : Domains::ENV_STAGING))}/'><span data-sort='domain'" . ($sortKey == 'domain' ? " data-selected='true'" : '') . ">{$wiki->getDomains()->getDomain((Environment::detectEnvironment() !== 'development' ? Domains::ENV_PRODUCTION : Domains::ENV_STAGING))}</span></a>
						<a href='https://{$wiki->getDomains()->getDomain((Environment::detectEnvironment() !== 'development' ? Domains::ENV_STAGING : Domains::ENV_DEVELOPMENT))}/'><span data-sort='domain'" . ($sortKey == 'domain' ? " data-selected='true'" : '') . ">{$wiki->getDomains()->getDomain((Environment::detectEnvironment() !== 'development' ? Domains::ENV_STAGING : Domains::ENV_DEVELOPMENT))}</span></a>
					</td>
					<td>
						<span data-sort='wiki_category'" . ($sortKey == 'wiki_category' ? " data-selected='true'" : '') . ">{$wiki->getCategory()}";
				if (count($wiki->getTags())) {
					$html .= "
						<span class='popup_list'>...
							<ul>";
					foreach ($wiki->getTags() as $tag) {
						$html .= "<li>{$tag}</li>";
					}
					$html .= "
							</ul>
						</span>";
				} else {
					$html .= "&nbsp;";
				}
					$html .= "
						</span>
					</td>
					<td><span data-sort='wiki_language'" . ($sortKey == 'wiki_language' ? " data-selected='true'" : '') . ">{$wiki->getLanguage()}</span></td>
					<td>
						<span data-sort='wiki_portal'" . ($sortKey == 'wiki_portal' ? " data-selected='true'" : '') . ">";
				if ($wiki->isPortalChild()) {
					if (array_key_exists($wiki->getPortalKey(), $wikis) && $wikis[$wiki->getPortalKey()] !== false) {
						$html .= $wikis[$wiki->getPortalKey()]->getDomains()->getDomain();
					} elseif (array_key_exists($wiki->getPortalKey(), $_wikis) && $_wikis[$wiki->getPortalKey()] !== false) {
						$html .= $_wikis[$wiki->getPortalKey()]->getDomains()->getDomain();
					} else {
						$_wikis[$wiki->getPortalKey()] = \DynamicSettings\Wiki::loadFromHash($wiki->getPortalKey());
						if ($_wikis[$wiki->getPortalKey()] !== false) {
							$html .= $_wikis[$wiki->getPortalKey()]->getDomains()->getDomain();
						}
					}
				} elseif ($wiki->isPortalMaster()) {
					$html .= "(" . wfMessage('portal_type') . ")";
				} else {
					$html .= "&nbsp;";
				}
					$html .= "
						</span>
						<span data-sort='wiki_group'" . ($sortKey == 'wiki_group' ? " data-selected='true'" : '') . ">";
				if ($wiki->isGroupChild()) {
					if (array_key_exists($wiki->getGroupKey(), $wikis) && $wikis[$wiki->getGroupKey()] !== false) {
						$html .= $wikis[$wiki->getGroupKey()]->getDomains()->getDomain();
					} elseif (array_key_exists($wiki->getGroupKey(), $_wikis) && $_wikis[$wiki->getGroupKey()] !== false) {
						$html .= $_wikis[$wiki->getGroupKey()]->getDomains()->getDomain();
					} else {
						$_wikis[$wiki->getGroupKey()] = \DynamicSettings\Wiki::loadFromHash($wiki->getGroupKey());
						if ($_wikis[$wiki->getGroupKey()] !== false) {
							$html .= $_wikis[$wiki->getGroupKey()]->getDomains()->getDomain();
						}
					}
				} elseif ($wiki->isGroupMaster()) {
					$html .= "(" . wfMessage('group_master') . ")";
				} else {
					$html .= "&nbsp;";
				}
					$html .= "
						</span>
					</td>
					<td>";
				if (count($wiki->getManagers())) {
					if (count($wiki->getManagers()) > 1) {
						$html .= "<span class='popup_list'>...";
					}
					$html .= "<ul>";
					foreach ($wiki->getManagers() as $userName) {
						$html .= "<li>{$userName}</li>";
					}
					$html .= "</ul>";
					if (count($wiki->getManagers()) > 1) {
						$html .= "</span>";
					}
				} else {
					$html .= "&nbsp;";
				}
				$html .= "
					</td>
					<td>
						<span data-sort='created'" . ($sortKey == 'created' ? " data-selected='true'" : '') . ">{$wiki->getCreated('Y-m-d H:i e')}</span>
						<span data-sort='edited'" . ($sortKey == 'edited' ? " data-selected='true'" : '') . ">{$wiki->getEdited('Y-m-d H:i e')}</span>
					</td>";
				$html .= "
				</tr>";
			}
		} else {
			$html .= "
			<tr>
				<td colspan='11'>" . wfMessage('no_wikis_found') . "</td>
			</tr>
			";
		}
		$html .= "
		</tbody>
	</table>";

		$html .= $pagination;

		return $html;
	}

	/**
	 * Wiki Information
	 *
	 * @param object Wiki
	 * @param array Array of Wiki objects for mass edit mode.
	 * @param array Key name => Error of errors
	 * @param string [Optional] Form action; one of: ['add', 'edit', 'massedit']
	 * @param array [Optional] Language codes to names
	 * @param array [Optional] Categories
	 * @param array [Optional] Wiki Managers
	 * @param array [Optional] Database Types
	 * @param array [Optional] Search Types
	 * @param array [Optional] Portal Masters
	 * @param array [Optional] Group Masters
	 * @param array [Optional] Wiki Tags
	 * @param array [Optional] Database Nodes available for autofill.
	 *
	 * @return string Built HTML
	 */
	public function wikiForm($wiki, $wikis, $errors, $action = 'add', $languages = [], $categories = [], $wikiManagers = [], $dbTypes = [], $searchTypes = [], $portalMasters = [], $groupMasters = [], $wikiTags = [], $dbNodes = []) {
		global $wgRequest, $wgUser;

		$html = '';

		$page = Title::newFromText('Special:WikiSites');

		$massEdit = false;
		if ($action === 'massedit') {
			$massEdit = true;
		}

		if (!$massEdit) {
			if (($wiki === false || !$wiki->getSiteKey()) && $wgUser->isAllowed('wiki_add_new')) {
				$html .= "
			<div class='button_bar'>
				<div class='button_break'></div>
				<div class='buttons_right'>
					<span id='wiki_fill_button' class='mw-ui-button mw-ui-progressive'>" . wfMessage('wiki_auto_fill')->escaped() . "</span>
				</div>
			</div>
			<div id='wiki_fill' class='popupWrapper initially_hidden'>
				<div class='popupInner'>
					<label for='name_fill' class='label_above'>" . wfMessage('name_fill')->escaped() . "</label>
					<input id='name_fill' name='name_fill' type='text' value=''/><input id='append_wiki' name='append_wiki' type='checkbox'/ checked='checked'><label for='append_wiki'>" . wfMessage('append_wiki_to_name')->escaped() . "</label>

					<label for='domain_fill' class='label_above'>" . wfMessage('domain_fill')->escaped() . "</label>
					<input id='domain_fill' name='domain_fill' type='text' value=''/>

					<label for='tld_fill' class='label_above'>" . wfMessage('tld_fill')->escaped() . "</label>
					<select id='tld_fill' name='tld_fill'>
						<option value='.com'>.com</option>
						<option value='.net'>.net</option>
						<option value='.org'>.org</option>
						<option value='.aero'>.aero</option>
						<option value='.asia'>.asia</option>
						<option value='.biz'>.biz</option>
						<option value='.cat'>.cat</option>
						<option value='.coop'>.coop</option>
						<option value='.info'>.info</option>
						<option value='.int'>.int</option>
						<option value='.jobs'>.jobs</option>
						<option value='.mobi'>.mobi</option>
						<option value='.museum'>.museum</option>
						<option value='.name'>.name</option>
						<option value='.post'>.post</option>
						<option value='.pro'>.pro</option>
						<option value='.tel'>.tel</option>
						<option value='.travel'>.travel</option>
						<option value='.xxx'>.xxx</option>
						<option>&nbsp;</option>
						<option>---USA TLD---</option>
						<option value='.edu'>.edu</option>
						<option value='.gov'>.gov</option>
						<option value='.mil'>.mil</option>
						<option>&nbsp;</option>
						<option>---Country Code TLD---</option>
						<option value='.ac'>.ac</option>
						<option value='.ad'>.ad</option>
						<option value='.ae'>.ae</option>
						<option value='.af'>.af</option>
						<option value='.ag'>.ag</option>
						<option value='.ai'>.ai</option>
						<option value='.al'>.al</option>
						<option value='.am'>.am</option>
						<option value='.an'>.an</option>
						<option value='.ao'>.ao</option>
						<option value='.aq'>.aq</option>
						<option value='.ar'>.ar</option>
						<option value='.as'>.as</option>
						<option value='.at'>.at</option>
						<option value='.au'>.au</option>
						<option value='.aw'>.aw</option>
						<option value='.ax'>.ax</option>
						<option value='.az'>.az</option>
						<option value='.ba'>.ba</option>
						<option value='.bb'>.bb</option>
						<option value='.bd'>.bd</option>
						<option value='.be'>.be</option>
						<option value='.bf'>.bf</option>
						<option value='.bg'>.bg</option>
						<option value='.bh'>.bh</option>
						<option value='.bi'>.bi</option>
						<option value='.bj'>.bj</option>
						<option value='.bm'>.bm</option>
						<option value='.bn'>.bn</option>
						<option value='.bo'>.bo</option>
						<option value='.br'>.br</option>
						<option value='.bs'>.bs</option>
						<option value='.bt'>.bt</option>
						<option value='.bv'>.bv</option>
						<option value='.bw'>.bw</option>
						<option value='.by'>.by</option>
						<option value='.bz'>.bz</option>
						<option value='.ca'>.ca</option>
						<option value='.cc'>.cc</option>
						<option value='.cd'>.cd</option>
						<option value='.cf'>.cf</option>
						<option value='.cg'>.cg</option>
						<option value='.ch'>.ch</option>
						<option value='.ci'>.ci</option>
						<option value='.ck'>.ck</option>
						<option value='.cl'>.cl</option>
						<option value='.cm'>.cm</option>
						<option value='.cn'>.cn</option>
						<option value='.co'>.co</option>
						<option value='.cr'>.cr</option>
						<option value='.cs'>.cs</option>
						<option value='.cu'>.cu</option>
						<option value='.cv'>.cv</option>
						<option value='.cx'>.cx</option>
						<option value='.cy'>.cy</option>
						<option value='.cz'>.cz</option>
						<option value='.dd'>.dd</option>
						<option value='.de'>.de</option>
						<option value='.dj'>.dj</option>
						<option value='.dk'>.dk</option>
						<option value='.dm'>.dm</option>
						<option value='.do'>.do</option>
						<option value='.dz'>.dz</option>
						<option value='.ec'>.ec</option>
						<option value='.ee'>.ee</option>
						<option value='.eg'>.eg</option>
						<option value='.eh'>.eh</option>
						<option value='.er'>.er</option>
						<option value='.es'>.es</option>
						<option value='.et'>.et</option>
						<option value='.eu'>.eu</option>
						<option value='.fi'>.fi</option>
						<option value='.fj'>.fj</option>
						<option value='.fk'>.fk</option>
						<option value='.fm'>.fm</option>
						<option value='.fo'>.fo</option>
						<option value='.fr'>.fr</option>
						<option value='.ga'>.ga</option>
						<option value='.gb'>.gb</option>
						<option value='.gd'>.gd</option>
						<option value='.ge'>.ge</option>
						<option value='.gf'>.gf</option>
						<option value='.gg'>.gg</option>
						<option value='.gh'>.gh</option>
						<option value='.gi'>.gi</option>
						<option value='.gl'>.gl</option>
						<option value='.gm'>.gm</option>
						<option value='.gn'>.gn</option>
						<option value='.gp'>.gp</option>
						<option value='.gq'>.gq</option>
						<option value='.gr'>.gr</option>
						<option value='.gs'>.gs</option>
						<option value='.gt'>.gt</option>
						<option value='.gu'>.gu</option>
						<option value='.gw'>.gw</option>
						<option value='.gy'>.gy</option>
						<option value='.hk'>.hk</option>
						<option value='.hm'>.hm</option>
						<option value='.hn'>.hn</option>
						<option value='.hr'>.hr</option>
						<option value='.ht'>.ht</option>
						<option value='.hu'>.hu</option>
						<option value='.id'>.id</option>
						<option value='.ie'>.ie</option>
						<option value='.il'>.il</option>
						<option value='.im'>.im</option>
						<option value='.in'>.in</option>
						<option value='.io'>.io</option>
						<option value='.iq'>.iq</option>
						<option value='.ir'>.ir</option>
						<option value='.is'>.is</option>
						<option value='.it'>.it</option>
						<option value='.je'>.je</option>
						<option value='.jm'>.jm</option>
						<option value='.jo'>.jo</option>
						<option value='.jp'>.jp</option>
						<option value='.ke'>.ke</option>
						<option value='.kg'>.kg</option>
						<option value='.kh'>.kh</option>
						<option value='.ki'>.ki</option>
						<option value='.km'>.km</option>
						<option value='.kn'>.kn</option>
						<option value='.kp'>.kp</option>
						<option value='.kr'>.kr</option>
						<option value='.kw'>.kw</option>
						<option value='.ky'>.ky</option>
						<option value='.kz'>.kz</option>
						<option value='.la'>.la</option>
						<option value='.lb'>.lb</option>
						<option value='.lc'>.lc</option>
						<option value='.li'>.li</option>
						<option value='.lk'>.lk</option>
						<option value='.lr'>.lr</option>
						<option value='.ls'>.ls</option>
						<option value='.lt'>.lt</option>
						<option value='.lu'>.lu</option>
						<option value='.lv'>.lv</option>
						<option value='.ly'>.ly</option>
						<option value='.ma'>.ma</option>
						<option value='.mc'>.mc</option>
						<option value='.md'>.md</option>
						<option value='.me'>.me</option>
						<option value='.mg'>.mg</option>
						<option value='.mh'>.mh</option>
						<option value='.mk'>.mk</option>
						<option value='.ml'>.ml</option>
						<option value='.mm'>.mm</option>
						<option value='.mn'>.mn</option>
						<option value='.mo'>.mo</option>
						<option value='.mp'>.mp</option>
						<option value='.mq'>.mq</option>
						<option value='.mr'>.mr</option>
						<option value='.ms'>.ms</option>
						<option value='.mt'>.mt</option>
						<option value='.mu'>.mu</option>
						<option value='.mv'>.mv</option>
						<option value='.mw'>.mw</option>
						<option value='.mx'>.mx</option>
						<option value='.my'>.my</option>
						<option value='.mz'>.mz</option>
						<option value='.na'>.na</option>
						<option value='.nc'>.nc</option>
						<option value='.ne'>.ne</option>
						<option value='.nf'>.nf</option>
						<option value='.ng'>.ng</option>
						<option value='.ni'>.ni</option>
						<option value='.nl'>.nl</option>
						<option value='.no'>.no</option>
						<option value='.np'>.np</option>
						<option value='.nr'>.nr</option>
						<option value='.nu'>.nu</option>
						<option value='.nz'>.nz</option>
						<option value='.om'>.om</option>
						<option value='.pa'>.pa</option>
						<option value='.pe'>.pe</option>
						<option value='.pf'>.pf</option>
						<option value='.pg'>.pg</option>
						<option value='.ph'>.ph</option>
						<option value='.pk'>.pk</option>
						<option value='.pl'>.pl</option>
						<option value='.pm'>.pm</option>
						<option value='.pn'>.pn</option>
						<option value='.pr'>.pr</option>
						<option value='.ps'>.ps</option>
						<option value='.pt'>.pt</option>
						<option value='.pw'>.pw</option>
						<option value='.py'>.py</option>
						<option value='.qa'>.qa</option>
						<option value='.re'>.re</option>
						<option value='.ro'>.ro</option>
						<option value='.rs'>.rs</option>
						<option value='.ru'>.ru</option>
						<option value='.rw'>.rw</option>
						<option value='.sa'>.sa</option>
						<option value='.sb'>.sb</option>
						<option value='.sc'>.sc</option>
						<option value='.sd'>.sd</option>
						<option value='.se'>.se</option>
						<option value='.sg'>.sg</option>
						<option value='.sh'>.sh</option>
						<option value='.si'>.si</option>
						<option value='.sj'>.sj</option>
						<option value='.sk'>.sk</option>
						<option value='.sl'>.sl</option>
						<option value='.sm'>.sm</option>
						<option value='.sn'>.sn</option>
						<option value='.so'>.so</option>
						<option value='.sr'>.sr</option>
						<option value='.ss'>.ss</option>
						<option value='.st'>.st</option>
						<option value='.su'>.su</option>
						<option value='.sv'>.sv</option>
						<option value='.sx'>.sx</option>
						<option value='.sy'>.sy</option>
						<option value='.sz'>.sz</option>
						<option value='.tc'>.tc</option>
						<option value='.td'>.td</option>
						<option value='.tf'>.tf</option>
						<option value='.tg'>.tg</option>
						<option value='.th'>.th</option>
						<option value='.tj'>.tj</option>
						<option value='.tk'>.tk</option>
						<option value='.tl'>.tl</option>
						<option value='.tm'>.tm</option>
						<option value='.tn'>.tn</option>
						<option value='.to'>.to</option>
						<option value='.tp'>.tp</option>
						<option value='.tr'>.tr</option>
						<option value='.tt'>.tt</option>
						<option value='.tv'>.tv</option>
						<option value='.tw'>.tw</option>
						<option value='.tz'>.tz</option>
						<option value='.ua'>.ua</option>
						<option value='.ug'>.ug</option>
						<option value='.uk'>.uk</option>
						<option value='.us'>.us</option>
						<option value='.uy'>.uy</option>
						<option value='.uz'>.uz</option>
						<option value='.va'>.va</option>
						<option value='.vc'>.vc</option>
						<option value='.ve'>.ve</option>
						<option value='.vg'>.vg</option>
						<option value='.vi'>.vi</option>
						<option value='.vn'>.vn</option>
						<option value='.vu'>.vu</option>
						<option value='.wf'>.wf</option>
						<option value='.ws'>.ws</option>
						<option value='.ye'>.ye</option>
						<option value='.yt'>.yt</option>
						<option value='.yu'>.yu</option>
						<option value='.za'>.za</option>
						<option value='.zm'>.zm</option>
						<option value='.zw'>.zw</option>
					</select>

					<label for='db_node' class='label_above'>" . wfMessage('db_node')->escaped() . "</label>";
				if (isset($dbNodes['Nodes'])) {
					$html .= "
						<select id='db_node' name='db_node'>";
					foreach ($dbNodes['Nodes'] as $nodeNumber => $nodeDetails) {
						$html .= "<option data-node='" . json_encode($nodeDetails, JSON_UNESCAPED_SLASHES) . "' value='" . $nodeDetails['node_number'] . "'>" . $nodeDetails['node'] . " (" . $nodeDetails['free'] . "/" . ($nodeDetails['free'] + $nodeDetails['count']) . " free)</option>";
					}
						$html .= "
						</select>";
				} else {
					if (isset($dbNodes['error'])) {
						$html .= "<small>" . wfMessage('db_nodes_error', $dbNodes['error'])->escaped() . "</small>";
					} else {
						$html .= "<small>" . wfMessage('no_db_nodes_remaining')->escaped() . "</small>";
					}
				}

					$html .= "

					<div class='button_bar'>
						<div class='button_break'></div>
						<div class='buttons_right'>
							<input id='wiki_fill_cancel_button' type='button' class='mw-ui-button mw-ui-destructive' value='" . wfMessage('wiki_cancel_fill')->escaped() . "'/>
							<input id='wiki_fill_go_button' type='button' class='mw-ui-button mw-ui-progressive' value='" . wfMessage('wiki_do_fill')->escaped() . "'/>
						</div>
					</div>
				</div>
			</div>";
			}
		}
		if ($massEdit) {
			$html .= wfMessage('mass_edit_warning')->parse();
		}
		$html .= "
		<form id='wiki_settings_form' method='post' action='{$page->getFullURL(['section' => 'wiki', 'action' => $action, 'do' => 'save'])}'>
			<fieldset>";
		if (!$massEdit && $wgUser->isAllowed('sites_edit_name_domain')) {
			if (is_array($errors) && count($errors)) {
				$html .= '
					<span class="wikisites errorbox">' . wfMessage('error-errors_occurred')->escaped() . '</span>
				';
			}
			/***********************/
			/* Name/Domain         */
			/***********************/
			$html .= "
				<h2>" . wfMessage('wiki_name_domain') . "</h2>
				" . (isset($errors['wiki_name']) ? '<span class="error">' . wfMessage($errors['wiki_name'])->escaped() . '</span>' : '') . "
				<label for='wiki_name' class='label_above'>" . wfMessage('wiki_name') . "</label>
				<input id='wiki_name' name='wiki_name' type='text' value='" . htmlentities($wiki->getName(), ENT_QUOTES) . "'/>

				" . (isset($errors['wiki_meta_name']) ? '<span class="error">' . wfMessage($errors['wiki_meta_name'])->escaped() . '</span>' : '') . "
				<label for='wiki_meta_name' class='label_above'>" . wfMessage('wiki_meta_name') . "</label>
				<input id='wiki_meta_name' name='wiki_meta_name' type='text' value='" . htmlentities($wiki->getMetaName(), ENT_QUOTES) . "'/>

				" . (isset($errors['wiki_domain']) ? '<span class="error">' . wfMessage($errors['wiki_domain'])->escaped() . '</span>' : '') . "
				<label for='wiki_domain' class='label_above'>" . wfMessage('wiki_domain') . "</label>
				<input id='wiki_domain' name='wiki_domain' type='text' value='" . htmlentities($wiki->getDomains()->getDomain(Domains::ENV_PRODUCTION), ENT_QUOTES) . "'/>

				" . (isset($errors['wiki_domain_local']) ? '<span class="error">' . wfMessage($errors['wiki_domain_local'])->escaped() . '</span>' : '') . "
				<label for='wiki_domain_local' class='label_above'>" . wfMessage('wiki_domain_local') . "</label>
				<input id='wiki_domain_local' name='wiki_domain_local' type='text' value='" . htmlentities($wiki->getDomains()->getDomain(Domains::ENV_DEVELOPMENT), ENT_QUOTES) . "'/>

				" . (isset($errors['wiki_domain_staging']) ? '<span class="error">' . wfMessage($errors['wiki_domain_staging'])->escaped() . '</span>' : '') . "
				<label for='wiki_domain_staging' class='label_above'>" . wfMessage('wiki_domain_staging') . "</label>
				<input id='wiki_domain_staging' name='wiki_domain_staging' type='text' value='" . htmlentities($wiki->getDomains()->getDomain(Domains::ENV_STAGING), ENT_QUOTES) . "'/>

				" . (isset($errors['wiki_domain_redirects']) ? '<span class="error">' . wfMessage($errors['wiki_domain_redirects'])->escaped() . '</span>' : '') . "
				<label for='wiki_domain_redirects' class='label_above'>" . wfMessage('wiki_domain_redirects') . "</label>
				<textarea id='wiki_domain_redirects' name='wiki_domain_redirects'>";
			if (is_array($wiki->getDomains()->getRedirects()) && count($wiki->getDomains()->getRedirects())) {
				$domains = '';
				foreach ($wiki->getDomains()->getRedirects() as $domain) {
					$domains .= htmlentities($domain, ENT_QUOTES) . "\n";
				}
				$html .= trim($domains, "\n");
			}
			$html .= "</textarea>";
		}

		/***********************/
		/* Information         */
		/***********************/
		if ($wgUser->isAllowed('sites_edit_information')) {
			$html .= "
				<h2>" . wfMessage('wiki_information') . ($massEdit ? "<input id='wiki_information' name='mass_edit_section[]' value='wiki_information' type='checkbox'/><label for='wiki_information'>" . wfMessage('edit_this_section')->escaped() . "</label>" : null) . "</h2>
				" . (isset($errors['wiki_category']) ? '<span class="error">' . wfMessage($errors['wiki_category'])->escaped() . '</span>' : '') . "
				<label for='wiki_category' class='label_above'>" . wfMessage('wiki_category') . "</label>
				<input id='wiki_category' name='wiki_category' type='text' value='" . htmlentities($wiki->getCategory(), ENT_QUOTES) . "'/>";
			if (is_array($categories) and count($categories)) {
				$html .= "
				<select id='wiki_category_select'>
					<option value='0'>" . wfMessage('choose_existing_category') . "</option>\n";
				foreach ($categories as $name) {
					$html .= "<option value='" . htmlentities($name, ENT_QUOTES) . "'>" . htmlentities($name, ENT_QUOTES) . "</option>\n";
				}
				$html .= "
				</select>";
			}

			$html .= (isset($errors['wiki_tags']) ? '<span class="error">' . wfMessage($errors['wiki_tags'])->escaped() . '</span>' : '') . "
				<script type='text/javascript'>
				var availableTags = [";
			foreach ($wikiTags as $tag) {
				$html .= "
					'" . htmlentities($tag, ENT_QUOTES) . "',";
			}
			$html .= "
				];
				</script>
				<label for='wiki_tags' class='label_above'>" . wfMessage('wiki_tags')->escaped() . "</label>
				<input id='wiki_tags' name='wiki_tags' type='text' value='" . htmlentities(implode(',', $wiki->getTags()), ENT_QUOTES) . "'/>";

			if (!$massEdit) {
				$html .= (isset($errors['wiki_language']) ? '<span class="error">' . wfMessage($errors['wiki_language'])->escaped() . '</span>' : '') . "
				<label for='wiki_language' class='label_above'>" . wfMessage('wiki_language') . "</label>
				<select class='wiki_language' name='wiki_language'>";
				if (is_array($languages) and count($languages)) {
					foreach ($languages as $code => $name) {
						$html .= "<option value='{$code}'" . ($wiki->getLanguage() == $code ? ' selected="selected"' : null) . (!$wiki->getLanguage() && $code == 'en' ? ' selected="selected"' : null) . ">" . strtolower($code) . " - {$name}</option>\n";
					}
				}
				$html .= "
				</select>";
			}

			if (is_array($wikiManagers) and count($wikiManagers)) {
				$html .= "
				<br/><h2>" . wfMessage('wiki_managers') . ($massEdit ? "<input id='wiki_managers' name='mass_edit_section[]' value='wiki_managers' type='checkbox'/><label for='wiki_managers'>" . wfMessage('edit_this_section')->escaped() . "</label>" : null) . "</h2>
				" . (isset($errors['wiki_managers']) ? '<span class="error">' . wfMessage($errors['wiki_managers'])->escaped() . '</span>' : '') . "
				<select id='wiki_managers_select' name='wiki_managers[]' multiple='multiple'>\n";
				foreach ($wikiManagers as $userId => $details) {
					$user = $details['user'];
					$html .= "
					<option value='" . htmlentities($user->getName(), ENT_QUOTES) . "'" . (is_array($wiki->getManagers()) && in_array($user->getName(), $wiki->getManagers()) ? " selected='selected'" : null) . ">" . htmlentities($user->getName(), ENT_QUOTES) . "</option>\n";
				}
				$html .= "
				</select>";
			}

			$html .= "
				<h2>" . wfMessage('wiki_notes') . ($massEdit ? "<input id='wiki_notes' name='mass_edit_section[]' value='wiki_notes' type='checkbox'/><label for='wiki_notes'>" . wfMessage('edit_this_section')->escaped() . "</label>" : null) . "</h2>
				" . (isset($errors['wiki_notes']) ? '<span class="error">' . $errors['wiki_notes'] . '</span>' : '') . "
				<textarea id='wiki_notes' name='wiki_notes'>" . htmlspecialchars($wiki->getNotes(), ENT_QUOTES, 'UTF-8') . "</textarea>";
		}

		/***********************/
		/* Wiki "Type"         */
		/***********************/
		$_wikis = [];
		if ($wgUser->isAllowed('sites_edit_type')) {
			$html .= "
				<br/><h2>" . wfMessage('portal_option') . ($massEdit ? "<input id='portal_option' name='mass_edit_section[]' value='portal_option' type='checkbox'/><label>" . wfMessage('edit_this_section')->escaped() . "</label>" : null) . "</h2>
				" . (isset($errors['portal']) ? '<span class="error">' . wfMessage($errors['portal'])->escaped() . '</span>' : '');

			$html .= "
				<select id='portal' name='portal'>
					<option value='-1'" . (!$wiki->isPortalMaster() && !$wiki->isPortalChild() ? " selected='selected'" : null) . ">" . wfMessage('portal_none') . "</option>
					<option value='0'" . ($wiki->isPortalMaster() ? " selected='selected'" : null) . ">" . wfMessage('portal_master') . "</option>\n";
			if (is_array($portalMasters) && count($portalMasters)) {
				foreach ($portalMasters as $siteKey => $domain) {
					if ($siteKey == $wiki->getSiteKey()) {
						// Skip showing this wiki as an assignable master.
						continue;
					}
					$domain = '';
					if (array_key_exists($siteKey, $_wikis)) {
						$domain = $_wikis[$siteKey]->getDomains()->getDomain();
					} else {
						$_wikis[$siteKey] = \DynamicSettings\Wiki::loadFromHash($siteKey);
						if ($_wikis[$siteKey] !== false) {
							$domain = $_wikis[$siteKey]->getDomains()->getDomain();
						}
					}
					$html .= "<option value='" . htmlentities($siteKey, ENT_QUOTES) . "'" . ($wiki->getPortalKey() == $siteKey ? " selected='selected'" : null) . ">" . htmlentities($domain, ENT_QUOTES) . "</option>\n";
				}
			}
			$html .= "
				</select><br/>";

			$html .= "
				<br/><h2>" . wfMessage('group_option') . ($massEdit ? "<input id='group_option' name='mass_edit_section[]' value='group_option' type='checkbox'/><label for='group_option'>" . wfMessage('edit_this_section')->escaped() . "</label>" : null) . "</h2>
				" . (isset($errors['group']) ? '<span class="error">' . wfMessage($errors['group'])->escaped() . '</span>' : '');

			$html .= "
				<select id='group' name='group'>
					<option value='-1'" . (!$wiki->isGroupMaster() && !$wiki->isGroupChild() ? " selected='selected'" : null) . ">" . wfMessage('group_none') . "</option>
					<option value='0'" . ($wiki->isGroupMaster() ? " selected='selected'" : null) . ">" . wfMessage('group_master') . "</option>\n";
			if (is_array($groupMasters) && count($groupMasters)) {
				foreach ($groupMasters as $siteKey => $domain) {
					if ($siteKey == $wiki->getSiteKey()) {
						// Skip showing this wiki as an assignable master.
						continue;
					}
					$domain = '';
					if (array_key_exists($siteKey, $_wikis)) {
						$domain = $_wikis[$siteKey]->getDomains()->getDomain();
					} else {
						$_wikis[$siteKey] = \DynamicSettings\Wiki::loadFromHash($siteKey);
						if ($_wikis[$siteKey] !== false) {
							$domain = $_wikis[$siteKey]->getDomains()->getDomain();
						} else {
							unset($_wikis[$siteKey]);
						}
					}
					$html .= "<option value='" . htmlentities($siteKey, ENT_QUOTES) . "'" . ($wiki->getGroupKey() == $siteKey ? " selected='selected'" : null) . ">" . htmlentities($domain, ENT_QUOTES) . "</option>\n";
				}
			}
			$html .= "
				</select>";

			$html .= "<br/>
				" . (isset($errors['group_file_repo']) ? '<span class="error">' . wfMessage($errors['group_file_repo'])->escaped() . '</span>' : '') . "
				<input id='group_file_repo' name='group_file_repo' type='checkbox' value='1'" . ($wiki->isGroupFileRepo() ? ' checked="checked"' : null) . "/><label for='group_file_repo'>" . wfMessage('group_file_repo') . "</label>";
		}

		/***********************/
		/* Database            */
		/***********************/
		if (!$massEdit && $wgUser->isAllowed('sites_edit_database')) {
			$html .= "<br/><h2>" . wfMessage('wiki_aws') . "</h2>
				" . (isset($errors['aws_region']) ? '<span class="error">' . $errors['aws_region'] . '</span>' : '') . "
				<label for='aws_region' class='label_above'>" . wfMessage('aws_region') . "</label>
				<input id='aws_region' name='aws_region' type='text' value='" . htmlentities($wiki->getAWSRegion()) . "'/>" .

				(isset($errors['s3_bucket']) ? '<span class="error">' . $errors['s3_bucket'] . '</span>' : '') . "
				<label for='s3_bucket' class='label_above'>" . wfMessage('s3_bucket') . "</label>
				<input id='s3_bucket' name='s3_bucket' type='text' value='" . htmlentities($wiki->getS3Bucket()) . "'/>" .

				(isset($errors['cloudfront_id']) ? '<span class="error">' . $errors['cloudfront_id'] . '</span>' : '') . "
				<label for='cloudfront_id' class='label_above'>" . wfMessage('cloudfront_id') . "</label>
				<input id='cloudfront_id' name='cloudfront_id' type='text' value='" . htmlentities($wiki->getCloudfrontId()) . "'/>" .

				(isset($errors['cloudfront_domain']) ? '<span class="error">' . $errors['cloudfront_domain'] . '</span>' : '') . "
				<label for='cloudfront_domain' class='label_above'>" . wfMessage('cloudfront_domain') . "</label>
				<input id='cloudfront_domain' name='cloudfront_domain' type='text' value='" . htmlentities($wiki->getCloudfrontDomain()) . "'/>";

			$html .= "<br/><h2>" . wfMessage('wiki_database') . "</h2>";
			$html .= (isset($errors['db_type']) ? '<span class="error">' . wfMessage($errors['db_type'])->escaped() . '</span>' : '') . "
				<label for='db_type' class='label_above'>" . wfMessage('db_type') . "</label>
				<select id='db_type' name='db_type'>";
			if (is_array($dbTypes) && count($dbTypes)) {
				foreach ($dbTypes as $type) {
					$html .= "<option value='{$type}'" . ($wiki->getDatabase()['db_type'] == $type ? ' selected="selected"' : null) . (!$wiki->getDatabase()['db_type'] && $type == 'mysql' ? ' selected="selected"' : null) . ">{$type}</option>\n";
				}
			}
				$html .= "
				</select>

				" . (isset($errors['db_server']) ? '<span class="error">' . wfMessage($errors['db_server'])->escaped() . '</span>' : '') .
				(isset($errors['db_servers_same']) ? '<span class="error">' . wfMessage($errors['db_servers_same'])->escaped() . '</span>' : '') .
				(isset($errors['db_server_no_port']) ? '<span class="error">' . wfMessage($errors['db_server_no_port'])->escaped() . '</span>' : '') . "
				<label for='db_server' class='label_above'>" . wfMessage('db_server')->escaped() . "</label>
				<input id='db_server' name='db_server' type='text' value='" . htmlentities($wiki->getDatabase()['db_server'], ENT_QUOTES) . "'/>

				" . (isset($errors['db_server_replica']) ? '<span class="error">' . wfMessage($errors['db_server_replica'])->escaped() . '</span>' : '') .
				(isset($errors['db_server_replica_no_port']) ? '<span class="error">' . wfMessage($errors['db_server_replica_no_port'])->escaped() . '</span>' : '') . "
				<label for='db_server_replica' class='label_above'>" . wfMessage('db_server_replica')->escaped() . "</label>
				<input id='db_server_replica' name='db_server_replica' type='text' value='" . htmlentities($wiki->getDatabase()['db_server_replica'], ENT_QUOTES) . "'/>

				" . (isset($errors['db_port']) ? '<span class="error">' . wfMessage($errors['db_port'])->escaped() . '</span>' : '') . "
				<label for='db_port' class='label_above'>" . wfMessage('db_port')->escaped() . "</label>
				<input id='db_port' name='db_port' type='text' value='" . htmlentities($wiki->getDatabase()['db_port'], ENT_QUOTES) . "'/>

				" . (isset($errors['db_name']) ? '<span class="error">' . wfMessage($errors['db_name'])->escaped() . '</span>' : '') . "
				<label for='db_name' class='label_above'>" . wfMessage('db_name')->escaped() . "</label>
				<input id='db_name' name='db_name' type='text' value='" . htmlentities($wiki->getDatabase()['db_name'], ENT_QUOTES) . "' maxlength='64'/>

				" . (isset($errors['db_user']) ? '<span class="error">' . wfMessage($errors['db_user'])->escaped() . '</span>' : '') . "
				<label for='db_user' class='label_above'>" . wfMessage('db_user')->escaped() . "</label>
				<input id='db_user' name='db_user' type='text' value='" . htmlentities($wiki->getDatabase()['db_user'], ENT_QUOTES) . "' maxlength='16'/>

				" . (isset($errors['db_password']) ? '<span class="error">' . wfMessage($errors['db_password'])->escaped() . '</span>' : '') . "
				<label for='db_password' class='label_above'>" . wfMessage('db_password')->escaped() . " - <span>" . ($wiki->getDatabase()['db_password'] ? wfMessage('leave_password_blank')->escaped() : wfMessage('please_enter_password')->escaped()) . "</span></label>
				<input id='db_password' name='db_password' type='text'/>";
		}

		/***********************/
		/* Search              */
		/***********************/
		if (!$massEdit && $wgUser->isAllowed('sites_edit_search')) {
			$html .= "
				<br/><h2>" . wfMessage('wiki_search')->escaped() . "</h2>";
			$html .= (isset($errors['search_type']) ? '<span class="error">' . wfMessage($errors['search_type'])->escaped() . '</span>' : '') . "
				<label for='search_type' class='label_above'>" . wfMessage('search_type')->escaped() . "</label>
				<select id='search_type' name='search_type'>";
			if (is_array($searchTypes) && count($searchTypes)) {
				foreach ($searchTypes as $type) {
					$html .= "<option value='{$type}'" . ($wiki->getSearchSetup()['search_type'] == $type ? ' selected="selected"' : null) . (!$wiki->getSearchSetup()['search_type'] && $type == 'elastic' ? ' selected="selected"' : null) . ">{$type}</option>\n";
				}
			}
				$html .= "
				</select>

				" . (isset($errors['search_server']) ? '<span class="error">' . wfMessage($errors['search_server'])->escaped() . '</span>' : '') . "
				<label for='search_server' class='label_above'>" . wfMessage('search_server')->escaped() . "</label>
				<input id='search_server' name='search_server' type='text' value='" . htmlentities($wiki->getSearchSetup()['search_server'], ENT_QUOTES) . "'/>

				" . (isset($errors['search_port']) ? '<span class="error">' . wfMessage($errors['search_port'])->escaped() . '</span>' : '') . "
				<label for='search_port' class='label_above'>" . wfMessage('search_port')->escaped() . "</label>
				<input id='search_port' name='search_port' type='text' value='" . htmlentities($wiki->getSearchSetup()['search_port'], ENT_QUOTES) . "'/>";
		}
		$siteKeys = [];
		if (is_array($wikis) && count($wikis)) {
			foreach ($wikis as $_wiki) {
				$siteKeys[] = $_wiki->getSiteKey();
			}
		}
		$html .= "
			</fieldset>
			<fieldset id='commit_fieldset'>
				" . (isset($errors['commit_message']) ? '<span class="error">' . $errors['commit_message'] . '</span>' : '') . "
				<label for='commit_message' class='label_above'>" . wfMessage('commit_message')->escaped() . "</label>
				<input id='commit_message' name='commit_message' type='text' value='" . htmlentities(trim($wgRequest->getText('commit_message')), ENT_QUOTES) . "'/><br/>";

		if ($massEdit) {
			$html .= "
				<input id='siteKey' name='sites' type='hidden' value='" . implode(',', $siteKeys) . "'/>";
		} else {
			$html .= "
				<input id='siteKey' name='siteKey' type='hidden' value='{$wiki->getSiteKey()}'/>";
		}
		$html .= "
				<button id='wiki_submit' name='wiki_submit' type='submit' class='mw-ui-button mw-ui-progressive'>" . wfMessage('save_key_value')->escaped() . "</button>
			</fieldset>";
		$html .= "
		</form>";

		return $html;
	}

	/**
	 * Output of runTool function
	 *
	 * @param string The output to be displayed.
	 * @param string The special page for the return link.
	 * @param string The type of job (recache, recacheAds, etecetera) being run.
	 * @param string [Optional] The jobKey(s) for the job created.
	 *
	 * @return string The built HTML
	 */
	public function runToolOutput($output, $returnLocation, $type, $jobKeys = false) {
		global $wgDSToolsSupportEmail;

		$wikiToolsLogPage = Title::newFromText('Special:WikiToolsLog');
		$wikiToolsLogURL = $wikiToolsLogPage->getFullURL();

		$type = ucfirst($type);

		$html = "
		<div>Encounter a Problem? <a href='mailto:{$wgDSToolsSupportEmail}?subject=Recache%20Issue&body=%0A%0ARecache%20output%20follows:%0A%0A" . rawurlencode($output) . "'>" . wfMessage('email_team') . "</a></div>
		<pre id='updateDBOutput'>" . htmlspecialchars($output) . "</pre>
		<div class='button_bar'>
			<div class='button_break'></div>
			<div class='buttons_right'>
		";

		if ($jobKeys !== false && count($jobKeys)) {
			foreach ($jobKeys as $task => $jobKey) {
				$html .= "
				<a href='{$wikiToolsLogURL}?log_type=" . $task . "' class=\"mw-ui-button\">" . wfMessage('view_job_queue', $task)->escaped() . "</a>";
			}
			foreach ($jobKeys as $task => $jobKey) {
				$html .= "
				<a href='{$wikiToolsLogURL}?action=viewoutput&jobkey=" . $jobKey . "' class=\"mw-ui-button mw-ui-progressive\">" . wfMessage('view_job_output', $task)->escaped() . "</a>";
			}
		}

		$html .= "
				<a href='" . Title::newFromText('Special:' . $returnLocation)->getLocalURL() . "' class='mw-ui-button'>Return to {$returnLocation}</a>
			</div>
		</div>
		";

		return $html;
	}

	/**
	 * Wiki (Un)Deletion Form
	 *
	 * @param array   $wiki      Wiki information
	 * @param boolean $delete    [Optional] Delete or Undelete
	 * @param string  $errorHtml [Optional] HTML to wrap in an error box.
	 *
	 * @return string Built HTML
	 */
	public function wikiDelete($wiki, $delete = true, $errorHtml = null) {
		$type = $delete ? 'delete' : 'undelete';
		$action = Title::newFromText('Special:WikiSites');
		$html = '';

		if ($errorHtml !== null) {
			$html .= "<div class='errorbox'>{$errorHtml}</div>";
		}

		$html .= "
		<form method='post' action='" . $action->getFullUrl(['section' => 'wiki', 'action' => $type]) . "'>
			" . wfMessage($type . '_wiki_confirm')->escaped() . "<br/>
			<input type='hidden' name='do' value='confirm'/>
			<input type='hidden' name='siteKey' value='{$wiki->getSiteKey()}'/>
			<button type='submit' class='mw-ui-button mw-ui-destructive'>" . wfMessage($type . '_wiki')->escaped() . "</button>
		</form>";

		return $html;
	}
}
