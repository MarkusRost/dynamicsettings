<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Wiki Promo Special Page
 *
 * @author    Tim Aldridge
 * @copyright (c) 2014 Curse Inc.
 * @license   GNU General Public License v2.0 or later
 * @package   Dynamic Settings
 * @link      https://gitlab.com/hydrawiki
**/

class TemplateWikiPromotions {
	/**
	 * Output HTML
	 *
	 * @var string
	 */
	private $HMTL;

	/**
	 * Wiki Promotions List
	 *
	 * @param array Multidimensional array of wiki Promotion objects.
	 * @param string [Optional] Page Key - promotion or notice, defaults to promotion.
	 *
	 * @return string Built HTML
	 */
	public function wikiPromotionsList($promotions, $pageKey = 'promotion') {
		global $wgOut, $wgUser, $wgRequest, $wgScriptPath;

		$promotionsPage	= Title::newFromText('Special:WikiPromotions');
		$promotionsURL	= $promotionsPage->getFullURL();

		$otherKey = ($pageKey == 'promotion' ? 'notice' : 'promotion');

		$thisPage	= Title::newFromText('Special:Wiki' . ucfirst($pageKey) . 's');
		$thisURL	= $thisPage->getFullURL();
		$otherPage	= Title::newFromText('Special:Wiki' . ucfirst($otherKey) . 's');
		$otherURL	= $otherPage->getFullURL();

		$html = "
			<div class='button_bar'>
				<div class='button_break'></div>
				<div class='buttons_right'>
					" . ($wgUser->isAllowed('wiki_promotions_recache') ? "<a href='{$promotionsURL}?section=recache' class='mw-ui-button'>" . wfMessage('recache_notices_and_promotions') . "</a>" : null) . "
					" . ($wgUser->isAllowed('wiki_promotions') ? "<a href='{$otherURL}' class='mw-ui-button'>" . wfMessage('manage_' . $otherKey . 's') . "</a>" : null) . "
					" . ($wgUser->isAllowed('wiki_promotions') ? "<a href='{$thisURL}?section=form&amp;do=add' class='mw-ui-button mw-ui-constructive'>" . wfMessage('add_' . $pageKey) . "</a>" : null) . "
				</div>
			</div>
			<table id='wikilist' class='weight_sort'>
				<thead>
					<tr>
						<th class='controls'>&nbsp;</th>
						<th>&nbsp;</th>
						<th>" . wfMessage($pageKey . '_name') . "</th>
						<th>" . wfMessage($pageKey . '_description') . "</th>
						<th>" . wfMessage($pageKey . '_image') . "</th>
						<th>" . wfMessage($pageKey . '_link') . "</th>
						<th>" . wfMessage($pageKey . '_begins') . "</th>
						<th>" . wfMessage($pageKey . '_expires') . "</th>
					</tr>
				</thead>
				<tbody>";
		if (is_array($promotions) && count($promotions)) {
			foreach ($promotions as $promotion_id => $promotion) {
				$html .= "
					<tr data-id='{$promotion_id}'>
						<td class='controls'>
							<div class='controls_container'>";
				if ($wgUser->isAllowed('wiki_promotions')) {
					$html .= HydraCore::awesomeIcon('wrench') . "
								<span class='dropdown'>
									<a href='{$thisURL}?section=form&amp;do=edit&amp;promotion_id={$promotion->getDatabaseId()}' title='" . wfMessage('edit_promotion')->escaped() . "' class='edit_promos'>" . HydraCore::awesomeIcon('pencil-alt') . wfMessage('edit')->escaped() . "</a>
									<a href='{$thisURL}?section=recache&amp;promotion_id={$promotion->getDatabaseId()}' title='" . wfMessage('recache')->escaped() . "' class='edit_promos'>" . HydraCore::awesomeIcon("sync") . wfMessage('recache')->escaped() . "</a>";
					if ($promotion->isPaused()) {
						$html .= "
									<a href='{$thisURL}?section=pause&do=resume&amp;promotion_id={$promotion->getDatabaseId()}' title='" . wfMessage('resume_promotion')->escaped() . "' class='resume promos'>" . HydraCore::awesomeIcon("play") . wfMessage('resume')->escaped() . "</a>";
					} else {
						$html .= "
									<a href='{$thisURL}?section=pause&do=pause&amp;promotion_id={$promotion->getDatabaseId()}' title='" . wfMessage('pause_promotion')->escaped() . "' class='pause promos'>" . HydraCore::awesomeIcon("pause") . wfMessage('pause')->escaped() . "</a>";
					}
					$html .= "
									<a href='{$thisURL}?section=delete&amp;promotion_id={$promotion->getDatabaseId()}' title='" . wfMessage('delete_promotion')->escaped() . "' class='delete promos'>" . HydraCore::awesomeIcon("times") . wfMessage('delete')->escaped() . "</a>
								</span>";
				}
				$html .= "
							</div>
						</td>
						<td class='promo weight'>" . intval($promotion->getWeight()) . "</td>
						<td>" . htmlentities($promotion->getName(), ENT_QUOTES) . "</td>
						<td>" . htmlentities($promotion->getDescription(), ENT_QUOTES) . "</td>
						<td class='center'>" . ($promotion->getImage() ? HydraCore::awesomeIcon("check") : HydraCore::awesomeIcon("times")) . "</td>
						<td>" . ($promotion->getLink() ? "<a href='" . htmlentities($promotion->getLink(), ENT_QUOTES) . "'>" . htmlentities($promotion->getLink(), ENT_QUOTES) . "</a>" : "&nbsp;") . "</td>
						<td>" . ($promotion->getBegins() ? htmlentities(wfTimestamp(TS_DB, $promotion->getBegins()), ENT_QUOTES) : "&nbsp;") . "</td>
						<td>" . ($promotion->getExpires() ? htmlentities(wfTimestamp(TS_DB, $promotion->getExpires()), ENT_QUOTES) : "&nbsp;") . "</td>
					</tr>";
			}
		} else {
			$html .= "
					<tr>
						<td colspan='8'>" . wfMessage('no_' . $pageKey . '_found') . "</td>
					</tr>";
		}
		$html .= "
				</tbody>
			</table>";

		return $html;
	}

	/**
	 * Wiki Promotion Form
	 *
	 * @param object Promotion object
	 * @param array Multidimensional array of wiki information.
	 * @param array Key name => Error of errors
	 * @param string [Optional] Page Key - promotion or notice, defaults to promotion.
	 *
	 * @return string Built HTML
	*/
	public function wikiPromotionsForm($promotion, $wikis, $errors, $pageKey = 'promotion') {
		$otherKey = ($pageKey == 'promotion' ? 'notice' : 'promotion');

		$thisPage	= Title::newFromText('Special:Wiki' . ucfirst($pageKey) . 's');
		$thisURL	= $thisPage->getFullURL();
		$otherPage	= Title::newFromText('Special:Wiki' . ucfirst($otherKey) . 's');
		$otherURL	= $otherPage->getFullURL();

		$html = '';

		$html .= "
			<form id='wiki_settings_form' method='post' action='{$thisPage->getFullURL(['section' => 'form', 'do' => 'save'])}'>
				<fieldset>
					" . (isset($errors['name']) ? '<span class="error">' . $errors['name'] . '</span>' : '') . "
					<label for='name' class='label_above'>" . wfMessage('promotion_name') . "</label>
					<input id='name' name='name' type='text' class='mw-ui-input mw-ui-input-large' value='" . htmlentities($promotion->getName(), ENT_QUOTES) . "' />";

		if ($pageKey == 'notice') {
			$html .= "
					<div id='typeTabs' class='form_tabs'>
						<ul>
							<li><a href='#tab-text'>Text Based</a></li>
							<li><a href='#tab-image'>Image Based</a></li>
						</ul>
						<div id='tab-text' data-type='text'>";
		}

		if ($pageKey == 'notice') {
			$html .= (isset($errors['description']) ? '<span class="error">' . $errors['description'] . '</span>' : '') . "
							<textarea id='description' name='description' type='text' class='mw-ui-input'>" . htmlentities($promotion->getDescription(), ENT_QUOTES) . "</textarea>
							<small>" . wfMessage('promotion_description_notice')->escaped() . "</small>
						</div>
						<div id='tab-image' data-type='image'>";
		} else {
			$html .= (isset($errors['description']) ? '<span class="error">' . $errors['description'] . '</span>' : '') . "
							<label for='image' class='label_above'>" . wfMessage('promotion_description_promotion')->escaped() . "</label>
							<input id='description' name='description' type='text' value='" . htmlentities($promotion->getDescription(), ENT_QUOTES) . "'/>";
		}
		$html .= (isset($errors['image']) ? '<span class="error">' . $errors['image'] . '</span>' : '') . "
							<label for='image' class='label_above'>" . wfMessage('promotion_image')->escaped() . "</label>
							<input id='image' name='image' type='text' value='" . htmlentities($promotion->getImage(), ENT_QUOTES) . "'/>

							" . (isset($errors['link']) ? '<span class="error">' . $errors['link'] . '</span>' : '') . "
							<label for='link' class='label_above'>" . wfMessage('promotion_link')->escaped() . "</label>
							<input id='link' name='link' type='text' value='" . htmlentities($promotion->getLink(), ENT_QUOTES) . "'/>";
		if ($pageKey == 'notice') {
			$html .= "
						</div>
					</div>";
		}

		$html .= (isset($errors['begins']) ? '<span class="error">' . $errors['begins'] . '</span>' : '') . "
					<label for='begins' class='label_above'>" . wfMessage('promotion_begins')->escaped() . "</label>
					<input id='begins_datepicker' data-input='begins' type='text' value='' placeholder='" . wfMessage('promotion_begins_placeholder', date_default_timezone_get())->escaped() . "'/>
					<input id='begins' name='begins' type='hidden' value='" . htmlentities($promotion->getBegins(), ENT_QUOTES) . "'/>

					" . (isset($errors['expires']) ? '<span class="error">' . $errors['expires'] . '</span>' : '') . "
					<label for='expires' class='label_above'>" . wfMessage('promotion_expires')->escaped() . "</label>
					<input id='expires_datepicker' data-input='expires' type='text' value='' placeholder='" . wfMessage('promotion_expires_placeholder', date_default_timezone_get())->escaped() . "'/>
					<input id='expires' name='expires' type='hidden' value='" . htmlentities($promotion->getExpires(), ENT_QUOTES) . "'/>";

		if ($pageKey == 'promotion') {
			$html .= "
					<br/><input id='isAd' name='isAd' type='checkbox' value='1'" . ($promotion->isAdvertisement() ? ' checked="checked"' : null) . "/><label for='isAd'>" . wfMessage('promo_is_ad')->escaped() . "</label><br/>";
		}
		$html .= "
					<label for='wiki_search' class='label_above'>" . wfMessage('add_or_remove_wikis')->escaped() . "</label>
					<div id='wiki_selection_container'>
						<input class='wiki_selections' name='wikis' data-select-key='wikipromotions' data-select-type='addremove' type='hidden' value='" . (is_array($wikis) && count($wikis) ? json_encode($wikis) : '[]') . "'/>
						<input type='hidden' class='everywhere' name='everywhere' value='" . ($promotion->isEnabledEverywhere() ? 1 : 0) . "'>
					</div>
					<input id='promotion_id' name='promotion_id' type='hidden' value='{$promotion->getDatabaseId()}'/>
					<input id='languages' name='languages' type='hidden' value='{$promotion->getLanguages()}'/>
					<button id='wiki_submit' name='wiki_submit' class='mw-ui-button mw-ui-progressive'>" . wfMessage('save_key_value')->escaped() . "</button>
				</fieldset>
				<div id='dialog-confirm' title='" . wfMessage('promotion_text_image_conflict_title')->parse() . "'>
					<p>
						<span class='ui-icon ui-icon-alert'></span>
						" . wfMessage('promotion_text_image_conflict')->parse() . "
					</p>
				</div>
			</form>";

		return $html;
	}

	/**
	 * Wiki Promos Deletion Form
	 *
	 * @param object Promotion object
	 * @param string [Optional] Page Key - promotion or notice, defaults to promotion.
	 *
	 * @return string Built HTML
	 */
	public function wikiPromotionsDelete($promotion, $pageKey = 'promotion') {
		$action = Title::newFromText('Special:Wiki' . ucfirst($pageKey) . 's');

		$html = "
		<form method='post' action='" . $action->getFullUrl(['section' => 'delete']) . "'>
			" . wfMessage('delete_' . $pageKey . '_confirm')->escaped() . "<br/>
			<input type='hidden' name='do' value='confirm'/>
			<input type='hidden' name='promotion_id' value='{$promotion->getDatabaseId()}'/>
			<button type='submit' class='mw-ui-button mw-ui-destructive'>" . wfMessage('delete_' . $pageKey)->escaped() . "</button>
		</form>";

		return $html;
	}
}
