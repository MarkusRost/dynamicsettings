function WikiPermissions($) {
	'use strict';
	this.init = function() {
		$(document).on('click', '.add_group', permissions.addGroup);
		$(document).on('click', '.group_fieldset .remove_group', permissions.removeGroup);
	};

	var permissions = {
		addGroup: function(e) {
			var template = $('#hidden_group_template .group_fieldset').clone(true);
			$(template).insertBefore('#commit_fieldset');
		},

		removeGroup: function(e) {
			var parent = $(this).parent();
			$(parent).remove();
		}
	};
}

var WP = new WikiPermissions(jQuery);
jQuery(document).ready(WP.init);